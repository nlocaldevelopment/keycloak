RSpec.describe Keycloak do

  # version
  it 'has a version number' do
    expect(Keycloak::VERSION).not_to be nil
  end

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Error
  ]

  # class
  context 'class' do

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          configure
          config
          default_realm
          default_client
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '.configure' do

          let(:configuration) { attributes_for(:configuration, :demo) }

          it 'should set a configuration, default_realm and default_client' do
            VCR.use_cassette('default_configuration') do
              expect do
                subject.configure do |config|
                  configuration.each do |attribute,value|
                    config.send("#{attribute}=".to_sym,value)
                  end
                end
              end.to change(subject, :config).and change(subject, :default_realm).and change(subject, :default_client)
            end
          end

        end

        describe '.config' do

          before { subject.configure }

          it 'should return current configuration' do
            expect(subject.config).to be_a Keycloak::Configuration
          end

        end

        describe '.default_realm' do

          context 'without realm_id configuration value' do

            before {
              Keycloak.configure do |config|
                attributes_for(:configuration).each do |attr,value|
                  config.send("#{attr}=".to_sym,value)
                end
              end
            }

            it 'should return nil' do
              expect(subject.default_realm).to be_nil
            end
          end

          context 'with realm_id configuration value' do
            before {
              VCR.use_cassette('default configuration') do
                Keycloak.configure do |config|
                  attributes_for(:configuration, :demo).each do |attr,value|
                    config.send("#{attr}=".to_sym,value)
                  end
                end
              end
            }

            it 'should return default realm' do
              expect(subject.default_realm).to be_a Keycloak::Entity::Realm
            end
          end

        end

        describe '.default_client' do

          context 'without realm configuration value' do

            before {
              Keycloak.configure do |config|
                attributes_for(:configuration).each do |attr,value|
                  config.send("#{attr}=".to_sym,value)
                end
              end
            }

            it 'should return nil' do
              expect(subject.default_client).to be_nil
            end
          end

          context 'with realm_id configuration value' do

            context 'without client_id configuration value' do

              before {
                VCR.use_cassette('default configuration') do
                  Keycloak.configure do |config|
                    attributes_for(:configuration, :demo, :without_client).each do |attr,value|
                      config.send("#{attr}=".to_sym,value)
                    end
                  end
                end
              }

              it 'should return nil' do
                expect(subject.default_client).to be_nil
              end
            end

            context 'with client_id configuration value' do
              before {
                VCR.use_cassette('default configuration') do
                  Keycloak.configure do |config|
                    attributes_for(:configuration, :demo).each do |attr,value|
                      config.send("#{attr}=".to_sym,value)
                    end
                  end
                end
              }

              it 'should return default client' do
                expect(subject.default_client).to be_a Keycloak::Entity::Client
              end
            end

          end

        end

      end

    end

  end

end
