RSpec.describe Keycloak::Configuration, type: :model do

  # class
  context 'class' do

    # libs
    include_examples 'included modules', modules: %w{
      ActiveModel::Validations
    }

  end

  # instance
  context 'instance' do

    # public interface
    describe 'public interface' do

      subject(:instance) { described_class.new }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          server_protocol
          server_domain
          server_port
          realm_id
          admin_client_id
          admin_client_secret
          admin_username
          admin_password
          default_client_id
          default_client_secret
        }
      }

      include_examples 'public methods', {
        methods: %i{
          reset
          to_h
        }
      }

      # attributes validations
      context 'attributes validations' do

        examples = [
          OpenStruct.new({
            attribute: :server_protocol,
            valids: %w{ http https },
            invalids: [ {}, '', 'lalala' ]
          }),
          OpenStruct.new({
            attribute: :server_domain,
            valids: %w{ localhost api.keycloak.com keycloak-new.es },
            invalids: [ {}, '', 'keycloak', 'keycloak.a', 'keycloak.lalala' ]
          }),
          OpenStruct.new({
            attribute: :server_port,
            valids: %w{ 9 99 999 9999 99999 },
            invalids: [ {}, '', '999999', 0.9 ]
          }),
        ]

        examples.each do |example|

          context "#{example.attribute}" do
            context 'valids' do
              example.valids.each do |value|
                it { should allow_value(value).for(example.attribute) }
              end
            end

            context 'invalids' do
              example.invalids.each do |value|
                it { should_not allow_value(value).for(example.attribute) }
              end
            end
          end

        end

      end

      # behaviour pact
      context 'behaviour pact' do

        describe '#reset' do

          it 'should return the instance with default values' do
            instance.admin_username = 'raul'
            expect{ instance.reset }.to change(instance,:admin_username)
          end

        end

        describe '#to_h' do
          subject { instance.to_h }
          it { is_expected.to be_a HashWithIndifferentAccess }
          it { is_expected.to include *instance.instance_variables.map{|v| v[1..-1].to_sym } }
        end

      end

    end

  end

end