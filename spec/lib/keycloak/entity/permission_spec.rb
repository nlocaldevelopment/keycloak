RSpec.describe Keycloak::Entity::Permission do

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::Permission::Error
  ]

  # class
  context 'class' do

    subject { described_class }

    # initialization
    include_examples 'mandatory attributes', {
      method: :new,
      attributes: %i{ name description type logic decisionStrategy client realm },
      params: { name: 'default-enabled', description: 'description', type: 'scope', logic: 'POSITIVE', decisionStrategy: 'UNANIMOUS', resources: [ SecureRandom.uuid ], client: OpenStruct.new(id: SecureRandom.uuid), realm: OpenStruct.new(id: SecureRandom.uuid) },
      error: 'Keycloak::Error'
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
        }
      }

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          search_url
        }
      }

      # behaviour pact
      describe 'behaviour pact' do

        describe '.search_url' do

          context 'without realm' do
            it 'should raise an error' do
              expect{ subject.send(:search_url) }.to raise_error Keycloak::Entity::Permission::Error
            end
          end

          context 'with realm' do
            let(:realm) { FFaker::Product.brand.downcase }

            context 'without client' do
              it 'should raise an error' do
                expect{ subject.send(:search_url,{ realm: realm }) }.to raise_error Keycloak::Entity::Permission::Error
              end
            end

            context 'with client' do
              let(:client_id) { SecureRandom.uuid }
              let(:param) { described_class.search_key }
              subject { described_class.send(:search_url,{ 'realm' => realm , client: client_id, param => 'resource-1-private' }) }
              it { is_expected.to be_a String }
              it { is_expected.to match %r{permission} }
              it { is_expected.to match %r{\?} }
              it { is_expected.to match %r{#{param}} }
            end

          end

        end

      end

    end

  end

  # instance
  context 'instance' do

    subject(:instance) { build(:permission) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          client
          policies
          resources
          scopes
          token
        }
      }

      include_examples 'public methods', {
        methods: %i{
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#' do
        end

      end

      describe 'relations' do

        context 'parents' do

          include_examples 'public methods', {
            methods: described_class.up_associations
          }

        end

        context 'childrens' do

          include_examples 'public methods', {
            methods: described_class.down_associations
          }

          include_examples 'childrens', {
            adapter: :http,
            error: 'Keycloak::Relations::Error',
            relations: described_class.down_associations
          }

        end

      end

    end

    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes

          entity_url
          resource_url

          entity_credentials
          client_credentials

          admin_token
          refresh_admin_token
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        disabled_methods = %i{
          entity_credentials
          client_credentials
        }

        context 'disabled methods' do
          disabled_methods.each do |disabled_method|
            subject(disabled_method) { instance.send(disabled_method) }
            include_examples 'disabled method', type: :hash, method: disabled_method, symbol: '#'
          end
        end

        describe '#entity_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.instance_variable_set("@#{described_class.primary_key}".to_sym,id)
            instance.send(:entity_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{resource-server} }
          it { is_expected.to match %r{permission} }
          it { is_expected.to match %r{#{id}} }
        end

        describe '#resource_url' do

          let(:resource) { 'associatedPolicies' }

          include_examples 'mandatory attributes', {
            method: :resource_url,
            attributes: %i{ resource },
            params: { resource: ':associatedPolicies' },
            error: 'Keycloak::Entity::Permission::Error'
          }

          context 'result' do

            context 'without params' do
              subject { instance.send(:resource_url, { resource: resource }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
            end

            context 'with params' do
              let(:param) { 'paramID' }
              subject { instance.send(:resource_url, { resource: resource, params: { param => 0 } }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
              it { is_expected.to match %r{\?} }
              it { is_expected.to match %r{#{param}} }
            end

          end

        end

      end

    end

  end

end