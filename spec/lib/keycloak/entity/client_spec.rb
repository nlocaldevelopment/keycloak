RSpec.describe Keycloak::Entity::Client do

  subject { described_class }

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::Client::Error
  ]


  # class
  context 'class' do

    let(:realm) { build(:realm) }

    # initialization
    include_examples 'mandatory attributes', {
      method: :new,
      attributes: %i{ realm clientId },
      params: { realm: Keycloak::Entity::Realm.new(id: FFaker::Product.brand.downcase) },
      error: 'Keycloak::Error'
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
        }
      }

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          search_url
        }
      }

      # behaviour pact
      describe 'behaviour pact' do

        describe '#search_url' do

          context 'without realm' do
            it 'should raise an error' do
              expect{ subject.send(:search_url) }.to raise_error Keycloak::Entity::Client::Error
            end
          end

          context 'with realm' do
            let(:param) { described_class.search_key }
            let(:realm) { FFaker::Product.brand.downcase }
            subject { described_class.send(:search_url,{ 'realm' => realm , param => 'api' }) }
            it { is_expected.to be_a String }
            it { is_expected.to match %r{#{realm}} }
            it { is_expected.to match %r{clients} }
            it { is_expected.to match %r{\?} }
            it { is_expected.to match %r{#{param}} }
          end

        end

      end

    end

  end

  # instance
  context 'instance' do

    subject(:instance) { build(:client) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
          realm
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          secret
        }
      }

      include_examples 'public methods',
        methods: %i[
          resource_server
          request_secret
          available_realm_management_permissions
        ]

      # behaviour pact
      context 'behaviour pact' do

        describe '#request_secret' do

          before {
            stub_request(:get, "#{instance.send(:entity_url)}/client-secret").to_return(secret)
          }

          context 'with bad request' do
            let(:secret) { build(:client_secret_show_request, :fail) }

            it 'should raise error' do
              expect{ instance.request_secret }.to raise_error Keycloak::APIError
            end
          end

          context 'with good request' do
            let(:secret) { build(:client_secret_show_request, :success) }

            it 'should returns secrets value' do
              secret_value = JSON.parse(secret.body)['value']
              expect(instance.request_secret).to eq secret_value
            end
          end

        end

        describe '#resource_server' do

          context 'when resource server instance variable is defined' do
            before { instance.resource_server = Keycloak::Entity::ResourceServer.new(realm: OpenStruct.new, client: OpenStruct.new) }

            it 'should returns the instance variable value' do
              expect(instance).not_to receive(:get)
              instance.resource_server
            end

            it 'should returns resource server entity' do
              expect(instance.resource_server).to be_a Keycloak::Entity::ResourceServer
            end
          end

          context 'when resource server instance variable is not defined' do
            context 'with an invalid request' do
              before { stub_request(:get, instance.send(:resource_server_url)).to_return(build(:resource_server_show_request, :fail)) }

              it 'should raise an error' do
                expect{ instance.resource_server }.to raise_error Keycloak::APIError
              end
            end

            context 'with a valid request' do
              before { stub_request(:get, instance.send(:resource_server_url)).to_return(build(:resource_server_show_request, :success)) }

              it 'should returns resource server entity' do
                expect(instance.resource_server).to be_a Keycloak::Entity::ResourceServer
              end

              it 'should sets resource server instance variable' do
                expect{instance.resource_server}.to change{instance.instance_variable_get(:@resource_server)}
              end
            end
          end

        end

        describe '#available_realm_management_permissions' do

          context 'with a valid request' do
            before {
              stub_request(:put, "#{instance.send(:entity_url)}/management/permissions").to_return(build(:management_permission_request, :valid))
            }

            it 'should return a response with scopePermissions attribute' do
              expect(instance.send(:available_realm_management_permissions)).to include 'scopePermissions'
            end
          end

          context 'with a bad request' do
            before {
              stub_request(:put, "#{instance.send(:entity_url)}/management/permissions").to_return(build(:management_permission_request, :fail))
            }

            it 'should raise error' do
              expect{ instance.send(:available_realm_management_permissions) }.to raise_error Keycloak::APIError
            end
          end

        end

      end

      describe 'relations' do

        context 'parents' do

          include_examples 'public methods', {
            methods: described_class.up_associations
          }

        end

        context 'childrens' do

          include_examples 'public methods', {
            methods: described_class.down_associations
          }

          include_examples 'childrens', {
            adapter: :http,
            error: 'Keycloak::Relations::Error',
            relations: described_class.down_associations
          }

        end

      end

    end

    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes

          entity_credentials
          client_credentials

          entity_url
          resource_server_url
          resource_url

          admin_token
          refresh_admin_token
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#entity_credentials' do
          it 'should call #client_credentials method' do
            expect(instance).to receive(:client_credentials).once
            instance.send(:entity_credentials)
          end
        end

        describe '#client_credentials' do
          before { instance.instance_variable_set(:@secret,'secret') }
          subject { instance.send(:client_credentials) }
          it { is_expected.to be_a Hash }
          it { is_expected.to include :grant_type, :client_id, :client_secret }
        end

        describe '#entity_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.instance_variable_set("@#{described_class.primary_key}".to_sym,id)
            instance.send(:entity_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{clients} }
          it { is_expected.to match %r{#{id}} }
        end

        describe '#resource_server_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.instance_variable_set("@#{described_class.primary_key}".to_sym,id)
            instance.send(:resource_server_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{resource-server} }
        end

        describe '#resource_url' do

          let(:resource) { 'resources' }

          include_examples 'mandatory attributes', {
            method: :resource_url,
            attributes: %i{ resource },
            params: { resource: 'resources' },
            error: 'Keycloak::Entity::Client::Error'
          }

          context 'result' do

            context 'when resources are roles' do
              let(:resource) { 'roles' }
              subject { instance.send(:resource_url, { resource: resource }) }

              it { is_expected.to be_a String }
              it { is_expected.not_to match %r{resource-server} }
              it { is_expected.to match %r{#{resource}} }
            end

            context 'when resources are not roles' do
              context 'without params' do
                subject { instance.send(:resource_url, { resource: resource }) }

                it { is_expected.to be_a String }
                it { is_expected.to match %r{resource-server} }
                it { is_expected.to match %r{#{resource}} }
              end

              context 'with params' do
                let(:param) { 'paramID' }
                subject { instance.send(:resource_url, { resource: resource, params: { param => 0 } }) }

                it { is_expected.to be_a String }
                it { is_expected.to match %r{resource-server} }
                it { is_expected.to match %r{#{resource}} }
                it { is_expected.to match %r{\?} }
                it { is_expected.to match %r{#{param}} }
              end
            end

          end

        end

      end

    end

  end

end