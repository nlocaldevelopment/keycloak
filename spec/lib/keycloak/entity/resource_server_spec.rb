RSpec.describe Keycloak::Entity::ResourceServer do

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::ResourceServer::Error
  ]

  # class
  context 'class' do

    subject { described_class }

    # libs
    include_examples 'included modules', modules: %w{
      Keycloak::Api
      Keycloak::Relations
      ActiveModel::Validations
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          new
        }
      }

      # initialization
      include_examples 'mandatory attributes', {
        method: :new,
        attributes: %i{ client realm },
        params: { client: OpenStruct.new(id: SecureRandom.uuid), realm: OpenStruct.new(id: SecureRandom.uuid) },
        error: 'Keycloak::Entity::ResourceServer::Error'
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#new' do

          context 'with mandatory attributes defined' do
            let(:mandatory_attribute) { :id }
            before { allow_any_instance_of(subject).to receive(:mandatory_attributes).and_return([mandatory_attribute]) }

            it 'should return an error when mandatory attribute is lost' do
              expect{ subject.new }.to raise_error Keycloak::Entity::ResourceServer::Error
            end

            it 'should not return an error when mandatory attribute is' do
              expect{ subject.new(mandatory_attribute => 1) }.not_to raise_error
            end
          end

        end

      end

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
        }
      }

    end

  end

  # instances
  context 'instances' do

    let(:params) { JSON.parse(build(:resource_server_show_request, :success).body) }

    subject(:instance) { build(:resource_server) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          client
          realm
        }
      }

      include_examples 'public methods', {
        methods: %i{
          to_h

          valid?
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#to_h' do
          subject { instance.to_h }
          let(:attributes) { params.keys }

          it { is_expected.to be_a HashWithIndifferentAccess }
          it { is_expected.to include *attributes }

          it 'any values should be keycloak entity or an openstruct instance' do
            subject.values.each do |value|
              expect(value.class.name).not_to match(%r{Keycloak::Entity})
              expect(value).not_to be_a OpenStruct
            end
          end
        end

      end

      describe 'relations' do

        context 'parents' do

          include_examples 'public methods', {
            methods: described_class.up_associations
          }

        end

        context 'childrens' do

          include_examples 'public methods', {
            methods: described_class.down_associations
          }

          include_examples 'childrens', {
            adapter: :http,
            error: 'Keycloak::Relations::Error',
            relations: described_class.down_associations
          }

        end

      end

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes
          base_url
          entity_url

          admin_token
          refresh_admin_token
        }
      }


      # behaviour pact
      context 'behaviour pact' do

        context '#base_url' do
          it 'should delegate to realm#admin_entity_url' do
            expect(instance.realm).to receive(:admin_entity_url).once
            instance.send(:base_url)
          end
        end

        describe '#entity_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.client.instance_variable_set("@#{instance.client.class.primary_key}".to_sym,id)
            instance.send(:entity_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{clients} }
          it { is_expected.to match %r{#{id}} }
          it { is_expected.to match %r{authz/resource-server} }
        end

        describe '#mandatory_attributes' do
          it 'should return an Array' do
            expect(instance.send(:mandatory_attributes)).to be_a Array
          end
        end

        describe '#admin_token' do
          it 'should call realm#admin_token' do
            expect(instance.realm).to receive(:admin_token).once
            instance.send(:admin_token)
          end
        end

        describe '#refresh_admin_token' do
          it 'should call realm#refresh_token' do
            expect(instance.realm).to receive(:refresh_token).once
            instance.send(:refresh_admin_token)
          end
        end

      end

    end

  end

end