# frozen_string_literal: true

RSpec.describe Keycloak::Entity::Group do

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::Group::Error
  ]

  # class
  context 'class' do

    subject { described_class }

    # initialization
    include_examples 'mandatory attributes', {
      method: :new,
      attributes: %i{ name path realm },
      params: { name: 'api', path: '/api', realm: OpenStruct.new(id: 1) },
      error: 'Keycloak::Error'
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          find_by
        }
      }

      # behaviour pact
      describe 'behaviour pact' do
        describe '.find_by' do
          context 'with invalid param format' do
            it 'should raise an error' do
              expect{ subject.find_by(1) }.to raise_error Keycloak::Entity::Group::Error
            end
          end

          context 'with valid param format' do
            it 'should raise an error when has invalid amount of attributes' do
              expect{ subject.find_by(id: 1, name: 'test') }.to raise_error Keycloak::Entity::Group::Error
            end

            it 'should call .where' do
              query = { id: 1 }
              expect(subject).to receive(:where).with(query).once.and_return([])
              subject.find_by(query)
            end

            it 'should ignore parent relations and admin_token in params' do
              query = { id: 1, realm: build(:realm), admin_token: OpenStruct.new(access_token: 'admin_token') }
              expect(subject).to receive(:where).with(query).once.and_return([])
              subject.find_by(query)
            end

            context 'when .where method returns a group who matchs with search_key' do
              it 'should returns the group' do
                group = OpenStruct.new(id: 1, subGroups: [])
                query = { id: 1 }
                expect(subject).to receive(:where).with(query).once.and_return([group])
                expect(subject).not_to receive(:search_into_subgroups)
                expect(subject.find_by(query)).to eq group
              end
            end

            context 'when .where method returns a group who doesnt match with search_key' do
              it 'should call .search_into_subgroups' do
                group = OpenStruct.new(id: 2, subGroups: [OpenStruct.new(id: 1)])
                query = { id: 1 }
                expect(subject).to receive(:where).with(query).once.and_return([group])
                expect(subject).to receive(:search_into_subgroups)
                subject.find_by(query)
              end
            end

          end

        end
      end

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          search_url
          search_into_subgroups
        }
      }

      # behaviour pact
      describe 'behaviour pact' do

        describe '.search_url' do

          context 'without realm' do
            it 'should raise an error' do
              expect{ subject.send(:search_url) }.to raise_error Keycloak::Entity::Group::Error
            end
          end

          context 'with realm' do
            let(:param) { described_class.search_key_aliases[described_class.search_key] }
            let(:realm) { FFaker::Product.brand.downcase }
            subject { described_class.send(:search_url,{ 'realm' => realm , param => 'api' }) }
            it { is_expected.to be_a String }
            it { is_expected.to match %r{#{realm}} }
            it { is_expected.to match %r{groups} }
            it { is_expected.to match %r{\?} }
            it { is_expected.to match %r{search} }
          end

        end

        describe '.search_into_subgroups' do
          let(:identity_key) { :name }
          let(:group) { build(:group) }

          it 'should returns the subgroup who matchs with the identity key' do
            subgroup = group.subgroups.first
            group.subGroups = [build(:group).to_h]
            expect(subject.send(:search_into_subgroups,{group: group, key: identity_key, value: subgroup.send(identity_key)}).send(identity_key)).to eq subgroup.send(identity_key)
          end
        end

      end

    end

  end

  # instance
  context 'instance' do

    subject(:instance) { build(:group) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
          realm
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          token
        }
      }

      include_examples 'public methods', {
        methods: %i{
          subgroups
          add_subgroup
          realm_roles
          client_roles
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#subgroups' do
          it 'should returns subgroups like a list of group entity' do
            instance.subgroups.each do |group|
              expect(group).to be_a Keycloak::Entity::Group
            end
          end
        end

        describe '#add_subgroup' do

          include_examples 'mandatory attributes', {
            method: :add_subgroup,
            attributes: %i{ subgroup },
            params: { subgroup: OpenStruct.new },
            error: 'Keycloak::Entity::Group::Error'
          }

          let(:subgroup_request) { build(:group_show_request, :success) }
          let(:subgroup) { described_class.new(JSON.parse(subgroup_request.body).merge(realm: instance.realm)) }

          before { stub_request(:post, "#{instance.send(:entity_url)}/children").to_return(subgroup_request) }

          it 'should add group to subgroup' do
            instance.add_subgroup(subgroup: subgroup)
            expect(instance.subGroups).to include subgroup.to_h
          end

        end

        describe '#realm_roles' do
          let(:assigned_roles_request) { build(:role_search_request, :success) }
          let(:roles) { JSON.parse(assigned_roles_request.body) }
          before { allow(instance).to receive(:get_roles).and_return(roles) }

          it 'should returns assigned realm roles' do
            expect(instance.realm_roles).to be_any
          end
        end

        describe '#client_roles' do
          let(:client) { build(:client) }
          let(:assigned_roles_request) { build(:role_search_request, :success) }
          let(:roles) { JSON.parse(assigned_roles_request.body) }
          before { allow(instance).to receive(:get_roles).and_return(roles) }

          context 'without client' do
            it 'should raise an error' do
              expect{ instance.client_roles }.to raise_error Keycloak::Entity::Group::Error
            end
          end

          context 'with client' do
            it 'should returns assigned client roles' do
              expect(instance.client_roles(client: client)).to be_any
            end
          end
        end

      end

      describe 'relations' do

        context 'parents' do

          include_examples 'public methods', {
            methods: described_class.up_associations
          }

        end

        context 'childrens' do

          include_examples 'public methods', {
            methods: described_class.down_associations
          }

          include_examples 'childrens', {
            adapter: :http,
            error: 'Keycloak::Relations::Error',
            relations: described_class.down_associations
          }

        end

      end

    end

    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes

          entity_url
          resource_url

          entity_credentials
          client_credentials

          admin_token
          refresh_admin_token

          get_roles
          build_role
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        disabled_methods = %i{
          entity_credentials
          client_credentials
        }

        context 'disabled methods' do
          disabled_methods.each do |disabled_method|
            subject(disabled_method) { instance.send(disabled_method) }
            include_examples 'disabled method', type: :hash, method: disabled_method, symbol: '#'
          end
        end

        describe '#entity_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.instance_variable_set("@#{described_class.primary_key}".to_sym,id)
            instance.send(:entity_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{groups} }
          it { is_expected.to match %r{#{id}} }
        end

        describe '#resource_url' do

          let(:resource) { 'members' }

          include_examples 'mandatory attributes', {
            method: :resource_url,
            attributes: %i{ resource },
            params: { resource: 'members' },
            error: 'Keycloak::Entity::Group::Error'
          }

          context 'result' do

            context 'without params' do
              subject { instance.send(:resource_url, { resource: resource }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
            end

            context 'with params' do
              let(:param) { 'paramID' }
              subject { instance.send(:resource_url, { resource: resource, params: { param => 0 } }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
              it { is_expected.to match %r{\?} }
              it { is_expected.to match %r{#{param}} }
            end

          end

        end

        describe '#get_roles' do
          let(:url) { "#{instance.send(:entity_url)}/role-mappings/realm" }
          before { stub_request(:get, url).to_return(roles_request) }

          context 'with a valid request' do
            let(:roles_request) { build(:role_search_request, :success) }

            it 'should returns roles array' do
              expect(instance.send(:get_roles,{url: url})).to be_any
            end
          end

          context 'with an invalid request' do
            let(:roles_request) { build(:role_search_request, :fail) }

            it 'should returns error request' do
              expect{ instance.send(:get_roles,{url: url}) }.to raise_error Keycloak::Entity::Group::Error
            end
          end
        end

        describe '#build_role' do
          let(:url) { "#{instance.send(:entity_url)}/role-mappings/realm" }
          let(:data) { JSON.parse(build(:role_show_request, :success).body) }
          before {
            allow(instance.realm).to receive(:admin_token).and_return(OpenStruct.new(access_token: 'token'))
          }

          context 'when type params is not defined
                  assigned type is used by default' do
            let(:role) { instance.send(:build_role, {reference: url, data: data}) }
            before {
              stub_request(:delete, url).to_return(OpenStruct.new(body: nil, status: 204))
            }

            it 'should return a role openstruct entity' do
              expect(role).to be_a OpenStruct
            end

            it 'role should respond to #leave_group' do
              expect(role).to respond_to :leave_group
            end
          end

          context 'when type params is defined as :available' do
            let(:role) { instance.send(:build_role, {type: :available, reference: "#{url}/available", data: data}) }
            before {
              stub_request(:post, url).to_return(OpenStruct.new(body: nil, status: 204))
            }

            it 'should return a role openstruct entity' do
              expect(role).to be_a OpenStruct
            end

            it 'role should respond to #join_group' do
              expect(role).to respond_to :join_group
            end
          end
        end

      end

    end

  end

end