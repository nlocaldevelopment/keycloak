RSpec.describe Keycloak::Entity::IdentityProvider do

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::IdentityProvider::Error
  ]

  # class
  context 'class' do

    subject { described_class }

    # initialization
    include_examples 'mandatory attributes', {
      method: :new,
      attributes: %i{ alias realm },
      params: { alias: 'facebook', realm: OpenStruct.new(id: 1) },
      error: 'Keycloak::Error'
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
        }
      }

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          search_url
        }
      }

      # behaviour pact
      describe 'behaviour pact' do

        describe '#search_url' do

          context 'without realm' do
            it 'should raise an error' do
              expect{ subject.send(:search_url) }.to raise_error Keycloak::Entity::IdentityProvider::Error
            end
          end

          context 'with realm' do
            let(:param) { described_class.search_key }
            let(:realm) { FFaker::Product.brand.downcase }
            subject { described_class.send(:search_url,{ 'realm' => realm , param => 'api' }) }
            it { is_expected.to be_a String }
            it { is_expected.to match %r{#{realm}} }
            it { is_expected.to match %r{identity-provider} }
            it { is_expected.to match %r{\?} }
            it { is_expected.to match %r{#{param}} }
          end

        end

      end

    end

  end

  # instance
  context 'instance' do

    subject(:instance) { build(:identity_provider) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
          realm
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          token
        }
      }

      include_examples 'public methods', {
        methods: %i{
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#' do
        end

      end

      describe 'relations' do

        context 'parents' do

          include_examples 'public methods', {
            methods: described_class.up_associations
          }

        end

      end

    end

    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes

          entity_url

          entity_credentials
          client_credentials

          admin_token
          refresh_admin_token
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        disabled_methods = %i{
          entity_credentials
          client_credentials
        }

        context 'disabled methods' do
          disabled_methods.each do |disabled_method|
            subject(disabled_method) { instance.send(disabled_method) }
            include_examples 'disabled method', type: :hash, method: disabled_method, symbol: '#'
          end
        end

        describe '#entity_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.instance_variable_set("@#{described_class.primary_key}".to_sym,id)
            instance.send(:entity_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{identity-provider} }
          it { is_expected.to match %r{#{id}} }
        end

      end

    end

  end

end