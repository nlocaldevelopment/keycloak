RSpec.describe Keycloak::Entity::Base do

  # class
  context 'class' do

    subject { described_class }

    # libs
    include_examples 'included modules', modules: %w{
      Keycloak::Api
      Keycloak::Relations
      Keycloak::TokenHandler
      Keycloak::Authorization
      ActiveModel::Validations
    }

    # extend modules
    describe 'modules' do
      include_examples 'api module', klass: described_class, load: :extend
      include_examples 'relations module', klass: described_class, load: %i{ extend }
      include_examples 'token_handler module', klass: described_class, load: %i{ extend }
      include_examples 'authorization module', klass: described_class, load: %i{ extend }
      include_examples 'crud module', {
        klass: described_class,
        search_key: :id,
        load: %i{ extend },
        params: {
          valid: { id: 'test' }
        }
      }
    end

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          new
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#new' do

          context 'with mandatory attributes defined' do
            let(:mandatory_attribute) { :id }
            before { allow_any_instance_of(subject).to receive(:mandatory_attributes).and_return([mandatory_attribute]) }

            it 'should return an error when mandatory attribute is lost' do
              expect{ subject.new }.to raise_error Keycloak::Error
            end

            it 'should not return an error when mandatory attribute is' do
              expect{ subject.new(mandatory_attribute => 1) }.not_to raise_error
            end
          end

          context 'with zero attributes' do
            it 'should generate a new instance with zero attributes' do
              attributes = {}
              expect(subject.new(attributes).instance_variables.size).to eq attributes.keys.size
            end
          end

          context 'with 2 attributes' do
            attributes = {id: 0, name: 'example'}
            subject(:instance) { described_class.new(attributes) }

            it 'should generate a new instance with 2 of attributes' do
              expect(instance.instance_variables.size).to eq attributes.keys.size
            end

            include_examples 'instance attributes', {
              accessor: false,
              attributes: attributes.keys
            }
          end

        end

      end

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
        }
      }

    end

  end

  # instances
  context 'instances' do

    let(:params) { {
      id: 1,
      entity: build(:realm),
      struct: OpenStruct.new,
    } }

    subject(:instance) { described_class.new params }

    before { allow_any_instance_of(described_class).to receive(:realm).and_return(build(:realm)) }

    # include modules
    include_examples 'api module', klass: described_class, load: :include, params: { id: 1 }
    include_examples 'relations module', klass: described_class, load: %i{ include }
    include_examples 'token_handler module', klass: described_class, load: %i{ include }
    include_examples 'authorization module', klass: described_class, load: %i{ include }
    include_examples 'crud module', {
      klass: described_class,
      search_key: :id,
      load: %i{ include },
      params: {
        valid: { id: 'test' }
      }
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          to_h
          update_configuration

          valid?
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#to_h' do
          subject { instance.to_h }
          let(:attributes) {
            k = params.keys
            k.delete(:entity)
            k.delete(:struct)
            k
          }

          it { is_expected.to be_a HashWithIndifferentAccess }
          it { is_expected.to include *attributes }

          it 'any values should be keycloak entity or an openstruct instance' do
            subject.values.each do |value|
              expect(value).not_to be_a Keycloak::Entity::Base
              expect(value).not_to be_a OpenStruct
            end
          end
        end

      end

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes
          base_url
          broker_url
          config_with

          entity_credentials
          client_credentials

          admin_token
          refresh_admin_token
        }
      }


      # behaviour pact
      context 'behaviour pact' do

        before { allow(instance).to receive(:realm).and_return(build(:realm)) }

        include_examples 'delegated methods', {
          delegators: {
            realm: %i{
              token_endpoint
              end_session_endpoint
              public_key
              algorithm
              issuer
            }
          }
        }

        disabled_methods = %i{
          entity_credentials
          client_credentials
        }

        context 'disabled methods' do
          disabled_methods.each do |disabled_method|
            subject(disabled_method) { instance.send(disabled_method) }
            include_examples 'disabled method', type: :hash, method: disabled_method, symbol: '#'
          end
        end

        context '#base_url' do
          it 'should delegate to realm#admin_entity_url' do
            expect(instance.realm).to receive(:admin_entity_url).once
            instance.send(:base_url)
          end
        end

        context '#broker_url' do
          subject { instance.send(:broker_url) }

          it 'should call to realm#entity_url' do
            expect(instance.realm).to receive(:entity_url).once
            instance.send(:broker_url)
          end

          it { is_expected.to be_a String }
          it { is_expected.to match(%r{broker}) }
        end

        describe '#mandatory_attributes' do
          it 'should return an Array' do
            expect(instance.send(:mandatory_attributes)).to be_a Array
          end
        end

        describe '#admin_token' do
          it 'should call realm#admin_token' do
            expect(instance.realm).to receive(:admin_token).once
            instance.send(:admin_token)
          end
        end

        describe '#refresh_admin_token' do
          it 'should call realm#refresh_token' do
            expect(instance.realm).to receive(:refresh_token).once
            instance.send(:refresh_admin_token)
          end
        end

      end

    end

  end

end