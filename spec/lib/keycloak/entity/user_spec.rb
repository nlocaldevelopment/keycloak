# frozen_string_literal: true

RSpec.describe Keycloak::Entity::User do

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::User::Error
  ]

  # class
  context 'class' do

    subject { described_class }

    # initialization
    include_examples 'mandatory attributes', {
      method: :new,
      attributes: %i{ email realm },
      params: { email: FFaker::Internet.email, realm: Keycloak::Entity::Realm.new(id: FFaker::Product.brand.downcase) },
      error: 'Keycloak::Error'
    }

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          build_from_token
        }
      }

      # behaviour pact
      describe 'behaviour pact' do

        describe '.build_from_token' do
          let!(:configuration) { Keycloak.config }
          before {
            VCR.use_cassette('default configuration') do
              Keycloak.configure do |config|
                attributes_for(:configuration, :demo).each do |attr,value|
                  config.send("#{attr}=".to_sym,value)
                end
              end
            end
          }

          context 'without token' do
            it 'should raise an error' do
              expect{ subject.build_from_token }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with token' do
            let!(:realm_id) { 'demo' }
            before {
              allow(Keycloak).to receive(:config).and_return(Keycloak::Configuration.new(realm_id: realm_id))

              # realm reload
              stub_request(:get, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/realms/#{realm_id}/.well-known/openid-configuration").to_return(build(:openid_configuration_request, :success))
              stub_request(:get, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/realms/#{realm_id}/.well-known/uma2-configuration").to_return(build(:uma2_configuration_request, :success))
              stub_request(:get, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/realms/#{realm_id}/protocol/openid-connect/certs").to_return(build(:certs_request, :success))
              stub_request(:post, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/realms/#{realm_id}/protocol/openid-connect/token").to_return(build(:user_token_request, :success))
            }

            context 'when doesnt include access token' do
              it 'should raise an error' do
                expect{ subject.build_from_token(token: {}) }.to raise_error Keycloak::Entity::User::Error
              end
            end

            context 'when includes access token' do
              let!(:token) { OpenStruct.new(JSON.parse(build(:user_token_request, :success).body)) }

              context 'expired' do
                it 'should raise an error' do
                  expect{ subject.build_from_token(token: token) }.to raise_error Keycloak::Entity::User::Error
                end
              end

              context 'active' do
                let!(:client_uuid) { SecureRandom.uuid }
                let!(:user_uuid) { SecureRandom.uuid }

                before do
                  # handle token inspection
                  allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token, type: :state).and_return(:active)
                  allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token).and_call_original

                  # client reload
                  allow_any_instance_of(Keycloak::Entity::Client).to receive(:id).and_return(client_uuid)
                  stub_request(:get, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/admin/realms/#{realm_id}/clients?clientId=gatekeeper").to_return(build(:client_search_request, :success))
                  stub_request(:get, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/admin/realms/#{realm_id}/clients/#{client_uuid}").to_return(build(:client_show_request, :success))

                  # user reload
                  allow_any_instance_of(described_class).to receive(:id).and_return(user_uuid)
                  stub_request(:get, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/admin/realms/#{realm_id}/users/#{user_uuid}").to_return(build(:user_show_request, :success))
                end

                context 'when token origin is a external realm' do
                  let!(:token) { OpenStruct.new(JSON.parse(build(:external_user_token_request, :success).body)) }
                  before {
                    allow_any_instance_of(described_class).to receive(:token_introspection).and_call_original
                    allow_any_instance_of(described_class).to receive(:token_introspection).with(type: :origin).and_return(:external)
                  }

                  context 'without audience param' do
                    it 'should return a user and doesnt exchange token' do
                      expect_any_instance_of(described_class).to receive(:exchange_token).and_call_original
                      user = subject.build_from_token(token: token)
                      expect(user).to be_a described_class
                      expect(user.token.access_token).not_to eq token
                    end
                  end

                  context 'with audience param' do
                    it 'should return a user and doesnt exchange token' do
                      expect_any_instance_of(described_class).to receive(:exchange_token).and_call_original
                      user = subject.build_from_token(token: token, audience: 'api')
                      expect(user).to be_a described_class
                      expect(user.token.access_token).not_to eq token
                    end
                  end

                end

                context 'when token origin is current realm' do
                  before {
                    allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token).and_call_original

                    allow_any_instance_of(described_class).to receive(:token_introspection).and_call_original
                    allow_any_instance_of(described_class).to receive(:token_introspection).with(type: :origin).and_return(:internal)
                  }

                  context 'without audience param' do
                    it 'should return a user and doesnt exchange token' do
                      expect_any_instance_of(described_class).not_to receive(:exchange_token)
                      expect(subject.build_from_token(token: token)).to be_a described_class
                    end
                  end

                  context 'with audience param' do
                    let!(:client_id) { 'gatekeeper' }
                    before {
                      allow_any_instance_of(Keycloak::Entity::Client).to receive(:clientId).and_return(client_id)
                    }

                    context 'similar to client token' do
                      it 'should return a user and doesnt exchange token' do
                        expect_any_instance_of(described_class).not_to receive(:exchange_token)
                        expect(subject.build_from_token(token: token, audience: client_id)).to be_a described_class
                      end
                    end

                    context 'different to client token' do
                      let!(:new_token) { OpenStruct.new(body: { access_token: 1234 }.to_json, code: 200) }
                      let!(:fake_token_introspection) { HashWithIndifferentAccess.new(JSON.parse(build(:user_token_introspection_request, :success).body)) }
                      before do
                        # stub user token exchange
                        stub_request(:post, "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth/realms/#{realm_id}/protocol/openid-connect/token").with(body: /subject_token/).to_return(new_token)
                        # make fake introspection with new token
                        allow_any_instance_of(described_class).to receive(:token_introspection).and_return(fake_token_introspection)
                      end

                      it 'should return a user with a new token' do
                        expect_any_instance_of(described_class).to receive(:exchange_token).and_call_original
                        user = subject.build_from_token(token: token, audience: 'api')
                        expect(user).to be_a described_class
                        expect(user.token.access_token).not_to eq token
                      end

                    end

                  end

                end

              end

            end

          end

        end

      end

    end

    # private interface
    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          search_url
        }
      }

      # behaviour pact
      describe 'behaviour pact' do

        describe '.search_url' do

          context 'without realm' do
            it 'should raise an error' do
              expect{ subject.send(:search_url) }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with realm' do
            let(:param) { described_class.search_key }
            let(:realm) { FFaker::Product.brand.downcase }
            subject { described_class.send(:search_url,{ 'realm' => realm , param => 'admin@publicar.com' }) }
            it { is_expected.to be_a String }
            it { is_expected.to match %r{#{realm}} }
            it { is_expected.to match %r{users} }
            it { is_expected.to match %r{\?} }
            it { is_expected.to match %r{#{param}} }
          end

        end

      end

    end

  end

  # instance
  context 'instance' do

    subject(:instance) { build(:user) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
          realm
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          password
          client
          token
        }
      }

      include_examples 'public methods', {
        methods: %i{
          client=

          policies
          policy

          join_to_group
          leave_a_group

          send_emails

          reset_password
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#client=' do
          let(:client) { build(:client) }
          it 'should update realm' do
            expect{ instance.client = client }.to change(instance,:realm).and change(instance,:client)
          end
        end

        describe '#policies' do

          context 'without client defined' do
            it 'should raise an error' do
              expect{ instance.policies }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with client defined' do
            before {
              instance.client = build(:client)
            }

            context 'when user has policies' do
              it 'should return an old resource ownership policy' do
                allow(instance.client).to receive(:policies).and_return([build(:policy)])
                expect(instance.policies).to all(be_a Keycloak::Entity::Policy)
              end
            end

            context 'when user hasnt policies' do
              it 'should create a new resource ownership policy' do
                allow(instance.client).to receive(:policies).and_return([])
                expect(instance.policies).to be_empty
              end
            end

          end

        end

        describe '#policy' do
          before {
            instance.client = build(:client)
          }

          context 'if policy exists' do
            it 'should return an old resource ownership policy' do
              allow(instance).to receive(:policies).and_return([build(:policy)])
              expect(instance.policy).to be_a Keycloak::Entity::Policy
            end
          end

          context 'if policy doesnt exist' do
            it 'should create a new resource ownership policy' do
              allow(instance).to receive(:policies).and_return([])
              expect(Keycloak::Entity::Policy).to receive(:create).and_return(build(:policy))
              expect(instance.policy).to be_a Keycloak::Entity::Policy
            end
          end
        end

        describe '#join_to_group' do
          let!(:group) { SecureRandom.uuid }

          context 'without group param' do
            it 'should raise an error' do
              expect{ instance.join_to_group }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with group param' do

            context 'with a invalid request' do
              before {
                stub_request(:put, "#{instance.send(:entity_url)}/groups/#{group}").to_return(status: 500)
              }

              it 'should raise an error' do
                expect{ instance.join_to_group(group: group) }.to raise_error Keycloak::Entity::User::Error
              end
            end

            context 'with a valid request' do
              before {
                stub_request(:put, "#{instance.send(:entity_url)}/groups/#{group}").to_return(status: 204)
              }

              it 'should call instance#groups' do
                expect(instance).to receive(:groups)
                instance.join_to_group(group: group)
              end
            end

          end

        end

        describe '#leave_a_group' do
          let!(:group) { SecureRandom.uuid }

          context 'without group param' do
            it 'should raise an error' do
              expect{ instance.leave_a_group }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with group param' do

            context 'with a invalid request' do
              before {
                stub_request(:delete, "#{instance.send(:entity_url)}/groups/#{group}").to_return(status: 500)
              }

              it 'should raise an error' do
                expect{ instance.leave_a_group(group: group) }.to raise_error Keycloak::Entity::User::Error
              end
            end

            context 'with a valid request' do
              before {
                stub_request(:delete, "#{instance.send(:entity_url)}/groups/#{group}").to_return(status: 204)
              }

              it 'should call instance#groups' do
                expect(instance).to receive(:groups)
                instance.leave_a_group(group: group)
              end
            end

          end

        end

        describe '#send_emails' do

          include_examples 'mandatory attributes', {
            method: :send_emails,
            attributes: %i{ types },
            params: { types: described_class.const_get(:EMAIL_TYPES) },
            error: 'Keycloak::Entity::User::Error'
          }

          context 'when types' do
            context 'valid' do
              context 'when request is invalid' do
                before { stub_request(:put, "#{instance.send(:entity_url)}/execute-actions-email?lifespan=#{12*3600}").to_return(body: { error: 'HTTP 401 Unauthorized' }.to_json, status: 401) }

                it 'should raise error' do
                  expect{ instance.send(:send_emails,{types: %i[verify_email]}) }.to raise_error Keycloak::Entity::User::Error
                end
              end

              context 'when request is valid' do
                before { stub_request(:put, "#{instance.send(:entity_url)}/execute-actions-email?lifespan=#{12*3600}").to_return(status: 200) }

                it 'should doesnt raise error' do
                  expect{ instance.send(:send_emails,{types: %i[verify_email]}) }.not_to raise_error
                end
              end
            end

            context 'invalid' do
              it 'should return error' do
                expect{ instance.send(:send_emails,{types: %i[explosive_email]}) }.to raise_error Keycloak::Entity::User::Error
              end
            end
          end

        end

        describe '#reset_password' do
          let(:arguments) { {password: 'pass'} }

          include_examples 'mandatory attributes', {
            method: :reset_password,
            attributes: %i{ password },
            params: { password: 'password' },
            error: 'Keycloak::Entity::User::Error'
          }

          context 'with bad request' do
            before { stub_request(:put, "#{instance.send(:entity_url)}/reset-password").to_return(body: { error: 'fail' }.to_json, status: 500) }

            it 'should raise error' do
              expect{ instance.send(:reset_password,arguments) }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with valid request' do
            before { stub_request(:put, "#{instance.send(:entity_url)}/reset-password").to_return(body: '', status: 204) }

            it 'should doesnt raise error' do
              expect{ instance.send(:reset_password,arguments) }.not_to raise_error
            end
          end

        end

      end

      describe 'relations' do

        context 'parents' do

          include_examples 'public methods', {
            methods: described_class.up_associations
          }

        end

        context 'childrens' do

          include_examples 'public methods', {
            methods: described_class.down_associations
          }

          include_examples 'childrens', {
            adapter: :http,
            error: 'Keycloak::Relations::Error',
            relations: described_class.down_associations
          }

        end

      end

    end

    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          mandatory_attributes

          entity_url
          resource_url
          entity_credentials
          client_credentials

          admin_token
          refresh_admin_token

          mutate_entity
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#entity_url' do
          let!(:id) { SecureRandom.uuid }
          subject {
            instance.instance_variable_set("@#{described_class.primary_key}".to_sym,id)
            instance.send(:entity_url)
          }
          it { is_expected.to be_a String}
          it { is_expected.to match %r{users} }
          it { is_expected.to match %r{#{id}} }
        end

        describe '#entity_credentials' do

          context 'without password' do
            it 'should raise an error' do
              expect{ instance.send(:entity_credentials) }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with password' do
            before { instance.password = '1234' }

            context 'without client' do
              it 'should raise an error' do
                expect{ instance.send(:entity_credentials) }.to raise_error Keycloak::Entity::User::Error
              end
            end

            context 'with client' do
              let(:client) { build(:client) }
              before { instance.instance_variable_set(:@client,client) }
              subject { instance.send(:entity_credentials) }

              it { is_expected.to be_a Hash }
              it { is_expected.to include :grant_type, :client_id, :client_secret, :username, :password }
            end

          end

        end

        describe '#client_credentials' do

          context 'without client' do
            it 'should raise an error' do
              expect{ instance.send(:client_credentials) }.to raise_error Keycloak::Entity::User::Error
            end
          end

          context 'with client' do
            let(:client) { build(:client) }
            before { instance.instance_variable_set(:@client,client) }

            it 'should call client.entity_credentials' do
              expect(client).to receive(:entity_credentials).once
              instance.send(:client_credentials)
            end
          end

        end

        describe '#resource_url' do

          let(:resource) { 'groups' }

          include_examples 'mandatory attributes', {
            method: :resource_url,
            attributes: %i{ resource },
            params: { resource: 'groups' },
            error: 'Keycloak::Entity::User::Error'
          }

          context 'result' do

            context 'without params' do
              subject { instance.send(:resource_url, { resource: resource }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
            end

            context 'with params' do
              let(:param) { 'paramID' }
              subject { instance.send(:resource_url, { resource: resource, params: { param => 0 } }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
              it { is_expected.to match %r{\?} }
              it { is_expected.to match %r{#{param}} }
            end

          end

        end

        describe '#mutate_entity' do

          modules = %w{
            TokenHandler
            CRUD
          }

          context 'without modules' do
            modules.each do |mod|
              class Dump; end
              before { allow(instance.class).to receive(:ancestors).and_return(Dump.ancestors) }

              context "#{mod.downcase}" do
                it 'should raise an error' do
                  expect{ instance.send(:mutate_entity) }.to raise_error Keycloak::Entity::User::Error
                end
              end
            end
          end

          features = %i{
            token_introspection
            reload
          }

          context 'disabled features' do
            features.each do |feature|
              before { allow(instance).to receive(:respond_to?).with(feature).and_return(false) }

              context "#{feature}" do
                it 'should raise an error' do
                  expect{ instance.send(:mutate_entity) }.to raise_error Keycloak::Entity::User::Error
                end
              end
            end
          end

          context 'with necesary modules and features enabled' do
            let(:token) { HashWithIndifferentAccess.new(JSON.parse(build(:user_token_introspection_request,:success).body)).merge(iss: 'https://keycloak.com/auth/realms/test', email: FFaker::Internet.email) }

            before {
              allow(instance).to receive(:token_introspection).and_return(token)
              allow(instance).to receive(:token_introspection).with(type: :origin).and_return(:internal)
            }

            context 'with client' do
              before { expect(instance).to receive(:reload).once }

              context 'defined' do
                it 'should not change client entity relation' do
                  instance.client = build(:client)
                  expect_any_instance_of(Keycloak::Entity::Client).not_to receive(:reload)
                  expect{ instance.send(:mutate_entity) }.not_to change(instance,:client)
                end
              end

              context 'undefined' do
                it 'should change client entity relation' do
                  expect_any_instance_of(Keycloak::Entity::Client).to receive(:reload).once
                  expect{ instance.send(:mutate_entity) }.to change(instance,:client)
                end
              end

            end

            context 'when token' do
              before { expect_any_instance_of(Keycloak::Entity::Client).to receive(:reload).once }

              context 'comes from external source' do
                it 'should not change instance himself' do
                  allow(instance).to receive(:token_introspection).with(type: :origin).and_return(:external)
                  expect(instance).not_to receive(:reload)
                  expect{ instance.send(:mutate_entity) }.not_to change(instance,:id)
                end
              end

              context 'is internal' do

                context 'if token entity id is different' do
                  it 'should change instance himself' do
                    expect(instance).to receive(:reload).once
                    expect{ instance.send(:mutate_entity) }.to change(instance,:id).and change(instance,:email)
                  end
                end

                context 'if token entity id is equal' do
                  it 'should not change instance himself' do
                    allow(instance).to receive(:token_introspection).and_return(token.merge(sub: instance.id))
                    expect(instance).not_to receive(:reload)
                    expect{ instance.send(:mutate_entity) }.not_to change(instance,:id)
                  end
                end

              end

            end

          end

        end

      end

    end

  end

end