RSpec.describe Keycloak::Entity::Realm do

  # custom errors
  include_examples 'custom errors', errors: [
    Keycloak::Entity::Realm::Error
  ]

  # class
  context 'class' do

    subject { described_class }

    before { allow(Keycloak.config).to receive(:realm_id).and_return(nil) }

    # initialization
    include_examples 'mandatory attributes', {
      method: :new,
      attributes: %i{ id },
      params: { id: FFaker::Product.brand.downcase },
      error: 'Keycloak::Error'
    }

  end

  # instance
  context 'instance' do

    subject(:instance) { build(:realm) }

    # public interface
    describe 'public interface' do

      include_examples 'instance attributes', {
        accessor: false,
        attributes: %i{
          algorithm
          public_key
          configuration
        }
      }

      include_examples 'instance attributes', {
        accessor: true,
        attributes: %i{
          id
          token
        }
      }

      include_examples 'public methods', {
        methods: %i{
          reload
          update_cert
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#id=' do
          context 'with configuration' do
            it 'should update configuration' do
              expect{ instance.id = '$$$' }.to change(instance.configuration,:realm_id)
            end
          end
        end

        describe '#update_cert' do

          before {
            stub_request(:get, instance.send(:openid_configuration_url)).to_return(build(:openid_configuration_request, :success))
            stub_request(:get, instance.send(:uma2_configuration_url)).to_return(build(:uma2_configuration_request, :success))
            allow(instance).to receive(:update_cert)
            allow(instance).to receive(:request_token)
            instance.reload #reload calls to update_cert

            allow(instance).to receive(:update_cert).and_call_original
          }

          context 'with invalid request' do
            it 'should raise an error' do
              stub_request(:get, instance.send(:jwks_uri)).to_return(build(:certs_request, :fail))
              expect { instance.update_cert }.to raise_error Keycloak::Entity::Realm::Error
            end
          end

          it 'should update algorithm and public_key' do
            stub_request(:get, instance.send(:jwks_uri)).to_return(build(:certs_request, :success))
            expect { instance.update_cert }.to change(subject, :algorithm).and change(subject, :public_key)
          end
        end

        describe '#reload' do
          it 'should update all sub-configurations' do
            expect(instance).to receive(:update_openid_configuration).once
            expect(instance).to receive(:update_uma2_configuration).once
            expect(instance).to receive(:update_cert).once
            expect(instance).to receive(:request_token).once
            instance.reload
          end
        end

      end

      describe 'relations' do

        context 'childrens' do

          include_examples 'public methods', {
            methods: described_class.down_associations
          }

          include_examples 'childrens', {
            adapter: :http,
            error: 'Keycloak::Relations::Error',
            relations: described_class.down_associations
          }

        end

      end


    end

    describe 'private' do

      include_examples 'private methods', {
        methods: %i{
          base_url
          entity_url
          admin_entity_url
          openid_configuration_url
          uma2_configuration_url
          broker_url
          resource_url
          update_openid_configuration
          update_uma2_configuration
          token_endpoint

          entity_credentials
          client_credentials

          admin_token
        }
      }

      include_examples 'delegated methods', {
        delegators: {
          itself: %i{
            client_credentials
          }
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        describe '#base_url' do
          subject { instance.send(:base_url) }

          it { is_expected.to be_a String }
          it { is_expected.to match %r{#{instance.configuration.server_protocol}} }
          it { is_expected.to match %r{#{instance.configuration.server_domain}} }
          it { is_expected.to match %r{#{instance.configuration.server_port}} }
        end

        describe '#entity_url' do
          subject { instance.send(:entity_url) }

          it { is_expected.to be_a String }
          it { is_expected.to match %r{auth} }
          it { is_expected.not_to match %r{admin} }
          it { is_expected.to match %r{realms} }
          it { is_expected.to match %r{#{instance.id}} }
        end

        describe '#admin_entity_url' do
          subject { instance.send(:admin_entity_url) }

          it { is_expected.to be_a String }
          it { is_expected.to match %r{auth} }
          it { is_expected.to match %r{admin} }
          it { is_expected.to match %r{realms} }
          it { is_expected.to match %r{#{instance.id}} }
        end

        describe '#openid_configuration_url' do
          subject { instance.send(:openid_configuration_url) }

          it { is_expected.to be_a String }
          it { is_expected.to match %r{openid} }
        end

        describe '#uma2_configuration_url' do
          subject { instance.send(:uma2_configuration_url) }

          it { is_expected.to be_a String }
          it { is_expected.to match %r{uma2} }
        end

        describe '#broker_url' do
          subject { instance.send(:broker_url) }

          it { is_expected.to be_a String }
          it { is_expected.to match %r{broker} }
        end

        describe '#resource_url' do

          let(:resource) { 'clients' }

          include_examples 'mandatory attributes', {
            method: :resource_url,
            attributes: %i{ resource },
            params: { resource: 'clients' },
            error: 'Keycloak::Entity::Realm::Error'
          }

          context 'result' do

            context 'without params' do
              subject { instance.send(:resource_url, { resource: resource }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
            end

            context 'with params' do
              let(:param) { 'paramID' }
              subject { instance.send(:resource_url, { resource: resource, params: { param => 0 } }) }

              it { is_expected.to be_a String }
              it { is_expected.to match %r{#{resource}} }
              it { is_expected.to match %r{\?} }
              it { is_expected.to match %r{#{param}} }
            end

          end

        end

        describe '#update_openid_configuration' do
          context 'with invalid request' do
            it 'should raise an error' do
              stub_request(:get, instance.send(:openid_configuration_url)).to_return(build(:openid_configuration_request, :fail))
              expect { instance.send(:update_openid_configuration) }.to raise_error Keycloak::Entity::Realm::Error
            end
          end

          it 'should config with requested datas' do
            stub_request(:get, instance.send(:openid_configuration_url)).to_return(build(:openid_configuration_request, :success))
            expect(instance).to receive(:config_with).once.and_return(nil)
            instance.send(:update_openid_configuration)
          end
        end

        describe '#update_uma2_configuration' do
          context 'with invalid request' do
            it 'should raise an error' do
              stub_request(:get, instance.send(:uma2_configuration_url)).to_return(build(:uma2_configuration_request, :fail))
              expect { instance.send(:update_uma2_configuration) }.to raise_error Keycloak::Entity::Realm::Error
            end
          end

          it 'should config with requested datas' do
            stub_request(:get, instance.send(:uma2_configuration_url)).to_return(build(:uma2_configuration_request, :success))
            expect(instance).to receive(:config_with).once.and_return(nil)
            instance.send(:update_uma2_configuration)
          end
        end

        describe '#token_endpoint' do

          context 'before reload entity' do
            it 'should return nil' do
              expect(instance.send(:token_endpoint)).to be_nil
            end
          end

          context 'after reload entity' do
            before {
              stub_request(:get, instance.send(:openid_configuration_url)).to_return(build(:openid_configuration_request, :success))
              instance.send(:update_openid_configuration)
            }
            subject { instance.send(:token_endpoint) }

            it { is_expected.to be_a String }
          end

        end

        describe '#admin_token' do
          it 'should call token attribute' do
            expect(instance).to receive(:token).once
            instance.send(:admin_token)
          end
        end

        describe '#entity_credentials' do
          subject { instance.send(:entity_credentials) }
          it { is_expected.to be_a Hash }
          it { is_expected.to include :grant_type, :client_id, :client_secret, :username, :password }
        end

      end

    end

  end

end