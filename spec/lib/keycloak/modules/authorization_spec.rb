RSpec.describe Keycloak::Authorization do

  class Dump; end

  include_examples 'authorization module', {
    klass: Dump,
    load: %i{ extend include }
  }

end