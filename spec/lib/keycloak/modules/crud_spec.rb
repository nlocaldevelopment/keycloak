# frozen_string_literal: true

RSpec.describe Keycloak::CRUD do

  include_examples 'crud module', {
    klass: OpenStruct,
    load: %i{ extend include },
    search_key: :colour,
    params: {
      valid: { id: 1, colour: 'red' },
    }
  }

end