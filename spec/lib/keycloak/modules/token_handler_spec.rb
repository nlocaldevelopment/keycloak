RSpec.describe Keycloak::TokenHandler do

  class Dump
    def initialize(args={}); end
  end

  include_examples 'token_handler module', klass: Dump, load: %i{ extend include }

end