# frozen_string_literal: true

require 'bundler/setup'
require 'keycloak'

# utils
require 'ffaker'
require 'webmock/rspec'
require 'shoulda-matchers'
require 'pry'

# autoload support folder
Dir['spec/support/**/*.rb'].each do |file|
  file = file.split('/')[1..-1].join('/')
  require File.expand_path(file,__dir__)
end

RSpec.configure do |config|
  # avoid run feature tests
  # config.filter_run_including focus: true
  # config.filter_run_excluding(vcr: true) if ARGV.empty?

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec

    # Keep as many of these lines as are necessary:
    with.library :active_model
  end
end