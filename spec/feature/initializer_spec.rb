RSpec.describe 'Initializer', :vcr do

  include_context 'initializer', configuration: :demo

  # is necesary define a valid default configuration
  describe Keycloak do
    it 'should return default configuration' do
      expect(subject.config.to_h).to eq build(:configuration, :demo).to_h
    end
  end

end
