RSpec.describe 'Token', :vcr do

  include_context 'initializer', configuration: :demo

  # this example is with default realm admin token
  context 'when request is unauthorized because token is expired' do

    context 'and refresh_token is not expired' do
      it 'should works refresh token' do
        expect{ realm.refresh_token }.not_to raise_error
      end
    end

    context 'and refresh_token is expired' do
      before { allow(realm).to receive(:token).and_return(build(:client_token_request, :success)) }

      it 'should doesnt work refresh token' do
        expect{ realm.refresh_token }.to raise_error Keycloak::TokenHandler::Error
      end

      it 'should request a new token' do
        expect{ realm.request_token }.not_to raise_error
      end
    end

  end

end
