RSpec.describe 'Authentication', :vcr do

  # client info inherit from gatekeeper service develop
  include_context 'initializer', configuration: :demo do
    let!(:request_uri) { URI.parse 'http://api:80' }
  end

  describe 'use cases' do

    context 'with a wrong token' do
      it 'should raises an error' do
        expect{ Keycloak::Entity::User.build_from_token(token: {}) }.to raise_error Keycloak::Entity::User::Error
      end
    end

    context 'with a expired token' do
      it 'should raises an error' do
        expired_token = OpenStruct.new(JSON.parse(build(:user_token_request, :success).body))
        expect{ Keycloak::Entity::User.build_from_token(token: expired_token) }.to raise_error Keycloak::Entity::User::Error
      end
    end

    context 'with a active token' do

      context 'from external realm' do
        let(:configuration) { build(:configuration, :external) }
        let(:external_realm) { Keycloak::Entity::Realm.new(id: 'external', configuration: configuration).reload }
        let(:client) { Keycloak::Entity::Client.new(clientId: external_realm.configuration.default_client_id, secret: external_realm.configuration.default_client_secret, realm: external_realm) }
        let(:user) { Keycloak::Entity::User.new(email: 'external@publicar.com', username: 'external', password: 'pass', client: client, realm: external_realm) }
        let(:token) { user.request_token.token }


        it 'should build a user /
            and make a token exchange to get a valid internal token for default client' do
          # to avoid raise token expired error
          allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).and_call_original
          allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token, type: :state).and_return(:active)

          expect_any_instance_of(Keycloak::Entity::User).to receive(:exchange_token).and_call_original
          user = Keycloak::Entity::User.build_from_token(token: token)
          expect(user).to be_a Keycloak::Entity::User
          expect(user.token.access_token).not_to eq token.access_token
        end
      end

      context 'from internal realm' do
        let(:user) { Keycloak::Entity::User.new(email: 'external@publicar.com', username: 'external', password: 'pass', client: Keycloak.default_client,realm: Keycloak.default_realm) }
        let(:token) { user.request_token.token }


        context 'when target audience is equal a default client' do
          it 'should build a user /
              and dont make a token exchange' do
            # to avoid raise token expired error
            allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).and_call_original
            allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token, type: :state).and_return(:active)

            expect_any_instance_of(Keycloak::Entity::User).not_to receive(:exchange_token).and_call_original
            user = Keycloak::Entity::User.build_from_token(token: token, audience: Keycloak.config.default_client_id )
            expect(user).to be_a Keycloak::Entity::User
            expect(user.token.access_token).to eq token.access_token
          end
        end

        context 'when target audience is different a default client' do
          it 'should build a user /
              and make a token exchange for requested client' do
            # to avoid raise token expired error
            allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).and_call_original
            allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token, type: :state).and_return(:active)

            expect_any_instance_of(Keycloak::Entity::User).to receive(:exchange_token).and_call_original
            user = Keycloak::Entity::User.build_from_token(token: token, audience: request_uri.host)
            expect(user).to be_a Keycloak::Entity::User
            expect(user.token.access_token).not_to eq token.access_token
          end
        end

      end

    end

  end

end
