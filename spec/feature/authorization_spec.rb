RSpec.describe 'Authorization', :vcr do

  describe 'use cases' do

    client_id = 'api'
    client_secret = '9c43404c-6762-402a-9fd6-7a3c69c9db48'

    # there are six types of users:
    # id - type           - credentials - groups                         - description
    # 1.   anonymous user     client      any                              anonymous
    # 2.   user               token       default                          unregistered user in client
    # 3.   client user        token       default,<client>                 registered user in client
    # 4.   owner user         token       default,<client>                 client with own resources
    # 5.   admin user         token       default,<client>,<client/admin>  admin over client
    # 6.   superadmin user    token       default,superadmin               admin over realm

    context 'over public resource' do
      resource_path = '/resources'

      context 'with anonymous user' do
        include_context 'initializer', configuration: :demo do
          let!(:request_uri) { URI.parse "http://#{client_id}#{resource_path}?client_id=#{client_id}&client_secret=#{client_secret}" }
          let!(:params) { HashWithIndifferentAccess.new(URI::decode_www_form(request_uri.query).to_h) }
          let!(:client) { Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: Keycloak.default_realm).reload }
        end


        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          # anonymous user make authz request with client entity
          expect(client.authorized?(resource: resource.id, scope: :get, audience: client_id)).to be true
        end
      end

      context 'with user' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'user@publicar.com' },
          credentials: {
            username: 'user',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :get, audience: client_id)).to be true
        end

      end

      context 'with client' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'client@publicar.com' },
          credentials: {
            username: 'client',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :get, audience: client_id)).to be true
        end

      end

      # resources don't have a owner

      context 'with admin' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'admin@publicar.com' },
          credentials: {
            username: 'admin',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :get, audience: client_id)).to be true
        end

      end

      context 'with superadmin' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'superadmin@publicar.com' },
          credentials: {
            username: 'superadmin',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :get, audience: client_id)).to be true
        end

      end

    end

    context 'over private resource' do
      resource_path = '/resource/id'

      context 'with anonymous user' do
        include_context 'initializer', configuration: :demo do
          let!(:request_uri) { URI.parse "http://#{client_id}#{resource_path}?client_id=#{client_id}&client_secret=#{client_secret}" }
          let!(:params) { HashWithIndifferentAccess.new(URI::decode_www_form(request_uri.query).to_h) }
          let!(:client) { Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: Keycloak.default_realm).reload }
        end


        it 'should return false' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          # anonymous user make authz request with client entity
          expect(client.authorized?(resource: resource.id, scope: :put, audience: client_id)).to be false
        end
      end

      context 'with user' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'user@publicar.com' },
          credentials: {
            username: 'user',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return false' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :put, audience: client_id)).to be false
        end

      end

      context 'with client' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'client@publicar.com' },
          credentials: {
            username: 'client',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return false' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :put, audience: client_id)).to be false
        end

      end

      context 'with owner' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'owner@publicar.com' },
          credentials: {
            username: 'owner',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :put, audience: client_id)).to be true
        end

      end

      context 'with admin' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'admin@publicar.com' },
          credentials: {
            username: 'admin',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :put, audience: client_id)).to be true
        end

      end

      context 'with superadmin' do
        include_context 'authenticated user',
          configuration: :demo,
          resource_path: resource_path,
          audience: client_id,
          audience_secret: client_secret,
          user: { email: 'superadmin@publicar.com' },
          credentials: {
            username: 'superadmin',
            password: 'pass',
          }

        # exchange token for requested client
        before {
          user.client = Keycloak::Entity::Client.new(clientId: params[:client_id], secret: params[:client_secret], realm: user.realm).reload
          user.exchange_token
        }

        it 'should return true' do
          resource_uri = [client_id,resource_path].join
          resources = client.resources(uri: resource_uri)
          resource = resources.first

          expect(user.authorized?(resource: resource.id, scope: :put, audience: client_id)).to be true
        end

      end

    end

  end

end
