# frozen_string_literal: true

RSpec.describe 'User creation process', :vcr do

  include_context 'initializer', configuration: :demo do
    let!(:attributes) {
      {
        valid: {
          email: 'gem@rspec.es',
          username: 'gem',
          firstName: 'gem',
          lastName: 'keycloak',
          enabled: true,
          emailVerified: false, # put true in production environment
          attributes: {
            attribute: 'value'
          },
          realm: realm,
          client: client,
        },
        invalid: {},
      }
    }
  end

  context 'without admin_token' do
    it 'should raise an error' do
      expect{ Keycloak::Entity::User.create(attributes[:valid]) }.to raise_error Keycloak::CRUD::Error
    end
  end

  context 'with admin_token' do

    context 'with invalid attributes' do
      it 'should raise an error' do
        expect{ Keycloak::Entity::User.create(attributes[:invalid].merge(admin_token: admin_token )) }.to raise_error Keycloak::Error
      end
    end

    context 'with valid attributes' do
      let!(:user) { Keycloak::Entity::User.new(attributes[:valid]).save }
      let!(:group) { Keycloak::Entity::Group.find_by(name: 'api', admin_token: admin_token, realm: realm) }
      after {
        user.delete
      }

      it 'should return a new record' do
        expect(user).to respond_to :id
      end

      # create a ownership policy is a pre-requisite to create ownership resource permission
      it 'should can create a new user ownership policy' do
        expect(user.policy).to be_a Keycloak::Entity::Policy
      end

      # user joined to a client group becomes in a client
      it 'should can be added to a group' do
        expect{ user.join_to_group(group: group.id) }.to change{ user.groups.size }
      end

      # client leaved to a client group becomes in an simple user of realm
      it 'should can be removed from a group' do
        user.join_to_group(group: group.id)
        expect{ user.leave_a_group(group: group.id) }.to change{ user.groups.size }
      end
    end
  end

end