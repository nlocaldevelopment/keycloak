# frozen_string_literal: true

RSpec.describe 'Resource creation process', :vcr do

  include_context 'initializer', configuration: :demo do
    let!(:owner) { Keycloak::Entity::User.find_by(username: 'owner', admin_token: admin_token, realm: realm, client: client) }
    let!(:attributes) {
      {
        valid: {
          name: 'tester',
          type: 'teapot',
          owner: owner,
          displayName: 'this is a test' ,
          uris: %w{ /test },
          scopes: %w{ get post put patch delete },
          client: client,
          realm: client.realm,
        },
        invalid: {},
      }
    }
  end

  # TODO: automatizar crear client
  # automatizar crear default scopes (http methods)
  # automatizar crear default policies
  # automatizar crear recursos app

  context 'without admin_token' do
    it 'should raise an error' do
      expect{ Keycloak::Entity::Resource.create(attributes[:valid]) }.to raise_error Keycloak::CRUD::Error
    end
  end

  context 'with admin_token' do

    context 'with invalid attributes' do
      it 'should raise an error' do
        expect{ Keycloak::Entity::Resource.create(attributes[:invalid].merge(admin_token: admin_token )) }.to raise_error Keycloak::Error
      end
    end

    context 'with valid attributes' do
      let!(:resource) { Keycloak::Entity::Resource.create(attributes[:valid].merge(admin_token: realm.send(:admin_token))) }
      after {
        resource.delete
      }

      it 'should return a new record' do
        expect(resource).to respond_to :id
      end

      # permission join policies, resources and scopes
      # the usual policies are ownership, admin and public
      # we create scope list from http methods
      context 'permissions over resource' do

        # required policies
        let!(:ownership_policy) { owner.policy } # private policy
        let!(:admin_policy) { Keycloak::Entity::Policy.find_by(name: 'admin-access', client: client, realm: realm, admin_token: realm.send(:admin_token)) }
        let!(:public_policy) { Keycloak::Entity::Policy.find_by(name: 'public-access', client: client, realm: realm, admin_token: realm.send(:admin_token)) }

        # usual permissions are public and private
        # with scope permissions you can define the scope to be enabled
        # the number of policies or resources are open over a permission
        let!(:permissions) {
          {
            private: {
              name: "#{resource.name}-private",
              description: 'acceso privado',
              type: 'scope',
              logic: 'POSITIVE',
              decisionStrategy: 'AFFIRMATIVE',
              policies: [ admin_policy.id, ownership_policy.id ],
              resources: [ resource.id ],
              scopes: %w{ put patch delete },
            },
            public: {
              name: "#{resource.name}-public",
              description: 'acceso público',
              type: 'scope',
              logic: 'POSITIVE',
              decisionStrategy: 'AFFIRMATIVE',
              policies: [ public_policy.id ],
              resources: [ resource.id ],
              scopes: %w{ get }
            }
          }
        }

        # public policy is used for public access
        it 'should can create public access permission' do
          expect{ Keycloak::Entity::Permission.create(permissions[:public].merge(client: client, realm: client.realm, admin_token: realm.send(:admin_token))) }.to change{ Keycloak::Entity::Permission.where(name: permissions[:public][:name], client: client, realm: client.realm, admin_token: realm.send(:admin_token)).size }
        end

        # ownership and admin policies are used for private access
        it 'should can create private access permission' do
          expect{ Keycloak::Entity::Permission.create(permissions[:private].merge(client: client, realm: client.realm, admin_token: realm.send(:admin_token))) }.to change{ Keycloak::Entity::Permission.where(name: permissions[:private][:name], client: client, realm: client.realm, admin_token: realm.send(:admin_token)).size }
        end

      end

    end

  end

end