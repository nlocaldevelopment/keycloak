# frozen_string_literal: true

RSpec.describe 'Client creation process', :vcr do

  include_context 'initializer', configuration: :demo do
    let!(:attributes) {
      {
        valid: {
          clientId: 'test', # mandatory attribute for keycloak
          name: 'test',
          description: 'test client',
          baseUrl: 'http://test.es', # domain url
          redirectUris: ['/*'],
          standardFlowEnabled: true,
          implicitFlowEnabled: true,
          directAccessGrantsEnabled: true, # enable get token via api
          serviceAccountsEnabled: true, # enable client token
          authorizationServicesEnabled: true, # enable authorization feature
          authenticationFlowBindingOverrides: {}, # sets custom flows
          fullScopeAllowed: true,
          realm: realm, # mandatory attribute for gem
        },
        invalid: {},
      }
    }
  end

  context 'without admin_token' do
    it 'should raise an error' do
      expect{ Keycloak::Entity::Client.create(attributes[:valid]) }.to raise_error Keycloak::CRUD::Error
    end
  end

  context 'with admin_token' do

    context 'with invalid attributes' do
      it 'should raise an error' do
        expect{ Keycloak::Entity::Client.create(attributes[:invalid]) }.to raise_error Keycloak::CRUD::Error
      end
    end

    context 'with valid attributes' do
      let!(:client) { Keycloak::Entity::Client.new(attributes[:valid]).save }
      after {
        client.delete
      }

      it 'should return a new record' do
        expect(client).to respond_to :id
      end

      # INDEX
      # 01. change default settings of resource-server
      # 02. enable token exchange
      # 03. create basic scope
      # 04. update default resource to include domain and basic scope
      # 05. create groups: client and client-admin
      # 06. add client roles to gatekeeper group
      # 07. create new client roles: public
      # 08. add public role to default group
      # 09. create default policies
      # 10. create default permissions


      context 'after creation manual hooks' do
        default_scope    = %w[get post put patch delete]
        default_policies = %i[superadmin_group client_admin_group client_group]

        # datas
        let(:group_datas) {
          {
            group: {
              name: client.clientId,
              path: "/#{client.clientId}",
              realm: client.realm,
              admin_token: client.send(:admin_token),
            },
            subgroup: {
              name: "#{client.clientId}-admin",
              path: "/#{client.clientId}/admin",
              realm: client.realm,
            },
          }
        }

        let(:group_roles) {
          {
            group: {
              realmRoles: %w[offline_access uma_authorization], # offline_access allows get a offline token
              clientRoles: {
                broker: %w[read-token], # allow make a request to get stored token from identity-provider
              },
            },
            subgroup: {
              realmRoles: %w[offline_access uma_authorization],
              clientRoles: {
                "#{client.clientId}" => %w[uma_protection],
                "broker" => %w[read-token],
                "realm-management" => %w[
                  impersonation
                  manage-authorization
                  manage-identity-providers
                  manage-users
                  query-clients
                  query-groups
                  query-realms
                  query-users
                  view-clients
                  view-identity-providers
                  view-realm
                  view-users
                ],
              },
            },
          }
        }

        let(:policy_datas) {
          {
            superadmin_group: {
              name: 'superadmin-user',
              description: 'es admin a nivel de realm',
              type: 'group',
              logic: 'POSITIVE',
              decisionStrategy: 'UNANIMOUS',
              groups: [Keycloak::Entity::Group.find_by(search: 'superadmin', realm: client.realm, admin_token: client.send(:admin_token))].map{ |group| {id: group.id, extendChildren: false} }
            },
            client_admin_group: {
              name: 'admin-user',
              description: 'es admin a nivel de cliente',
              type: 'group',
              logic: 'POSITIVE',
              decisionStrategy: 'UNANIMOUS',
              groups: [Keycloak::Entity::Group.find_by(search: "#{client.clientId}-admin", realm: client.realm, admin_token: client.send(:admin_token))].map{ |group| {id: group.id, extendChildren: false} }
            },
            client_group: {
              name: 'client-user',
              description: 'es cliente de la api',
              type: 'group',
              logic: 'POSITIVE',
              decisionStrategy: 'UNANIMOUS',
              groups: [Keycloak::Entity::Group.find_by(search: client.clientId, realm: client.realm, admin_token: client.send(:admin_token))].map{ |group| {id: group.id, extendChildren: false} }
            },
          }
        }

        let(:permission_datas) {
          {
            default: {
              "name": "default",
              "description": "permisos por defecto",
              "type": "scope",
              "logic": "POSITIVE",
              "decisionStrategy": "AFFIRMATIVE",
            }
          }
        }

        context 'functional authorization feature' do

          context '01 resource-server basic settings' do
            context 'when value of serviceAccountsEnabled and authorizationServicesEnabled attributes are true' do
              # in the authorization flow this value allow returns true when one of the permissions over resource returns valid
              # the opposite is unanimous value
              let(:strategy) { 'AFFIRMATIVE' }

              # in the creation of a client you have to set the right resource-server settings
              it 'you have to set the resource-server basic settings' do
                client.resource_server.decisionStrategy = strategy
                client.resource_server.update
                expect(client.resource_server.decisionStrategy).to eq strategy
              end
            end
          end


          context '02 enable token exchange' do
            let(:scope) { 'token-exchange' }
            let(:policy_name) { 'internal-target-client-exchange' }
            let(:realm_management_client) {
              Keycloak::Entity::Client.find_by(
                clientId: 'realm-management',
                realm: client.realm,
                admin_token: client.send(:admin_token)
              )
            }
            let(:scoped_permission_id) { client.available_realm_management_permissions['scopePermissions'][scope] }
            let(:scoped_permission) {
              # fake values to avoid validations and make a show request
              Keycloak::Entity::Permission.new(
                id: scoped_permission_id,
                name: 'fake', type: 'fake', logic: 'fake',
                decisionStrategy: 'fake',
                realm: realm,
                client: realm_management_client
              )&.reload
            }

            it 'you have to enable realm-management permissions \
                and select token-exchange permission id' do
              expect(scoped_permission_id).not_to be_nil
            end

            it 'you have to search scoped permission entity for realm-management client' do
              expect(scoped_permission).to be
            end

            it 'you have to udate scoped permission' do
              datas = scoped_permission.to_h
              datas[:policies] = [policy_name]
              expect{ scoped_permission.update(datas) }.to change{ scoped_permission.to_h }
            end

          end

          # we need a basic set of scopes
          context '03 creation of scopes' do

            default_scope.each_with_index do |http_verb|
              it "you have to create #{http_verb} scope" do
                expect{
                  Keycloak::Entity::Scope.create(
                    name: http_verb, realm: client.realm, client: client, admin_token: client.send(:admin_token)
                  )
                }.to change{ client.scopes.count }.by 1
              end
            end
          end

          # we need add domain to uris because keycloak doesnt and include default scopes
          context '04 update default resource' do
            let(:uris) {
              uri = URI.parse client.baseUrl
              ["#{uri.host}/", "#{uri.host}/*"]
            }
            let(:resource) { client.resources.first }

            it 'you have to add the client domain to the uris' do
              resource.uris = uris
              resource.update
              expect(resource.reload.uris).to eq uris
            end

            it 'you have to add defaul scope' do
              default_scope.each do |http_verb|
                Keycloak::Entity::Scope.create(name: http_verb, realm: client.realm, client: client, admin_token: client.send(:admin_token))
              end

              resource.scopes = default_scope
              expect{ resource.update }.to change{ resource.reload.scopes }
            end
          end

          context '05 creation of a groups' do
            let(:group) { Keycloak::Entity::Group.create(group_datas[:group]) }

            after {
              group.delete
            }

            context 'client group' do
              it 'you have to create a client group' do
                expect(group).not_to be_nil
              end

              # group creation not save roles
              context 'you have to add group roles' do
                it 'from realm' do
                  available_realm_roles = group.realm_roles(available: true)
                  available_realm_roles.each do |available_role|
                    available_role.join_group if group_roles[:group][:realmRoles].include? available_role.name
                  end

                  group.reload
                  expect(group.realmRoles).not_to be_empty
                end

                it 'from clients' do
                  group_roles[:group][:clientRoles].each do |client_name, roles|
                    client = Keycloak::Entity::Client.find_by(clientId: client_name, realm: realm, admin_token: realm.send(:admin_token))
                    available_client_roles = group.client_roles(available: true, client: client)
                    available_client_roles.each do |available_role|
                      if roles.include? available_role.name
                        available_role.join_group
                        group.reload
                        expect(group.clientRoles[client_name.to_s]).not_to be_empty
                      end
                    end
                  end
                end
              end

              context 'subgroups' do
                context 'client-admin' do
                  it 'you have to create a admin subgroup into the client group' do
                    subgroup = Keycloak::Entity::Group.new(group_datas[:subgroup])
                    group.add_subgroup(subgroup: subgroup)
                    expect(group.subGroups).to include subgroup.to_h
                  end

                  context 'you have to add group roles' do
                    let(:subgroup) {
                      subgroup = Keycloak::Entity::Group.new(group_datas[:subgroup])
                      group.add_subgroup(subgroup: subgroup)
                      group.subgroups.first.reload
                    }

                    it 'from realm' do
                      available_realm_roles = subgroup.realm_roles(available: true)
                      available_realm_roles.each do |available_role|
                        available_role.join_group if group_roles[:subgroup][:realmRoles].include? available_role.name
                      end

                      subgroup.reload
                      expect(subgroup.realmRoles).not_to be_empty
                    end

                    it 'from clients' do
                      group_roles[:subgroup][:clientRoles].each do |client_name, roles|
                        client = Keycloak::Entity::Client.find_by(clientId: client_name, realm: realm, admin_token: realm.send(:admin_token))
                        available_client_roles = subgroup.client_roles(available: true, client: client)
                        available_client_roles.each do |available_role|
                          if roles.include? available_role.name
                            available_role.join_group
                            subgroup.reload
                            expect(subgroup.clientRoles[client_name.to_s]).not_to be_empty
                          end
                        end
                      end
                    end
                  end
                end
              end

            end
          end

          context '06 add client roles to gatekeeper group' do
            let(:client_roles) { %w[uma_protection] }
            let(:gatekeeper_client_group) {
              Keycloak::Entity::Group.find_by(
                search: 'gatekeeper',
                realm: realm,
                admin_token: realm.send(:admin_token)
              )
            }

            it 'from client' do
              available_client_roles = gatekeeper_client_group.client_roles(available: true, client: client)
              available_client_roles.each do |available_role|
                if client_roles.include? available_role.name
                  available_role.join_group
                  gatekeeper_client_group.reload
                  expect(gatekeeper_client_group.clientRoles[client.name]).not_to be_empty
                end
              end
            end
          end

          context '07 create new client roles' do
            new_roles = %w[public]

            new_roles.each do |role|
              before { client }
              it "should create #{role} new role" do
                expect{
                  Keycloak::Entity::ClientRole.create(name: role, client: client, realm: realm, admin_token: client.send(:admin_token))
                }.to change{ client.roles.count }.by(1)
              end
            end
          end

          context '08 add public role to default group' do
            let(:client_roles) { %w[public] }
            let(:default_client_group) {
              Keycloak::Entity::Group.find_by(
                search: 'default',
                realm: realm,
                admin_token: realm.send(:admin_token)
              )
            }
            before {
              client_roles.each do |role|
                Keycloak::Entity::ClientRole.create(name: role, client: client, realm: realm, admin_token: client.send(:admin_token))
              end
            }

            it 'from client' do
              available_client_roles = default_client_group.client_roles(available: true, client: client)
              available_client_roles.each do |available_role|
                if client_roles.include? available_role.name
                  available_role.join_group
                  default_client_group.reload
                  expect(default_client_group.clientRoles[client.name]).not_to be_empty
                end
              end
            end
          end

          context '09 creation of default policies' do
            let(:group) { Keycloak::Entity::Group.create(group_datas[:group]) }
            let(:subgroup) {
              subgroup = Keycloak::Entity::Group.new(group_datas[:subgroup])
              group.add_subgroup(subgroup: subgroup)
              group.subgroups.first.reload
            }
            before {
              group
              subgroup
            }
            after {
              group.delete
            }

            default_policies.each do |type|
              it "you have to create #{type} policy" do
                policy = Keycloak::Entity::Policy.create(policy_datas[type].merge(realm: client.realm, client: client, admin_token: client.send(:admin_token)))
                expect(policy.id).not_to be_nil
              end
            end

          end

          context '10 creation of default permission' do
            # we remove default permission because it is wrong type
            it 'you have to delete previous default permission' do
              default_permission = client.permissions.first
              expect{ default_permission.delete }.to change{ client.permissions }
            end

            it 'you have to create a new default permission' do
              # default_scopes ready!
              default_scope.each do |http_verb|
                Keycloak::Entity::Scope.create(name: http_verb, realm: client.realm, client: client, admin_token: client.send(:admin_token))
              end

              # default_resource ready!
              default_resource = client.resources.first
              default_resource.scopes = client.scopes.map(&:name)
              default_resource.update

              # groups ready!
              group = Keycloak::Entity::Group.create(group_datas[:group])
              subgroup = Keycloak::Entity::Group.new(group_datas[:subgroup])
              group.add_subgroup(subgroup: subgroup)
              group.reload
              subgroup.reload

              # policies ready!
              client.policies.first.delete
              default_policies.each do |type|
                Keycloak::Entity::Policy.create(policy_datas[type].merge(realm: client.realm, client: client, admin_token: client.send(:admin_token)))
              end

              permission = permission_datas[:default]
              permission[:scopes]    = client.scopes.map(&:name)
              permission[:policies]  = client.policies.map(&:name)
              permission[:resources] = [default_resource.name]
              expect{
                Keycloak::Entity::Permission.create(
                  permission.merge(realm: client.realm, client: client, admin_token: client.send(:admin_token))
                )
              }.to change{ client.permissions }

              # after
              group.delete
            end
          end
        end
      end
    end
  end

end