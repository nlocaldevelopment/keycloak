RSpec.shared_examples 'disabled method' do |options|

  describe "#{options[:symbol]}#{options[:method]}" do
    subject { eval(options[:method].to_s) }

    case options[:type]
    when :hash
      it { is_expected.to be_a Hash }
      it { is_expected.to be_empty }
    when :array
      it { is_expected.to be_a Array }
      it { is_expected.to be_empty }
    else
      it { is_expected.to be_nil }
    end
  end

end