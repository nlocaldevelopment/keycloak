RSpec.shared_examples 'included modules' do |options|

    [options[:modules]].flatten.each do |mod|
      it "#{mod} should is included" do
        reference = subject.respond_to?(:new) ? subject : subject.class
        expect(reference.ancestors).to include Kernel.const_get(mod)
      end
    end

end