RSpec.shared_examples 'custom errors' do |options|

    describe 'custom errors' do
      [options[:errors]].flatten.each do |error|
        context error do
          it { is_expected.to be }
        end
      end
    end

end