RSpec.shared_examples 'public methods' do |options|

    context 'methods' do
      [options[:methods]].flatten.each do |mthd|
        it { is_expected.to respond_to mthd }
      end
    end

end