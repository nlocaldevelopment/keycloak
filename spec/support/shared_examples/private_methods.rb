RSpec.shared_examples 'private methods' do |options|

    context 'methods' do
      [options[:methods]].flatten.each do |mthd|
        it { is_expected.to respond_to_private mthd }
      end
    end

end