RSpec.shared_examples 'authorization module' do |options|

  context 'authorization module' do

    # module dependencies
    before {
      options[:klass].include(Keycloak::Api).include(Keycloak::TokenHandler)
      options[:klass].send(:enable_token,:token)
    }

    subject { options[:klass].include(Keycloak::Authorization) }

    # custom errors
    include_examples 'custom errors', errors: [
      Keycloak::Authorization::Error
    ]


    context 'class' do

      # libs
      include_examples 'included modules', modules: %w{
        Keycloak::Api
      }

      # public interface
      describe 'public interface' do

        include_examples 'public methods', {
          methods: %i{
          }
        }

        # behaviour pact
        context 'behaviour pact' do

            describe '.' do
            end

        end

      end

      # private interface
      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            enable_authorization
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '.enable_authorization' do

            context 'for a client entity' do
              class ClientDump; end

              # module dependencies
              before {
                ClientDump.include(Keycloak::Api).include(Keycloak::TokenHandler)
              }

              subject(:client) {
                ClientDump.include(Keycloak::Authorization).send(:enable_authorization,:client).new
              }

              it { is_expected.to respond_to :authorized? }
            end

            context 'for a user entity' do
              class UserDump; end

              # module dependencies
              before {
                UserDump.include(Keycloak::Api).include(Keycloak::TokenHandler)
              }

              subject(:user) {
                UserDump.include(Keycloak::Authorization).send(:enable_authorization,:user).new
              }

              it { is_expected.to respond_to :authorized? }
            end

            context 'for a resource entity' do
              class ResourceDump; end

              # module dependencies
              before {
                ResourceDump.include(Keycloak::Api).include(Keycloak::TokenHandler)
              }

              subject(:resource) {
                ResourceDump.include(Keycloak::Authorization).send(:enable_authorization,:user).new
              }

              it { is_expected.to respond_to :authorized? }
            end

          end

        end

      end

    end if options[:load].include? :extend

    context 'instance'  do

      subject(:instance) {
        options[:klass].send(:enable_authorization,:client).new
      }

      # public interface
      describe 'public interface' do

        include_examples 'public methods', {
          methods: %i{
            authorization_request
          }
        }

        # behaviour pact
        context 'behaviour pact' do
          let!(:resource) { :box }
          let!(:url) { 'http://localhost:80' }
          let!(:token) { OpenStruct.new(JSON.parse(build(:client_token_request, :success).body)) }
          let!(:client_credentials) { { grant_type: 'client_credentials', client_id: 'api' } }

          before {
            allow(instance).to receive(:token_endpoint).and_return(url)
            allow(instance).to receive(:client_credentials).and_return(client_credentials)
          }

          describe '#authorized?' do

            context 'with an invalid request' do
              before { allow(instance).to receive(:authorization_request).and_raise(Keycloak::Authorization::Error) }

              it 'should raise an error' do
                expect{ instance.authorized? }.to raise_error Keycloak::Authorization::Error
              end
            end

            context 'with an authorized request' do
              before { allow(instance).to receive(:authorization_request).and_return(OpenStruct.new(code: '200')) }

              it 'should return true' do
                expect(instance.authorized?(resource: resource)).to eq true
              end
            end

            context 'with a unauthorized request' do
              before { allow(instance).to receive(:authorization_request).and_return(OpenStruct.new(code: '403')) }

              it 'should return false' do
                expect(instance.authorized?(resource: resource)).to eq false
              end
            end

          end

          describe '#authorization_request' do

            context 'without certs' do
              it 'should raise an error' do
                expect{ instance.authorization_request(resource: resource, access_token: token.access_token) }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'with certs' do
              before {
                allow(instance).to receive(:algorithm).and_return('algorithm')
                allow(instance).to receive(:public_key).and_return('public_key')
              }

              context 'without resource param' do
                it 'should raise an error' do
                  expect{ instance.authorization_request }.to raise_error Keycloak::Authorization::Error
                end
              end

              context 'with resource param' do

                context 'without access token' do
                  it 'should raise an error' do
                    expect{ instance.authorization_request(resource: resource) }.to raise_error Keycloak::Authorization::Error
                  end
                end

                context 'with access token' do

                  context 'with invalid request' do
                    it 'should raise an error' do
                      stub_request(:post, instance.send(:token_endpoint)).to_return({status: 500})
                      expect{ instance.authorization_request(resource: resource, access_token: token.access_token) }.to raise_error Keycloak::Authorization::Error
                    end
                  end

                  context 'with valid request' do

                    context 'with an authorized request' do
                      before { stub_request(:post, instance.send(:token_endpoint)).to_return({status: 200}) }
                      subject { instance.authorization_request(resource: resource, access_token: token.access_token) }

                      it { is_expected.to be_a Net::HTTPOK }
                      it 'is expected to return 200 code' do
                        expect(subject.code).to eq '200'
                      end
                    end

                    context 'with a unauthorized request' do
                      before { stub_request(:post, instance.send(:token_endpoint)).to_return({status: 403}) }
                      subject { instance.authorization_request(resource: resource, access_token: token.access_token) }

                      it { is_expected.to be_a Net::HTTPForbidden }
                      it 'is expected to return 403 code' do
                        expect(subject.code).to eq '403'
                      end
                    end

                  end

                end

              end

            end

          end

        end

      end

      # private interface
      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            algorithm
            token_endpoint
            client_credentials
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '#' do
          end

        end

      end

    end if options[:load].include? :include

  end

end