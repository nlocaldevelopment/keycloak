RSpec.shared_examples 'token_handler module' do |options|

  context 'token_handler module' do

    # module dependencies
    before {
      options[:klass].include(Keycloak::Api)
    }

    subject { options[:klass].include Keycloak::TokenHandler }

    # custom errors
    include_examples 'custom errors', errors: [
      Keycloak::TokenHandler::Error
    ]

    context 'class' do

      # libs
      include_examples 'included modules', modules: 'Keycloak::Api'

      # public interface
      describe 'public interface' do

        include_examples 'public methods', {
          methods: %i{
          }
        }

        # behaviour pact
        context 'behaviour pact' do

            describe '.' do
            end

        end

      end

      # private interface
      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            enable_token
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '.enable_token' do
            context 'token feature only' do
              class ADump; end
              before {
                ADump.include(Keycloak::Api)
                ADump.include(Keycloak::TokenHandler).send(:enable_token,:token)
              }

              context 'class methods' do
                subject { ADump }

                it { is_expected.to respond_to :build_from_token }
              end

              context 'instance methods' do
                subject { ADump.new }

                it { is_expected.to respond_to :request_token }
                it { is_expected.to respond_to :refresh_token }
                it { is_expected.to respond_to :token_introspection }
                it { is_expected.to respond_to :logout }
                it { is_expected.not_to respond_to :exchange_token }
                it { is_expected.not_to respond_to :external_provider_token }
                it { is_expected.not_to respond_to :impersonate }
              end
            end

            context 'exchange feature only' do
              class BDump; end
              before {
                BDump.include(Keycloak::Api)
                BDump.include(Keycloak::TokenHandler).send(:enable_token,:exchange)
              }

              context 'class methods' do
                subject { BDump }

                it { is_expected.not_to respond_to :build_from_token }
              end

              context 'instance methods' do
                subject { BDump.new }

                it { is_expected.not_to respond_to :request_token }
                it { is_expected.not_to respond_to :refresh_token }
                it { is_expected.not_to respond_to :token_introspection }
                it { is_expected.not_to respond_to :logout }
                it { is_expected.to respond_to :exchange_token }
                it { is_expected.to respond_to :external_provider_token }
                it { is_expected.not_to respond_to :impersonate }
              end
            end

            context 'exchange feature only' do
              class CDump; end
              before {
                CDump.include(Keycloak::Api)
                CDump.include(Keycloak::TokenHandler).send(:enable_token,:impersonation)
              }

              context 'class methods' do
                subject { CDump }

                it { is_expected.not_to respond_to :build_from_token }
              end

              context 'instance methods' do
                subject { CDump.new }

                it { is_expected.not_to respond_to :request_token }
                it { is_expected.not_to respond_to :refresh_token }
                it { is_expected.not_to respond_to :token_introspection }
                it { is_expected.not_to respond_to :logout }
                it { is_expected.not_to respond_to :exchange_token }
                it { is_expected.not_to respond_to :external_provider_token }
                it { is_expected.to respond_to :impersonate }
              end
            end

          end

        end

      end

    end if options[:load].include? :extend

    # instance
    context 'instance' do

      subject(:instance) { options[:klass].send(:enable_token,[:token,:exchange,:impersonation]).new options[:params] }

      # libs
      include_examples 'included modules', modules: %w{
        Keycloak::Api
      }

      # public interface
      describe 'public interface' do

        include_examples 'instance attributes', {
          accessor: true,
          attributes: %i{
            token
          }
        }

        include_examples 'public methods', {
          methods: %i{
            request_token
            refresh_token
            logout
            token_introspection

            exchange_token
            external_provider_token

            impersonate
          }
        }

        # behaviour pact
        describe 'behaviour pact' do

          let!(:url) { 'http://localhost:80' }
          let!(:access_token) { 'token' }
          let!(:client_credentials) { { grant_type: 'client_credentials', client_id: 'api' } }

          describe '#request_token' do

            before do
              allow(instance).to receive(:token_endpoint).and_return(url)
              allow(instance).to receive(:entity_credentials).and_return(client_credentials)
            end

            context 'with valid request' do
              it 'should set a token' do
                stub_request(:post, instance.send(:token_endpoint)).to_return(build(:client_token_request, :success))
                expect{ instance.request_token }.to change(instance,:token)
                expect(instance.token).to be_a OpenStruct
              end
            end

            context 'with invalid request' do
              it 'should raise an error' do
                stub_request(:post, instance.send(:token_endpoint)).to_return(build(:client_token_request, :fail))
                expect{ instance.request_token }.to raise_error Keycloak::TokenHandler::Error
              end
            end

          end

          describe '#refresh_token' do

            before do
              allow(instance).to receive(:token_endpoint).and_return(url)
              allow(instance).to receive(:client_credentials).and_return(client_credentials)
            end

            context 'without token' do
              it 'should raise an error' do
                expect{ instance.refresh_token }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'with token' do
              before { instance.token = OpenStruct.new(JSON.parse(build(:client_token_request, :success).body).merge(access_token: access_token)) }

              context 'with valid request' do
                it 'should set a new token' do
                  stub_request(:post, instance.send(:token_endpoint)).to_return(build(:client_token_request, :success))
                  expect{ instance.refresh_token }.to change(instance,:token)
                  expect(instance.token).to be_a OpenStruct
                end
              end

              context 'with invalid request' do
                it 'should raise an error' do
                  stub_request(:post, instance.send(:token_endpoint)).to_return(build(:client_token_request, :fail))
                  expect{ instance.refresh_token }.to raise_error Keycloak::TokenHandler::Error
                end
              end

            end

          end

          describe '#logout' do

            before do
              allow(instance).to receive(:end_session_endpoint).and_return(url)
              allow(instance).to receive(:client_credentials).and_return(client_credentials)
            end

            context 'without token' do
              it 'should raise an error' do
                expect{ instance.logout }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'with token' do
              before { instance.token = OpenStruct.new(JSON.parse(build(:client_token_request, :success).body).merge(access_token: access_token)) }

              context 'with valid request' do
                it 'should not raise an error' do
                  stub_request(:post, instance.send(:end_session_endpoint)).to_return(OpenStruct.new(status: 204, body: ''))
                  expect{ instance.logout }.not_to raise_error
                end
              end

              context 'with invalid request' do
                it 'should raise an error' do
                  stub_request(:post, instance.send(:end_session_endpoint)).to_return(OpenStruct.new(status: 500, body: ''))
                  expect{ instance.logout }.to raise_error Keycloak::TokenHandler::Error
                end
              end

            end

          end

          describe '#token_introspection' do
            let(:rsa_private) { OpenSSL::PKey::RSA.generate 2048 }
            let(:rsa_public) { rsa_private.public_key }
            let(:algorithm) { 'RS256' }
            let(:token) {
              JWT.encode({ data: {}, iss: 'http://internal.com', exp: Time.now.to_i + 60 }, rsa_private, 'RS256')
            }

            context 'without token' do
              it 'should raise an error' do
                expect{ instance.token_introspection }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'with token' do
              before { instance.token = OpenStruct.new(access_token: token) }

              context 'without algorithm' do
                before { allow(instance).to receive(:algorithm).and_return(algorithm) }

                it 'should raise an error' do
                  allow(instance).to receive(:algorithm).and_return(nil)
                  expect{ instance.token_introspection }.to raise_error Keycloak::TokenHandler::Error
                end

                context 'without public_key' do
                  it 'should raise an error' do
                    allow(instance).to receive(:public_key).and_return(nil)
                    expect{ instance.token_introspection }.to raise_error Keycloak::TokenHandler::Error
                  end
                end
              end


              context 'with public_key and algorithm' do
                before do
                  allow(instance).to receive(:algorithm).and_return(algorithm)
                  allow(instance).to receive(:public_key).and_return(rsa_public)
                end

                context 'invalid' do

                  context 'token type' do
                    it 'should return an error' do
                      instance.token.access_token = '123'
                      expect{ instance.token_introspection(type: :validate) }.to raise_error Keycloak::TokenHandler::Error
                    end
                  end

                  context 'token signature' do
                    it 'should return an error' do
                      rsa_private = OpenSSL::PKey::RSA.generate 2048
                      rsa_public = rsa_private.public_key
                      allow(instance).to receive(:public_key).and_return(rsa_public)
                      expect{ instance.token_introspection(type: :validate) }.to raise_error Keycloak::TokenHandler::Error
                    end
                  end
                end

                describe 'valid' do

                  context 'with type :deserialize' do
                    it 'should return deserializated token' do
                      expect(instance.token_introspection).to be_a HashWithIndifferentAccess
                    end
                  end

                  context 'with type :origin' do

                    context 'when issuer is equal' do
                      before { allow(instance).to receive(:issuer).and_return('http://internal.com') }
                      subject { instance.token_introspection(type: :origin) }

                      it { is_expected.to be_a Symbol }
                      it { is_expected.to eq :internal }
                    end

                    context 'when issuer is different' do
                      before { allow(instance).to receive(:issuer).and_return('http://external.com') }
                      subject { instance.token_introspection(type: :origin) }

                      it { is_expected.to be_a Symbol }
                      it { is_expected.to eq :external }
                    end

                  end

                  context 'with type :state' do

                    context 'when token is expired' do
                      before {
                        token = JWT.encode({ data: {}, iss: 'http://internal.com', exp: Time.now.to_i - 60 }, rsa_private, 'RS256')
                        instance.token = OpenStruct.new(access_token: token)
                      }
                      subject { instance.token_introspection(type: :state) }

                      it { is_expected.to be_a Symbol }
                      it { is_expected.to eq :expired }
                    end

                    context 'when token is active' do
                      before { allow(instance).to receive(:issuer).and_return('http://external.com') }
                      subject { instance.token_introspection(type: :state) }

                      it { is_expected.to be_a Symbol }
                      it { is_expected.to eq :active }
                    end

                  end

                  context 'with type :entity' do
                    context 'when is a user token' do
                      before { allow_any_instance_of(HashWithIndifferentAccess).to receive(:[]).with(:clientId).and_return(nil) }
                      subject { instance.token_introspection(type: :entity) }

                      it { is_expected.to be_a Symbol }
                      it { is_expected.to eq :user }
                    end

                    context 'when is a client token' do
                      before { allow_any_instance_of(HashWithIndifferentAccess).to receive(:[]).with(:clientId).and_return('test') }
                      subject { instance.token_introspection(type: :entity) }

                      it { is_expected.to be_a Symbol }
                      it { is_expected.to eq :client }
                    end
                  end
                end

              end

            end

          end

          describe '#exchange_token' do

            before do
              allow(instance).to receive(:token_endpoint).and_return(url)
              allow(instance).to receive(:client_credentials).and_return(client_credentials)
            end

            context 'when token is undefined' do
              it 'should raise an error' do
                expect{ instance.exchange_token }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'when token is defined' do
              before {
                instance.token = OpenStruct.new(JSON.parse(build(:user_token_request, :success).body))
              }

              context 'from internal provider' do
                before { allow(instance).to receive(:token_introspection).with(type: :origin).and_return(:internal) }

                context 'with invalid request' do
                  it 'should raise an error' do
                    stub_request(:post, instance.send(:token_endpoint)).to_return(build(:user_token_exchange_request, :fail))
                    expect{ instance.exchange_token }.to raise_error Keycloak::TokenHandler::Error
                  end
                end

                context 'with valid request' do
                  it 'should change token and call #mutate_entity' do
                    stub_request(:post, instance.send(:token_endpoint)).to_return(build(:user_token_exchange_request, :success))
                    expect(instance).to receive(:mutate_entity).once
                    expect{ instance.exchange_token(audience: 'app') }.to change(instance,:token)
                  end
                end

              end

              context 'from external provider' do
                before { allow(instance).to receive(:token_introspection).with(type: :origin).and_return(:external) }

                context 'with invalid request' do
                  it 'should raise an error' do
                    stub_request(:post, instance.send(:token_endpoint)).to_return(build(:user_token_exchange_request, :fail))
                    expect{ instance.exchange_token }.to raise_error Keycloak::TokenHandler::Error
                  end
                end

                context 'with valid request' do
                  it 'should change token and call #mutate_entity' do
                    stub_request(:post, instance.send(:token_endpoint)).to_return(build(:user_token_exchange_request, :success))
                    expect(instance).to receive(:mutate_entity).once
                    expect{ instance.exchange_token }.to change(instance,:token)
                  end
                end

              end

            end

          end

          describe '#external_provider_token' do
            let(:provider) { :facebook }

            before do
              allow(instance).to receive(:token_endpoint).and_return(url)
              allow(instance).to receive(:token_provider_endpoint).and_return(url)
              allow(instance).to receive(:client_credentials).and_return(client_credentials)
            end

            context 'when token is undefined' do
              it 'should raise an error' do
                expect{ instance.external_provider_token }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'when token is defined' do
              let(:token) { OpenStruct.new(JSON.parse(build(:user_token_request, :success).body)) }
              before {
                instance.token = token
              }

              include_examples 'mandatory attributes', {
                method: :external_provider_token,
                attributes: %i{ target },
                params: { target: :facebook },
                error: 'Keycloak::TokenHandler::Error'
              }

              context 'when makes a default request' do

                context 'with invalid request' do
                  it 'should raise an error' do
                    stub_request(:get, instance.send(:token_provider_endpoint)).to_return(build(:user_token_exchange_request, :fail))
                    expect{ instance.external_provider_token(target: provider) }.to raise_error Keycloak::TokenHandler::Error
                  end
                end

                context 'with valid request' do
                  it 'should return a stored token version' do
                    stub_request(:get, instance.send(:token_provider_endpoint)).to_return(build(:user_token_exchange_request, :success))
                    token = instance.external_provider_token(target: provider)
                    expect(token).to be_a OpenStruct
                    expect(token).to respond_to :access_token
                  end
                end

              end

              context 'when request live version' do
                it 'should return a stored token version' do
                  stub_request(:post, instance.send(:token_endpoint)).to_return(build(:user_token_exchange_request, :success))
                  token = instance.external_provider_token(target: provider, live: true)
                  expect(token).to be_a OpenStruct
                  expect(token).to respond_to :access_token
                end
              end

            end

          end

          describe '#impersonate' do

            context 'without requested_subject param' do
              it 'should raise an error' do
                expect{ instance.impersonate }.to raise_error Keycloak::TokenHandler::Error
              end
            end

            context 'with requested_subject param' do
              before {
                allow(instance).to receive(:token_endpoint).and_return(url)
                allow(instance).to receive(:client_credentials).and_return(client_credentials)
              }

              context 'without token' do

                context 'in a direct naked impersonation with client' do

                  context 'with invalid request' do
                    it 'should raise an error' do
                      stub_request(:post, instance.send(:token_endpoint)).to_return(build(:impersonated_user_token_request, :fail))
                      expect{ instance.impersonate(requested_subject: 'username', direct_naked: true) }.to raise_error Keycloak::TokenHandler::Error
                    end
                  end

                  context 'with valid request' do
                    it 'should set a token' do
                      stub_request(:post, instance.send(:token_endpoint)).to_return(build(:impersonated_user_token_request, :success))
                      expect(instance).to receive(:mutate_entity).once
                      expect{ instance.impersonate(requested_subject: 'username', direct_naked: true) }.to change(instance,:token)
                      expect(instance.token).to be_a OpenStruct
                    end
                  end
                end

                context 'in a default impersonation with user' do
                  it 'should raise an error' do
                    expect{ instance.impersonate(requested_subject: 'username') }.to raise_error Keycloak::TokenHandler::Error
                  end
                end

              end

              context 'with token' do
                before { instance.token = OpenStruct.new(JSON.parse(build(:user_token_request, :success).body)) }

                context 'with invalid request' do
                  it 'should raise an error' do
                    stub_request(:post, instance.send(:token_endpoint)).to_return(build(:impersonated_user_token_request, :fail))
                    expect{ instance.impersonate(requested_subject: 'username') }.to raise_error Keycloak::TokenHandler::Error
                  end
                end

                context 'with valid request' do
                  it 'should set a token' do
                    stub_request(:post, instance.send(:token_endpoint)).to_return(build(:impersonated_user_token_request, :success))
                    expect(instance).to receive(:mutate_entity).once
                    expect{ instance.impersonate(requested_subject: 'username') }.to change(instance,:token)
                    expect(instance.token).to be_a OpenStruct
                  end
                end

              end

            end

          end

        end

      end

      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            entity_url
            broker_url
            token_endpoint
            token_provider_endpoint
            end_session_endpoint
            entity_credentials
            client_credentials
            algorithm
            public_key
            issuer
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '#token_provider_endpoint' do
            let!(:provider) { 'facebook' }
            before { expect(instance).to receive(:broker_url).once }
            subject { instance.send(:token_provider_endpoint,provider) }

            it { is_expected.to be_a String }
            it { is_expected.to match(%r{#{provider}}) }
            it { is_expected.to match(%r{token}) }
          end
        end

      end

    end if options[:load].include? :include

  end

end