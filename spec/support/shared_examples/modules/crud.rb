# frozen_string_literal: true

RSpec.shared_examples 'crud module' do |options|

  context 'crud module' do

    subject { options[:klass].include(Keycloak::CRUD) }

    let(:url) { 'http://localhost:80' }
    let(:primary_key) { options[:primary_key] || subject.primary_key }
    let(:search_key) { options[:search_key] || subject.search_key }
    let(:params) { options[:params][:valid] }

    # custom errors
    include_examples 'custom errors', errors: [
      Keycloak::CRUD::Error
    ]

    context 'class' do

      # libs
      include_examples 'included modules', modules: %w{
        Keycloak::Api
        ActiveModel::Validations
      }

      # public interface
      describe 'public interface' do

        before { subject.send(:enable_crud,[:search,:creation]) }

        include_examples 'public methods', {
          methods: %i{
            set_primary_key
            set_search_key

            all
            where
            find_by
            find

            create
          }
        }

        # behaviour pact
        context 'behaviour pact' do
            let(:new_primary_key) { :name }
            let(:new_search_key) { :colour }
            let(:id) { 'linus' }
            let(:search_params) { options[:params][:search] }

            describe '.set_primary_key' do
              it 'should change primary_key' do
                expect{ subject.set_primary_key new_primary_key }.to change(subject,:primary_key)
              end
            end

            describe '.set_search_key' do
              it 'should change search_key' do
                expect{ subject.set_search_key new_search_key }.to change(subject,:search_key)
              end
            end

            describe '.all' do
              it 'should call #where' do
                admin_token = 'token'
                expect(subject).to receive(:where).with(admin_token: admin_token).once
                subject.all(admin_token: admin_token)
              end
            end

            describe '.where' do

              context 'without params attributes' do
                it 'should raise an error' do
                  expect{ subject.where }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with params attributes' do
                let!(:results) { [{ id: 1 },{ id: 2 }] }
                let!(:token) { OpenStruct.new(access_token: 'admin_token') }
                let!(:params) { search_params || {} }
                before {
                  allow(subject).to receive(:search_url).and_return(url) unless search_params
                  stub_request(:get, subject.send(:search_url,search_params)).to_return(body: results.to_json, status: 200)
                }

                context 'without admin_token' do
                  it 'should raise an error' do
                    expect{ subject.where(params) }.to raise_error Keycloak::CRUD::Error
                  end
                end

                context 'with admin_token' do

                  it 'should return array' do
                    expect(subject.where(params.merge(admin_token: token))).to be_a Array
                  end
                end

              end

            end

            describe '.find_by' do

              context 'with invalid param format' do
                it 'should raise an error' do
                  expect{ subject.find_by(1) }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with valid param format' do
                it 'should raise an error when has invalid amount of attributes' do
                  expect{ subject.find_by(id: 1, name: 'test') }.to raise_error Keycloak::CRUD::Error
                end

                it 'should call #where' do
                  query = { id: 1 }
                  expect(subject).to receive(:where).with(query).once.and_return([])
                  subject.find_by(query)
                end

                it 'should ignore parent relations and admin_token in params' do
                  query = { id: 1, realm: build(:realm), admin_token: OpenStruct.new(access_token: 'admin_token') }
                  allow(subject).to receive(:up_associations).and_return(%i{realm})
                  expect(subject).to receive(:where).with(query).once.and_return([])
                  subject.find_by(query)
                end
              end

            end

            describe '.find' do
              it 'should call #find_by primary_key' do
                subject.primary_key = primary_key
                admin_token = 'token'

                expect(subject).to receive(:find_by).with(primary_key => id, admin_token: admin_token).once
                subject.find(key: id, admin_token: admin_token)
              end
            end

            describe '.create' do
              context 'without params' do
                it 'should return an error' do
                  expect{ subject.create }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with params' do
                context 'without admin_token' do
                  it 'should return an error' do
                    expect{ subject.create(params) }.to raise_error Keycloak::CRUD::Error
                  end
                end

                context 'with admin_token' do
                  let!(:token) { OpenStruct.new(access_token: 'admin_token') }

                  context 'with invalid params' do
                    it 'should return an error' do
                      allow_any_instance_of(subject).to receive(:valid?).and_return(false)
                      expect{ subject.create(params.merge(admin_token: token)) }.to raise_error Keycloak::CRUD::Error
                    end
                  end

                  context 'with valid params' do

                    context 'with invalid request' do
                      it 'should raise an error' do
                        allow(subject).to receive(:search_url).and_return(url)
                        stub_request(:post, subject.send(:search_url)).to_return(body: { error: 'invalid' }.to_json, status: 500)
                        expect{ subject.create(params.merge(admin_token: token)) }.to raise_error Keycloak::CRUD::Error
                      end
                    end

                    context 'with valid request' do
                      let(:response_body) { subject.new(params).to_h.to_json }

                      it 'should create a new record and call record#reload' do
                        allow(subject).to receive(:search_url).and_return(url) unless params[:realm]
                        stub_request(:post, subject.send(:search_url,params.merge(subject.primary_key => nil))).to_return(body: response_body, status: 201)

                        expect(subject).to receive(:post).and_call_original
                        expect_any_instance_of(subject).to receive(:reload)
                        subject.create(params.merge(admin_token: token))
                      end
                    end

                  end
                end

              end

            end

        end

      end

      # private interface
      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            enable_crud
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '.enable_crud' do
            context 'search feature only' do
              class ADump; end

              subject(:adump) { ADump.include(Keycloak::CRUD)}
              before { adump.send(:enable_crud,:search) }

              it { is_expected.to respond_to :all }
              it { is_expected.to respond_to :where }
              it { is_expected.to respond_to :find_by }
              it { is_expected.to respond_to :find }
              it { is_expected.not_to respond_to :create }
            end

            context 'creation feature only' do
              class BDump; end

              subject(:bdump) { BDump.include(Keycloak::CRUD) }
              before { bdump.send(:enable_crud,:creation) }

              it { is_expected.not_to respond_to :all }
              it { is_expected.not_to respond_to :where }
              it { is_expected.not_to respond_to :find_by }
              it { is_expected.not_to respond_to :find }
              it { is_expected.to respond_to :create }
            end

          end
        end

      end

    end if options[:load].include? :extend

    context 'instance' do

      subject(:instance) { options[:klass].new(params) }

      # public interface
      describe 'public interface' do

        before { instance.class.send(:enable_crud,[:update,:delete,:reload]) }

        include_examples 'public methods', {
          methods: %i{
            save
            update
            delete
            reload
          }
        }

        # behaviour pact
        describe 'behaviour pact' do
          let(:attribute) { :attr }
          let(:attributes) { { attribute => 'value' } }
          let(:token) { OpenStruct.new(access_token: 'admin_token') }

          before {
            allow(instance).to receive(:entity_url).and_return(url)
          }

          describe '#save' do

            context 'when instance is invalid' do
              before{ allow(instance).to receive(:valid?).and_return(false) }

              it 'should raise an error' do
                expect{ instance.save }.to raise_error Keycloak::CRUD::Error
              end
            end

            context 'when instance is valid' do

              context 'without admin_token' do
                before { allow(instance).to receive(:admin_token).and_return(nil) }

                it 'should raise an error' do
                  expect{ instance.save }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with admin_token' do
                before {
                  allow(instance).to receive(:admin_token).and_return(token)
                  allow(options[:klass]).to receive(:create).and_return(instance)
                }

                it 'should call instance class.create method' do
                  expect(options[:klass]).to receive(:create)
                  instance.save
                end
              end

            end

          end

          describe '#update' do

            context 'without admin_token' do
              it 'should raise an error' do
                expect{ instance.update }.to raise_error Keycloak::CRUD::Error
              end
            end

            context 'with admin_token' do
              before { allow(instance).to receive(:admin_token).and_return(token) }

              context 'without primary_key' do
                it 'should raise an error' do
                  allow(instance).to receive(primary_key).and_return(nil)
                  expect{ instance.update }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with invalid params' do
                it 'should return an error' do
                  allow_any_instance_of(subject.class).to receive(:valid?).and_return(false)
                  expect{ subject.update params.merge(attributes) }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with valid params' do

                context 'with primary_key' do
                  context 'with invalid request' do
                    it 'should raise an error' do
                      stub_request(:put, instance.send(:entity_url)).to_return(body: { error: 'invalid' }.to_json, status: 500)
                      expect{ instance.update params.merge(attributes) }.to raise_error Keycloak::CRUD::Error
                    end
                  end

                  context 'with valid request' do
                    it 'should update instance' do
                      stub_request(:put, instance.send(:entity_url)).to_return(status: 204)
                      expect{ instance.update params.merge(attributes) }.to change(instance,attribute)
                    end
                  end

                end

              end

            end

          end

          describe '#delete' do
            before { instance.class.set_primary_key primary_key }

            context 'without admin_token' do
              it 'should raise an error' do
                expect{ instance.delete }.to raise_error Keycloak::CRUD::Error
              end
            end

            context 'with admin_token' do
              before { allow(instance).to receive(:admin_token).and_return(token) }

              context 'without primary_key' do
                it 'should raise an error' do
                  allow(instance).to receive(primary_key).and_return(nil)
                  expect{ instance.delete }.to raise_error Keycloak::CRUD::Error
                end
              end

              context 'with id' do

                context 'with invalid request' do
                  it 'should raise an error' do
                    stub_request(:delete, instance.send(:entity_url)).to_return(body: { error: 'invalid' }.to_json, status: 500)
                    expect{ instance.delete }.to raise_error Keycloak::CRUD::Error
                  end
                end

                context 'with valid request' do
                  it 'should update instance' do
                    stub_request(:delete, instance.send(:entity_url)).to_return(status: 204)
                    expect{ instance.delete }.to change(instance,primary_key)
                  end
                end

              end

            end

          end

          describe '#reload' do
            before { instance.class.set_search_key search_key }

            context 'without admin_token' do
              it 'should raise an error' do
                expect{ instance.reload }.to raise_error Keycloak::CRUD::Error
              end
            end

            context 'with admin_token' do
              before { allow(instance).to receive(:admin_token).and_return(token) }

              context 'without primary_key' do
                before { allow(instance).to receive(primary_key).and_return(nil) }

                context 'when search_key and primary_key are equal' do
                  it 'should raise an error' do
                    allow(instance.class).to receive(:search_key).and_return(primary_key)
                    expect{ instance.reload }.to raise_error Keycloak::CRUD::Error
                  end
                end

                context 'when search_key and primary_key are diferent' do
                  before { allow(instance.class).to receive(search_key).and_return(search_key) }

                  context 'without search_key' do
                    it 'should raise an error' do
                      allow(instance).to receive(search_key).and_return(nil)
                      expect{ instance.reload }.to raise_error Keycloak::CRUD::Error
                    end
                  end

                  context 'with search_key' do

                    context 'when record doesnt exist' do
                      it 'should raise an error' do
                        allow(instance.class).to receive(:find_by).and_return(nil)
                        expect{ instance.reload }.to raise_error Keycloak::CRUD::Error
                      end
                    end

                    context 'when record exists' do
                      before do
                        if instance.class.primary_key == instance.class.search_key
                          instance.class.set_search_key :random_key
                        end
                        instance.send(:config_with,{ data: instance.to_h.merge(primary_key => nil, instance.class.search_key => 'value') })

                        allow(instance.class).to receive(:find_by).and_return(OpenStruct.new(primary_key => 1))
                        stub_request(:get, instance.send(:entity_url)).to_return(body: {}.to_json, status: 200)
                      end

                      context 'when model has parent relations' do
                        it 'should add them to search params' do
                          realm = build(:realm)
                          allow(instance.class).to receive(:up_associations).and_return(%i{realm})
                          allow(instance).to receive(:realm).and_return(realm)
                          expect(instance.class).to receive(:find_by).once.with(instance.class.search_key => instance.send(instance.class.search_key),:realm => realm, :admin_token => token, :configuration => realm.configuration)
                          instance.reload
                        end
                      end

                      it 'should set primary_key record' do
                        allow(instance).to receive(primary_key).and_call_original
                        expect{ instance.reload }.to change(instance,primary_key)
                      end
                    end

                  end

                end

              end

              context 'with primary_key' do
                context 'with invalid request' do
                  it 'should raise an error' do
                    stub_request(:get, instance.send(:entity_url)).to_return(body: { error: 'invalid' }.to_json, status: 500)
                    expect{ instance.reload }.to raise_error Keycloak::CRUD::Error
                  end
                end

                context 'with valid request' do
                  let(:response_body) { instance.class.new(params.merge(attributes)).to_h.to_json }

                  it 'should reload instance' do
                    stub_request(:get, instance.send(:entity_url)).to_return(body: response_body,status: 200)
                    expect{ instance.reload }.to change(instance,attribute)
                  end
                end

              end

            end

          end

        end

      end

      # private interface
      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            entity_url
            config_with
            admin_token
          }
        }

        # behaviour pact
        describe 'behaviour pact' do

          describe '#config_with' do

            context 'with invalid data format' do
              it 'should raise an error' do
                expect { instance.send(:config_with, data: 'invalid datas') }.to raise_error Keycloak::CRUD::Error
              end
            end

            context 'with empty data' do
              it 'should return any changes' do
                expect{ instance.send(:config_with) }.not_to change(instance, :token_endpoint)
              end
            end

            context 'with datas' do
              it 'should set new datas' do
                expect { instance.send(:config_with, data: { public_key: 'key' }) }.to change(instance,:public_key)
              end
            end

          end if options[:klass].is_a? OpenStruct

        end

      end

    end if options[:load].include? :include

  end

end