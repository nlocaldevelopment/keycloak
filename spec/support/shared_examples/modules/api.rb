RSpec.shared_examples 'api module' do |options|

  context 'api module' do

    subject {
      options[:klass].send(options[:load],Keycloak::Api)
      options[:load] == :include ? options[:klass].new(options[:params]) : options[:klass]
    }

    # custom errors
    include_examples 'custom errors', errors: [
      Keycloak::Api::Error
    ]

    # public interface
    describe 'public interface' do

      include_examples 'public methods', {
        methods: %i{
          get
          post
          put
          del
        }
      }

      # behaviour pact
      context 'behaviour pact' do

        let!(:msg) { 'hello world!' }

        describe 'get' do

          params = {
            url: FFaker::Internet.http_url,
            token: 'token'
          }

          include_examples 'mandatory attributes', {
            method: :get,
            attributes: %i{ url },
            params: params,
            error: 'Keycloak::Api::Error'
          }

          it 'should make a simple request' do
            stub_request(:get, params[:url]).to_return(body: msg, status: '200')
            expect(subject.get(params)).to be
            expect(subject.get(params).body).to eq msg
          end

        end

        describe 'post' do

          params = {
            url: FFaker::Internet.http_url,
            form: { attr: 0 },
            token: 'token'
          }

          include_examples 'mandatory attributes', {
            method: :post,
            attributes: %i{ url form },
            params: params,
            error: 'Keycloak::Api::Error'
          }

          it 'should make a simple request' do
            stub_request(:post, params[:url]).to_return(body: msg, status: '200')
            expect(subject.post(params)).to be
            expect(subject.post(params).body).to eq msg
          end

        end

        describe 'put' do

          params = {
            url: FFaker::Internet.http_url,
            form: { attr: 0 },
            token: 'token'
          }

          include_examples 'mandatory attributes', {
            method: :put,
            attributes: %i{ url form },
            params: params,
            error: 'Keycloak::Api::Error'
          }

          it 'should make a simple request' do
            stub_request(:put, params[:url]).to_return(body: msg, status: '200')
            expect(subject.put(params)).to be
            expect(subject.put(params).body).to eq msg
          end

        end

        describe 'del' do

          params = {
            url: FFaker::Internet.http_url,
            token: 'token'
          }

          include_examples 'mandatory attributes', {
            method: :del,
            attributes: %i{ url },
            params: params,
            error: 'Keycloak::Api::Error'
          }

          it 'should make a simple request' do
            stub_request(:delete, params[:url]).to_return(body: msg, status: '200')
            expect(subject.del(params)).to be
            expect(subject.del(params).body).to eq msg
          end

        end

      end

    end

  end

end