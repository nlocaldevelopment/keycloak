RSpec.shared_examples 'relations module' do |options|

  context 'relations module' do

    # module dependencies
    before {
      options[:klass].include(Keycloak::CRUD)
    }

    # custom errors
    include_examples 'custom errors', errors: [
      Keycloak::Relations::Error
    ]

    context 'class' do


      subject { options[:klass].include Keycloak::Relations }

      # libs
      include_examples 'included modules', modules: %w{
        Keycloak::CRUD
      }

      # public interface
      describe 'public interface' do

        include_examples 'public methods', {
          methods: %i{
            has_many
            belongs_to
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '.has_many' do
            before { expect(subject).to receive(:build_children_relation).once }

            context 'without alias' do
              it 'should increase relations registry and call .build_children_relation method' do
                expect{ subject.has_many :childrens }.to change(subject, :down_associations)
              end
            end

            context 'with alias' do
              it 'should increase relations and aliases registry and call .build_children_relation method' do
                expect{ subject.has_many :childrens, as: :groups }.to change(subject, :down_associations).and change(subject, :aliases)
              end
            end

          end

          describe '.belongs_to' do

            it 'should increase belongs registry and call .build_parent_relation method' do
              expect(subject).to receive(:build_parent_relation).once
              expect{ subject.belongs_to :parent }.to change(subject, :up_associations)
            end

          end

        end

      end

      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            build_children_relation
            build_parent_relation
            add_entity
            add_aliase
          }
        }

        # behaviour pact
        context 'behaviour pact' do

          describe '.build_children_relation' do

            context 'without relation' do
              it 'should not define new methods' do
                expect(subject).not_to receive(:define_method)
                subject.send(:build_children_relation)
              end
            end

            context 'with relation' do
              let(:relation) { :childrens }

              it 'should define new relation method' do
                expect(subject).to receive(:define_method).with(relation)
                subject.send(:build_children_relation,relation)
              end
            end

          end

          describe '.buil_parent_relation' do

            context 'without relation' do
              it 'should not define parent methods' do
                expect(subject).not_to receive(:define_method)
                subject.send(:build_parent_relation)
              end
            end

            context 'with relation' do
              let(:relation) { :parent }

              it 'should define parent method' do
                expect(subject).to receive(:define_method).with(relation).once
                subject.send(:build_parent_relation,relation)
              end
            end

          end

          describe '.add_entity' do
            params = { reference: 'members', entity: 'users' }

            include_examples 'mandatory attributes', {
              method: :add_entity,
              attributes: %i{ reference entity },
              params: params,
              error: 'Keycloak::Relations::Error'
            }

            it 'should set entity for relation' do
              expect{ subject.send(:add_entity,params) }.to change(subject,:entities)
            end

          end

          describe '.add_aliase' do
            params = { reference: 'members', aliase: 'users' }

            include_examples 'mandatory attributes', {
              method: :add_aliase,
              attributes: %i{ reference aliase },
              params: params,
              error: 'Keycloak::Relations::Error'
            }

            it 'should set alias for relation' do
              expect{ subject.send(:add_aliase,params) }.to change(subject,:aliases)
            end

          end

        end

      end

    end if options[:load].include? :extend

    context 'instance' do

      subject(:instance) { options[:klass].new }

      describe 'private' do

        include_examples 'private methods', {
          methods: %i{
            resource_url
            build_resource
          }
        }

        describe '#build_resource' do

          context 'with a undefined entity' do
            it 'should return an instance of OpenStruct' do
              expect(instance.send(:build_resource, { type: :parent })).to be_a OpenStruct
            end
          end

          context 'with a defined entity' do
            it 'should return an instance of entity' do
              user = build(:user)
              info = user.to_h.merge(realm: user.realm)
              expect(instance.send(:build_resource, { type: :user, info: info})).to be_a Keycloak::Entity::User
            end
          end

        end

      end

    end if options[:load].include? :include

  end

end