RSpec.shared_examples 'delegated methods' do |options|

    describe "delegated methods" do

      options[:delegators].keys.each do |model|

        context "#{model}" do

          options[:delegators][model].each do |mthd|

            it "##{mthd} should be delegated " do
              expect(subject.send(model)).to receive(mthd).once
              subject.send(mthd)
            end

          end

        end

      end

    end

end