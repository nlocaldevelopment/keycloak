RSpec.shared_examples 'mandatory attributes' do |options|

  context 'without mandatory attributes' do

    options[:attributes].each do |param|
      it ":#{param} should return an error" do
        options[:params].delete(param)
        expect{ subject.send(options[:method],options[:params]) }.to raise_error Kernel.const_get(options[:error])
      end
    end

  end

end