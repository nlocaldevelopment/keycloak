RSpec.shared_examples 'instance attributes' do |options|

    describe "#attributes #{options[:accessor] ? 'read/write' : 'read'}" do
      [options[:attributes]].flatten.each do |attribute|

        it { is_expected.to respond_to attribute }
        it { is_expected.to respond_to "#{attribute}=" } if options[:accessor]

      end
    end

end