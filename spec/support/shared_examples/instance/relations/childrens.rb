RSpec.shared_examples 'childrens' do |options|

    [options[:relations]].flatten.each do |relation|
      describe "##{relation}" do
        entity = described_class.entities[relation] || relation
        aliase = described_class.aliases[relation] || relation
        request = aliase.to_s.singularize.to_sym

        context 'with bad request' do
          it 'should return an error' do
            case options[:adapter]
            when :http
              stub_request(:get, instance.send(:resource_url,{ resource: aliase })).to_return(build("#{request}_search_request".to_sym, :fail))
            end

            expect{ subject.send(relation) }.to raise_error Kernel.const_get(options[:error])
          end
        end

        context 'with valid request' do
          it "should return a list of #{entity}" do
            case options[:adapter]
            when :http
              stub_request(:get, instance.send(:resource_url,{ resource: aliase })).to_return(build("#{request}_search_request".to_sym, :success))
            end

            expect(subject.send(relation)).to be_a Array
          end
        end

      end
    end

end