require 'vcr'

VCR.configure do |c|
  c.cassette_library_dir = 'spec/cassettes'
  # c.default_cassette_options = { record: :all } # enable to generate new cassettes
  # c.allow_http_connections_when_no_cassette = true # enable when code new tests with requests
  c.hook_into :webmock
  c.configure_rspec_metadata!
end