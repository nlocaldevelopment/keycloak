require 'keycloak'

Keycloak.configure do |config|
  config.admin_client_id = 'realm-management'
  config.default_client_id = 'gatekeeper'
  config.admin_username = 'superadmin'
end