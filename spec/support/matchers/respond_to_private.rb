RSpec::Matchers.define :respond_to_private do |expected|
  match do |actual|
    actual.respond_to? expected, true
  end
  description do
    prefix = actual.is_a?(Class) ? '.' : '#'
    "respond to #{prefix}#{expected}"
  end
end