RSpec.shared_context 'initializer' do |options|

  before do
    VCR.use_cassette('default configuration') do
      Keycloak.configure do |config|
        build(:configuration, options[:configuration]).to_h.each do |attr,value|
          config.send("#{attr}=",value)
        end
      end
    end
  end

  let!(:realm) { Keycloak.default_realm }
  let!(:client) { Keycloak::Entity::Client.new(clientId: 'api', secret: '9c43404c-6762-402a-9fd6-7a3c69c9db48', realm: realm).reload }
  let!(:admin_token) { realm.send(:admin_token) }

end
