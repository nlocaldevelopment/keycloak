RSpec.shared_context 'authenticated user' do |options|

  include_context 'initializer', configuration: options[:configuration] do
    # client info inherit from gatekeeper service develop
    let(:request_uri) { URI.parse "http://#{options[:audience]}:80#{options[:resource_path]}?client_id=#{options[:audience]}&client_secret=#{options[:audience_secret]}" }
    let!(:params) { HashWithIndifferentAccess.new(URI::decode_www_form(request_uri.query).to_h) }

    let(:token) do
      user = Keycloak::Entity::User.new(options[:user].merge(options[:credentials]).merge(client: Keycloak.default_client, realm: Keycloak.default_realm))
      user.request_token.token
    end
    let!(:user) do
      # to avoid raise token expired error
      allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).and_call_original
      allow_any_instance_of(Keycloak::Entity::Realm).to receive(:token_introspection).with(access_token: token.access_token, type: :state).and_return(:active)
      Keycloak::Entity::User.build_from_token(token: token, audience: params[:client_id])
    end
  end

end
