FactoryBot.define do
  factory :associatedPolicy_show_request, class: OpenStruct do

    trait :success do
      realm = FactoryBot.build(:realm).id
      body {
        {
          "id": SecureRandom.uuid,
          "name": "superadmin-user",
          "description": "es admin a nivel de realm",
          "type": "group",
          "logic": "POSITIVE",
          "decisionStrategy": "UNANIMOUS",
          "config": {}
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end