FactoryBot.define do
  factory 'identity-provider_show_request'.to_sym, class: OpenStruct do

    trait :success do
      body {
        {
          "alias": "facebook",
          "internalId": SecureRandom.uuid,
          "providerId": "facebook",
          "enabled": true,
          "updateProfileFirstLoginMode": "on",
          "trustEmail": true,
          "storeToken": true,
          "addReadTokenRoleOnCreate": true,
          "authenticateByDefault": false,
          "linkOnly": false,
          "firstBrokerLoginFlowAlias": "first broker login",
          "config": {
              "hideOnLoginPage": "",
              "acceptsPromptNoneForwardFromClient": "",
              "clientId": "1027032561000431",
              "disableUserInfo": "",
              "clientSecret": "**********",
              "defaultScope": "ads_management, ads_read, business_management, email, leads_retrieval, manage_pages",
              "useJwksUrl": "true"
          }
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end