FactoryBot.define do
  factory :stored_token_request, class: OpenStruct do

    trait :facebook do
      body {
        {
          "access_token": "EAAOmFKb4nZB8BABwp9ycZAZBolZBBrx7cjLo6em6RZC9prKWZAdanZAZAzcxH6ucb0pV0a2oUZB6B2qfgjHPs1vD6mU8KUn7htRZChoMTovZB7O1CZAdEZAyi7FqvH1n7oswZAF51dz5xm1ZBtOjMlpsGwGTD246AfRasIs3v6AXQOfAJIWdxc6obh18Sk2MVxVTXITZBkAZD",
          "token_type": "bearer",
          "expires_in": 5183999
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end