FactoryBot.define do
  factory :group_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "id": SecureRandom.uuid,
          "name": "api",
          "path": "/api",
          "attributes": {},
          "realmRoles": [],
          "clientRoles": {},
          "subGroups": [
              {
                  "id": SecureRandom.uuid,
                  "name": "api-admin",
                  "path": "/api/admin",
                  "attributes": {},
                  "realmRoles": [],
                  "clientRoles": {
                      "realm-management": [
                          "impersonation"
                      ],
                      "api": [
                          "uma_protection"
                      ]
                  },
                  "subGroups": []
              }
          ],
          "access": {
              "view": true,
              "manage": true,
              "manageMembership": true
          }
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end