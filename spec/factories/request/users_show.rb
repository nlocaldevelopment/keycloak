FactoryBot.define do
  factory :user_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "id": SecureRandom.uuid,
          "createdTimestamp": 1567606575419,
          "username": "admin",
          "enabled": true,
          "totp": false,
          "emailVerified": false,
          "firstName": "admin",
          "lastName": "api",
          "email": "admin@publicar.com",
          "disableableCredentialTypes": [
              "password"
          ],
          "requiredActions": [],
          "notBefore": 0,
          "access": {
              "manageGroupMembership": true,
              "view": true,
              "mapRoles": true,
              "impersonate": true,
              "manage": true
          }
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end