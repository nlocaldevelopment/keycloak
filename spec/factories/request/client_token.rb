FactoryBot.define do
  factory :client_token_request, class: OpenStruct do

    trait :success do
      body {
        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIRDlmZGtNWXlCbFE4OTVZWDFBZU9IT09NZ2EwQlVOLVJYQlNlUW9xNlZBIn0.eyJqdGkiOiI3Mjc4NWFlYy0wMzk1LTRkNDUtOTU0Yy03ZmUzODhkMGNkZjYiLCJleHAiOjE1NzE4NDA3NzQsIm5iZiI6MCwiaWF0IjoxNTcxODQwNDc0LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjoiYXBpIiwic3ViIjoiYTVjZGEyZjMtMmQ3Ny00ZTdjLWFiZWEtMWIyZTdhZmY5N2I2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYXBpIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiZjcxNjU4M2EtNzkzNC00MWE1LTk1OGItNjJjOGY3ZDMxYzI2IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhcGkiOnsicm9sZXMiOlsicHVibGljIiwidW1hX3Byb3RlY3Rpb24iXX19LCJzY29wZSI6InByb2ZpbGUgYXBpIGVtYWlsIiwiY2xpZW50SWQiOiJhcGkiLCJjbGllbnRIb3N0IjoiMTcyLjE4LjAuMSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoic2VydmljZS1hY2NvdW50LWFwaSIsImNsaWVudEFkZHJlc3MiOiIxNzIuMTguMC4xIiwiZW1haWwiOiJzZXJ2aWNlLWFjY291bnQtYXBpQHBsYWNlaG9sZGVyLm9yZyJ9.nKZRXhPTsvxLVwJAqxLbCHYbCWUfa5R1ACVTbTdCLGMyJXJW0JyYDNex1z7gDpjAhHeun1hDQhNErIg8KXrNxo2vLyCxdP-jLXEh5jqy0EtLDe0GzniSumRV0hZJOiUAmhhqOSkXAcyMCJvzZxnN-9q3Z6VSxmHYMrg3y4-FZWT4s5UcvWqpjux4Y7EpvNBeScpUs5cuv9lOuYfSC31vg7Mf_WiBmb6AlM0sDn6fgr-PLjckxWOtXbqqdbpbSMVMRt13S13DiV6orgwXkVRZGXSgmidPARfBMi0p2LfID9PJfVGK7BOKd68CZsr5x3u_yd78K3kL2hVPeSEGnguUJQ",
          "expires_in": 300,
          "refresh_expires_in": 300,
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkYzA5NThkOC1iMzllLTQxMTQtYTIyZi1jZTkzN2ExOTkyODMifQ.eyJqdGkiOiIxYjlkZWJkYS0yMzcxLTRlODQtYTM2ZS04MDc3OGZjNTE4YTAiLCJleHAiOjE1NzE4NDA3NzQsIm5iZiI6MCwiaWF0IjoxNTcxODQwNDc0LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjoiaHR0cDovL2tleWNsb2FrOjgwODAvYXV0aC9yZWFsbXMvZGVtbyIsInN1YiI6ImE1Y2RhMmYzLTJkNzctNGU3Yy1hYmVhLTFiMmU3YWZmOTdiNiIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJhcGkiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJmNzE2NTgzYS03OTM0LTQxYTUtOTU4Yi02MmM4ZjdkMzFjMjYiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFwaSI6eyJyb2xlcyI6WyJwdWJsaWMiLCJ1bWFfcHJvdGVjdGlvbiJdfX0sInNjb3BlIjoicHJvZmlsZSBhcGkgZW1haWwifQ.Wfx4EynFhWZHHacUQkNoYxu-SVfOhORHZ094ShhJS-4",
          "token_type": "bearer",
          "not-before-policy": 1566559179,
          "session_state": SecureRandom.uuid,
          "scope": "profile api email"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end