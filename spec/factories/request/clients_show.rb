FactoryBot.define do
  factory :client_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "id": SecureRandom.uuid,
          "clientId": "gatekeeper",
          "name": "Rails API",
          "description": "Basic Rails API",
          "rootUrl": "http://api",
          "adminUrl": "",
          "baseUrl": "/",
          "surrogateAuthRequired": false,
          "enabled": true,
          "clientAuthenticatorType": "client-secret",
          "redirectUris": [
              "/*"
          ],
          "webOrigins": [
              ""
          ],
          "notBefore": 0,
          "bearerOnly": false,
          "consentRequired": false,
          "standardFlowEnabled": true,
          "implicitFlowEnabled": true,
          "directAccessGrantsEnabled": true,
          "serviceAccountsEnabled": true,
          "authorizationServicesEnabled": true,
          "publicClient": false,
          "frontchannelLogout": false,
          "protocol": "openid-connect",
          "attributes": {
              "saml.assertion.signature": "false",
              "saml.force.post.binding": "false",
              "saml.multivalued.roles": "false",
              "saml.encrypt": "false",
              "saml.server.signature": "false",
              "saml.server.signature.keyinfo.ext": "false",
              "exclude.session.state.from.auth.response": "false",
              "saml_force_name_id_format": "false",
              "saml.client.signature": "false",
              "tls.client.certificate.bound.access.tokens": "false",
              "saml.authnstatement": "false",
              "display.on.consent.screen": "false",
              "pkce.code.challenge.method": "",
              "saml.onetimeuse.condition": "false"
          },
          "authenticationFlowBindingOverrides": {},
          "fullScopeAllowed": false,
          "nodeReRegistrationTimeout": -1,
          "protocolMappers": [
              {
                  "id": SecureRandom.uuid,
                  "name": "Client IP Address",
                  "protocol": "openid-connect",
                  "protocolMapper": "oidc-usersessionmodel-note-mapper",
                  "consentRequired": false,
                  "config": {
                      "user.session.note": "clientAddress",
                      "userinfo.token.claim": "true",
                      "id.token.claim": "true",
                      "access.token.claim": "true",
                      "claim.name": "clientAddress",
                      "jsonType.label": "String"
                  }
              },
              {
                  "id": SecureRandom.uuid,
                  "name": "Client Host",
                  "protocol": "openid-connect",
                  "protocolMapper": "oidc-usersessionmodel-note-mapper",
                  "consentRequired": false,
                  "config": {
                      "user.session.note": "clientHost",
                      "userinfo.token.claim": "true",
                      "id.token.claim": "true",
                      "access.token.claim": "true",
                      "claim.name": "clientHost",
                      "jsonType.label": "String"
                  }
              },
              {
                  "id": SecureRandom.uuid,
                  "name": "Client ID",
                  "protocol": "openid-connect",
                  "protocolMapper": "oidc-usersessionmodel-note-mapper",
                  "consentRequired": false,
                  "config": {
                      "user.session.note": "clientId",
                      "userinfo.token.claim": "true",
                      "id.token.claim": "true",
                      "access.token.claim": "true",
                      "claim.name": "clientId",
                      "jsonType.label": "String"
                  }
              }
          ],
          "defaultClientScopes": [
              "web-origins",
              "role_list",
              "roles",
              "profile",
              "api",
              "email"
          ],
          "optionalClientScopes": [
              "address",
              "phone",
              "offline_access",
              "microprofile-jwt"
          ],
          "access": {
              "view": true,
              "configure": true,
              "manage": true
          }
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end