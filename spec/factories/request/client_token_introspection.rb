FactoryBot.define do
  factory :client_token_introspection_request, class: OpenStruct do

    trait :success do
      body {
        {
          "jti": SecureRandom.uuid,
          "exp": 1574419720,
          "nbf": 0,
          "iat": 1574419121,
          "iss": "http://keycloak:8080/auth/realms/demo",
          "aud": [
              "api",
              "realm-management"
          ],
          "sub": SecureRandom.uuid,
          "typ": "Bearer",
          "azp": "api",
          "auth_time": 0,
          "session_state": SecureRandom.uuid,
          "preferred_username": "service-account-api",
          "email": "service-account-api@placeholder.org",
          "email_verified": false,
          "acr": "1",
          "allowed-origins": [
              ""
          ],
          "realm_access": {
              "roles": [
                  "offline_access",
                  "uma_authorization"
              ]
          },
          "resource_access": {
              "realm-management": {
                  "roles": [
                      "manage-realm",
                      "impersonation",
                      "manage-users",
                      "view-users",
                      "manage-authorization",
                      "query-groups",
                      "query-users"
                  ]
              },
              "api": {
                  "roles": [
                      "public",
                      "uma_protection"
                  ]
              }
          },
          "scope": "profile api email",
          "clientId": "api",
          "clientHost": "172.28.0.1",
          "clientAddress": "172.28.0.1",
          "client_id": "api",
          "username": "service-account-api",
          "active": true
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end