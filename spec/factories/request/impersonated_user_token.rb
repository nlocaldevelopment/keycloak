FactoryBot.define do
  factory :impersonated_user_token_request, class: OpenStruct do

    trait :success do
      body {
        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIRDlmZGtNWXlCbFE4OTVZWDFBZU9IT09NZ2EwQlVOLVJYQlNlUW9xNlZBIn0.eyJqdGkiOiIyZDE4MTFjMi01MGQxLTRlYmItYTlhNy1lZWJkY2QzODdkNmQiLCJleHAiOjE1NzI0NDAzNjksIm5iZiI6MCwiaWF0IjoxNTcyNDQwMDY5LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjoiYXBpIiwic3ViIjoiNjQ3YjdmYmQtMzJlNC00ZWE0LTk5NzQtNzRmYzIzMDY2ODQ4IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYXBpIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiMmFlNWQ3ZWEtZDcwNi00NjFiLWI0YzMtZDk1MDI0NDY3OWJlIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhcGkiOnsicm9sZXMiOlsicHVibGljIiwidW1hX3Byb3RlY3Rpb24iXX19LCJzY29wZSI6InByb2ZpbGUgYXBpIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiYWRtaW4gcmVhbG0iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzdXBlcmFkbWluIiwiZ2l2ZW5fbmFtZSI6ImFkbWluIiwiZmFtaWx5X25hbWUiOiJyZWFsbSIsImVtYWlsIjoic3VwZXJhZG1pbkBwdWJsaWNhci5jb20ifQ.pHs6vlqa54snhj_OcctUc_XummeUbNhGwQfrB15kXu6HN-9E70BNl0aRy5sgWYcu2xHyfMBJdLV4kPVtmtSs3maWBVKoND5v17XzTYyrRMoWbrEI5LaFmazwqiwc8QHYGYgY1QpduwII8RVosRkvUlgZvVyvR8WZQb2qmCkAqYZC10Divuk9bXYSMBMS25m3TUPOgfTNBF5LLhEoppEYsQcbYKWRhwxPvTyAOCbgCMSp8VNscJeXIHiMtEGhT2r0WUjBqx1QvgZy1fpE25uehdBvaOSsSYDoRtWcxqiAfF1XTtqXITg8GxOf-iH731CkUCbZJOM-yjFQwQtYK_Ma5A",
          "expires_in": 300,
          "refresh_expires_in": 0,
          "token_type": "bearer",
          "not-before-policy": 1566559179,
          "session_state": SecureRandom.uuid,
          "scope": "profile api email"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end