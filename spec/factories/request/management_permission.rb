FactoryBot.define do
  factory :management_permission_request, class: OpenStruct do

    trait :valid do
      body {
        {
          enabled: true,
          resource: Random.uuid,
          scopePermissions: {
            view: Random.uuid,
            manage: Random.uuid,
            configure: Random.uuid,
            'map-roles'.to_sym => Random.uuid,
            'map-roles-client-scope'.to_sym => Random.uuid,
            'map-roles-composite'.to_sym => Random.uuid,
            'token-exchange'.to_sym => Random.uuid
          }
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end