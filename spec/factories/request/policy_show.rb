FactoryBot.define do
  factory :policy_show_request, class: OpenStruct do

    trait :success do
      realm = FactoryBot.build(:realm).id
      body {
        {
          "id": SecureRandom.uuid,
          "name": "admin-access",
          "description": "acceso admin",
          "type": "aggregate",
          "logic": "POSITIVE",
          "decisionStrategy": "AFFIRMATIVE",
          "policies": [ SecureRandom.uuid ],
          "config": {}
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end