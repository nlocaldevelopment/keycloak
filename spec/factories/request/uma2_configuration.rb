FactoryBot.define do
  factory :uma2_configuration_request, class: OpenStruct do

    trait :success do
      configuration = Keycloak::Configuration.new
      protocol = configuration.server_protocol
      domain = configuration.server_domain
      port = configuration.server_port
      realm = 'demo'
      body {
        {
          "issuer": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}",
          "authorization_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/protocol/openid-connect/auth",
          "token_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/protocol/openid-connect/token",
          "token_introspection_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/protocol/openid-connect/token/introspect",
          "end_session_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/protocol/openid-connect/logout",
          "jwks_uri": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/protocol/openid-connect/certs",
          "grant_types_supported": [
              "authorization_code",
              "implicit",
              "refresh_token",
              "password",
              "client_credentials"
          ],
          "response_types_supported": [
              "code",
              "none",
              "id_token",
              "token",
              "id_token token",
              "code id_token",
              "code token",
              "code id_token token"
          ],
          "response_modes_supported": [
              "query",
              "fragment",
              "form_post"
          ],
          "registration_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/clients-registrations/openid-connect",
          "token_endpoint_auth_methods_supported": [
              "private_key_jwt",
              "client_secret_basic",
              "client_secret_post",
              "client_secret_jwt"
          ],
          "token_endpoint_auth_signing_alg_values_supported": [
              "RS256"
          ],
          "scopes_supported": [
              "openid",
              "address",
              "api",
              "email",
              "microprofile-jwt",
              "offline_access",
              "phone",
              "profile",
              "roles",
              "web-origins"
          ],
          "resource_registration_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/authz/protection/resource_set",
          "permission_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/authz/protection/permission",
          "policy_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/authz/protection/uma-policy",
          "introspection_endpoint": "#{protocol}://#{domain}:#{port}/auth/realms/#{realm}/protocol/openid-connect/token/introspect"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end