FactoryBot.define do
  factory :user_search_request, class: OpenStruct do

    trait :success do
      body {
        [ ::JSON.parse(FactoryBot.build(:user_show_request, :success).body) ].to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end