FactoryBot.define do
  factory :client_secret_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "type": "secret",
          "value": SecureRandom.uuid
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end