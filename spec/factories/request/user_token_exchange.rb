FactoryBot.define do
  factory :user_token_exchange_request, class: OpenStruct do

    trait :success do
      body {
        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIRDlmZGtNWXlCbFE4OTVZWDFBZU9IT09NZ2EwQlVOLVJYQlNlUW9xNlZBIn0.eyJqdGkiOiJkMTBjMTQxNy01YTNjLTQwY2ItYTFhNS1hZjAzNWM1YmY0ZGQiLCJleHAiOjE1NzQzNTYwODksIm5iZiI6MCwiaWF0IjoxNTc0MzU1NDg5LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjpbImJyb2tlciIsImdhdGVrZWVwZXIiXSwic3ViIjoiNTBhZmFkM2UtNmVmMS00MDIxLWEzNmEtN2IxOTlkMzM0YjRiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZ2F0ZWtlZXBlciIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjU2Y2RjMGFiLTdmZDItNDg4My1iNWRhLWRkNDRiM2ZkZjFmYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiIl0sInJlc291cmNlX2FjY2VzcyI6eyJicm9rZXIiOnsicm9sZXMiOlsicmVhZC10b2tlbiJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoidXNlciBleHRlcm5hbCIsInByZWZlcnJlZF91c2VybmFtZSI6ImV4dGVybmFsQHB1YmxpY2FyLmNvbSIsImdpdmVuX25hbWUiOiJ1c2VyIiwiZmFtaWx5X25hbWUiOiJleHRlcm5hbCIsImVtYWlsIjoiZXh0ZXJuYWxAcHVibGljYXIuY29tIn0.PB7ipd9vdCU9OaBvehMDQDsCkI9dw3L7GgLErxM4CwWcjbSu_AXkgW6jqibbeGjTQhUbXNb-bVARU_WmT4OL1o5VKKmzVTBmmD8pOS66yIbzCTKCxh9HGL1yt6jY3auhonEtrHk63visEDuzgbmrL_oomehRnoLtR614jyyGZbuCaLlOcSQrHd8ICxbU3GW4rRed2z1B4uTwX9PFdVraf96h7aJcSMnUWKULx4-fVNLZ82dwoMrAS78kmRNFc_aCWzLoHgMnUM7E4l-8QlOpZTUZ_mXzpDhY24qnE53rqVGFZxqIJOZFi8tyiiMHng3-Q_TSeDeHc5o6V_5aYAOYZA",
          "expires_in": 600,
          "refresh_expires_in": 300,
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkYzA5NThkOC1iMzllLTQxMTQtYTIyZi1jZTkzN2ExOTkyODMifQ.eyJqdGkiOiI1NmQ2NzkzYS02MTQ3LTQ4NjctYTM0OS05OGMxZWQzYWJjZDMiLCJleHAiOjE1NzQzNTU3ODksIm5iZiI6MCwiaWF0IjoxNTc0MzU1NDg5LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjoiaHR0cDovL2tleWNsb2FrOjgwODAvYXV0aC9yZWFsbXMvZGVtbyIsInN1YiI6IjUwYWZhZDNlLTZlZjEtNDAyMS1hMzZhLTdiMTk5ZDMzNGI0YiIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJnYXRla2VlcGVyIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiNTZjZGMwYWItN2ZkMi00ODgzLWI1ZGEtZGQ0NGIzZmRmMWZjIiwicmVzb3VyY2VfYWNjZXNzIjp7ImJyb2tlciI6eyJyb2xlcyI6WyJyZWFkLXRva2VuIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIn0.FGX7clMQzjsBYEdNQe-w5ynpGXNJQayk7-5-rM0rXQ0",
          "token_type": "bearer",
          "not-before-policy": 1566559179,
          "session_state": SecureRandom.uuid,
          "scope": "profile email"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end