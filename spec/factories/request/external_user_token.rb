FactoryBot.define do
  factory :external_user_token_request, class: OpenStruct do

    trait :success do
      body {
        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI3eFB5M2ZsZHcxd3YxYmpGRHJ2bmE0OEM5UUFOMXdTZUdzaURnWDhvd2FFIn0.eyJqdGkiOiI4NDdlY2ZhMi01Njc1LTQ1MzEtYWJjYi01YzgxNmEwYWU1ZGQiLCJleHAiOjE1Njg2MjYzNDYsIm5iZiI6MCwiaWF0IjoxNTY4NjI2MDQ2LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9leHRlcm5hbCIsInN1YiI6ImZkNzFlZTY5LWJjZjUtNDNiMS1hNzQ4LTNjYTRmNjQ1NjMwOSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImdhdGVrZWVwZXIiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIyZGFiYTBhZS03NWMxLTRlZDktYjczNS1jNjhmOTk2Y2ExYzciLCJhY3IiOiIxIiwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoidXNlciBleHRlcm5hbCIsImF0dHJpYnV0ZXMiOnsiYXR0cmlidXRlIjoidmFsdWUifSwicHJlZmVycmVkX3VzZXJuYW1lIjoiZXh0ZXJuYWwiLCJnaXZlbl9uYW1lIjoidXNlciIsImZhbWlseV9uYW1lIjoiZXh0ZXJuYWwiLCJlbWFpbCI6ImV4dGVybmFsQHB1YmxpY2FyLmNvbSJ9.ctH-ycSNsBlG7dBQ4V3PwW1CbP2jPF1k-bF2XyR4KRmQ9ywH0vOOa5yS32goWLALGeaKh9xTgfPhwukr-dyhrBhaXfecQtmYuGJ-CPCYSfHRtDBS9bA3XT4VT_hRSsqgAne3vPTKBRAIVDKorJiWUgPtvBwqty_NiYiqlcKPeYkhsUPGjYw_N46NAly9On-_uUvgBqpVsSucoWaPvngwCYgF_z1XeIJUmZWVgWc-ZuzAJzytFO5GW_cVJvM079EcSwmlQXLSPgxQJ6aH3OvXXcnlIJf_YGyvVS2_xfTDJgnfcjkHMDalxopC_dsNWtBwo0Zy4fLJaLSUQ2IQaPNM5Q",
          "expires_in": 300,
          "refresh_expires_in": 1800,
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJhNGZkZGIyNi1kZTc0LTQxZDgtYWExNi1kM2Y4ZjJmMGZkMzgifQ.eyJqdGkiOiJjNzZmMGM2Ny1iZDgwLTQyNTktYjdjMy1jNmIyZGNjNzNhZjAiLCJleHAiOjE1Njg2Mjc4NDYsIm5iZiI6MCwiaWF0IjoxNTY4NjI2MDQ2LCJpc3MiOiJodHRwOi8va2V5Y2xvYWs6ODA4MC9hdXRoL3JlYWxtcy9leHRlcm5hbCIsImF1ZCI6Imh0dHA6Ly9rZXljbG9hazo4MDgwL2F1dGgvcmVhbG1zL2V4dGVybmFsIiwic3ViIjoiZmQ3MWVlNjktYmNmNS00M2IxLWE3NDgtM2NhNGY2NDU2MzA5IiwidHlwIjoiUmVmcmVzaCIsImF6cCI6ImdhdGVrZWVwZXIiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIyZGFiYTBhZS03NWMxLTRlZDktYjczNS1jNjhmOTk2Y2ExYzciLCJzY29wZSI6InByb2ZpbGUgZW1haWwifQ.URjk8cawLARh2zfOOzs7v6aiSik92VCYpSYXKMDonZg",
          "token_type": "bearer",
          "not-before-policy": 1567675666,
          "session_state": "2daba0ae-75c1-4ed9-b735-c68f996ca1c7",
          "scope": "profile email"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end