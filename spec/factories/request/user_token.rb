FactoryBot.define do
  factory :user_token_request, class: OpenStruct do

    trait :success do
      body {
        {
          "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJIRDlmZGtNWXlCbFE4OTVZWDFBZU9IT09NZ2EwQlVOLVJYQlNlUW9xNlZBIn0.eyJqdGkiOiI3ZDdhZDI0MC1hYWQwLTRjNDUtYjk3Yi0wNzBhMWE2MmE2Y2YiLCJleHAiOjE1NzQzNDczNjUsIm5iZiI6MCwiaWF0IjoxNTc0MzQ2NzY1LCJpc3MiOiJodHRwczovL2tleWNsb2FrLmNvbS9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjoiYnJva2VyIiwic3ViIjoiZDdhNmI0ZTgtNDAxZi00MzFiLThjMGItZTRhYjBjMGM1ZjFmIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiZ2F0ZWtlZXBlciIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjBhZTU4NWEzLTA5OTYtNDI4Zi04NTVhLWE3NzYzNWQ0ZDg3MSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiIl0sInJlc291cmNlX2FjY2VzcyI6eyJicm9rZXIiOnsicm9sZXMiOlsicmVhZC10b2tlbiJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6ImNsaWVudCBhcGkiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjbGllbnQiLCJnaXZlbl9uYW1lIjoiY2xpZW50IiwiZmFtaWx5X25hbWUiOiJhcGkiLCJlbWFpbCI6ImNsaWVudEBwdWJsaWNhci5jb20ifQ.Xne9Xdga7vgKFcHxj7TTrsddWYWFk82uDOPhqoHVIs6swj8Jgoq3c0N7LSLdnABw9JK8FRCWuNyOvqeQEk4lqfr98OnlmYzJ6No8bSGB7ct_utRZSteFH1KvxaP4F0XwsdGa6cN2BGLa1hSI44cSkyZV3RtZ2jF0hCDPW1zDuwrhyNA_zGN8fDHm5M0mQdL0O26w3bMwylFLVWUOO2kDE2EKsTbmfLVZzdgxZY6BIcQo-iZoU_fixpEP3LYo5rdRhTejMiR9ztr3UPoC6ncRRCgWW0vqZ_1KLWDUs9nGJcW4wSz5aAQIi48-9Mv2cdfjZvgH9Rb7FNIHD8hLTIMhpA",
          "expires_in": 600,
          "refresh_expires_in": 300,
          "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJkYzA5NThkOC1iMzllLTQxMTQtYTIyZi1jZTkzN2ExOTkyODMifQ.eyJqdGkiOiI4YzQ0MDNhMC0wNDAxLTQ0YmEtOGYwZi1jMjg0MGU2YmVhYTAiLCJleHAiOjE1NzQzNDcwNjUsIm5iZiI6MCwiaWF0IjoxNTc0MzQ2NzY1LCJpc3MiOiJodHRwczovL2tleWNsb2FrLmNvbS9hdXRoL3JlYWxtcy9kZW1vIiwiYXVkIjoiaHR0cHM6Ly9rZXljbG9hay5jb20vYXV0aC9yZWFsbXMvZGVtbyIsInN1YiI6ImQ3YTZiNGU4LTQwMWYtNDMxYi04YzBiLWU0YWIwYzBjNWYxZiIsInR5cCI6IlJlZnJlc2giLCJhenAiOiJnYXRla2VlcGVyIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiMGFlNTg1YTMtMDk5Ni00MjhmLTg1NWEtYTc3NjM1ZDRkODcxIiwicmVzb3VyY2VfYWNjZXNzIjp7ImJyb2tlciI6eyJyb2xlcyI6WyJyZWFkLXRva2VuIl19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIn0.FFd8ZPYOnmhHrnqUYZDzy14Wdx9uCCngKVGMSOa9sBA",
          "token_type": "bearer",
          "not-before-policy": 1568812405,
          "session_state": SecureRandom.uuid,
          "scope": "profile email"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end