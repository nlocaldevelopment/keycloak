FactoryBot.define do
  factory :member_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "id": SecureRandom.uuid,
          "createdTimestamp": 1568029754004,
          "username": "client",
          "enabled": true,
          "totp": false,
          "emailVerified": false,
          "firstName": "client",
          "lastName": "api",
          "email": "client@publicar.com",
          "attributes": {
              "example": [
                  "hola"
              ]
          },
          "disableableCredentialTypes": [],
          "requiredActions": [],
          "notBefore": 1568812405
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end