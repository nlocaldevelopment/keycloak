FactoryBot.define do
  factory 'federated-identity_search_request'.to_sym, class: OpenStruct do

    trait :success do
      body {
        [ ::JSON.parse(FactoryBot.build('federated-identity_show_request'.to_sym, :success).body) ].to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end