FactoryBot.define do
  factory :certs_request, class: OpenStruct do

    trait :success do
      body {
        {
          "keys": [
              {
                  "kid": "HD9fdkMYyBlQ895YX1AeOHOOMga0BUN-RXBSeQoq6VA",
                  "kty": "RSA",
                  "alg": "RS256",
                  "use": "sig",
                  "n": "vlF_rbjDvuujDAqzK4DOlcRqUMDKjKm3py3pzDrlzGrx3yD-M8bWnU6m8dLKdTNiDiKcsAxwIcX1FCITN3SxoXxag88clX22Ri1xMNu4-pTqX0nyoPxspUAq6vRD5IGFe9GQhbSTvC4vCkGoHnfQASVWMYSRsFwosLlXpSTOo4b-qjZ1ctw7H9Itx1MUYyVKc4iyiPUOAuxz3jvVQVePUhf4nybnn68qyCJWKd6HlPBDCj3-obH7CTgfCcfFTE8w_Th8yz9zNJC81GcVmlH6x2nsFPg3Dom4H_4ATCLpw8AqLU2eHgvxpH_PPUyLk7ySg9Z4Bkcx2rbSe7lIOUmgyQ",
                  "e": "AQAB",
                  "x5c": [
                      "MIIClzCCAX8CBgFs9t2mejANBgkqhkiG9w0BAQsFADAPMQ0wCwYDVQQDDARkZW1vMB4XDTE5MDkwMzExMjE1NloXDTI5MDkwMzExMjMzNlowDzENMAsGA1UEAwwEZGVtbzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL5Rf624w77rowwKsyuAzpXEalDAyoypt6ct6cw65cxq8d8g/jPG1p1OpvHSynUzYg4inLAMcCHF9RQiEzd0saF8WoPPHJV9tkYtcTDbuPqU6l9J8qD8bKVAKur0Q+SBhXvRkIW0k7wuLwpBqB530AElVjGEkbBcKLC5V6UkzqOG/qo2dXLcOx/SLcdTFGMlSnOIsoj1DgLsc9471UFXj1IX+J8m55+vKsgiVineh5TwQwo9/qGx+wk4HwnHxUxPMP04fMs/czSQvNRnFZpR+sdp7BT4Nw6JuB/+AEwi6cPAKi1Nnh4L8aR/zz1Mi5O8koPWeAZHMdq20nu5SDlJoMkCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAQhBKiEJr1sl8CH54y36T9Hzem+Ilzc3eO877YKHy6fEMAUaz0BAD2HbiLiR2xc9IDVxN4LrMwdZwpQL2XXxpM95/rj3v4Zw4C1JaOq3Cqv6EsE1nQxyjtJiuFNzsenaG7IaBa9e6Wv1u7N0RKfpl9EC7L4WGKzSuRiyqgGfARImFvc0MbjZsX2lYsAvwjkss2guleMZmZbNvV0TDq9lJtyTLK6ZMebaq/f45H+9mcDnF8DGj5NCyLtoXtWdsfYtSdyuDGLxLuY9jxJKXL5FYSle4MhU1R6tWTugumr/8qdxW8esrRtvyIBuRFOn6i14m00RuQUrID3ljlzvfJdSXGA=="
                  ],
                  "x5t": "6sC3rir0Tec2cnOrf4tKSmcez_0",
                  "x5t#S256": "dBsjQaUqzu9PKGICJb5jRkNHq9f7hbIuAgIv9sVuTpI"
              }
          ]
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end