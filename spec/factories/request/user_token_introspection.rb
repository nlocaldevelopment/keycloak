FactoryBot.define do
  factory :user_token_introspection_request, class: OpenStruct do

    trait :success do
      body {
        {
          "jti": SecureRandom.uuid,
          "exp": 1574348538,
          "nbf": 0,
          "iat": 1574347952,
          "iss": "https://keycloak.com/auth/realms/demo",
          "aud": "api",
          "sub": SecureRandom.uuid,
          "typ": "Bearer",
          "azp": "api",
          "auth_time": 0,
          "session_state": SecureRandom.uuid,
          "name": "client api",
          "given_name": "client",
          "family_name": "api",
          "preferred_username": "client",
          "email": "client@publicar.com",
          "email_verified": false,
          "acr": "1",
          "allowed-origins": [
              ""
          ],
          "realm_access": {
              "roles": [
                  "offline_access",
                  "uma_authorization"
              ]
          },
          "resource_access": {
              "api": {
                  "roles": [
                      "public"
                  ]
              }
          },
          "scope": "profile api email",
          "client_id": "api",
          "username": "client",
          "active": true
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end