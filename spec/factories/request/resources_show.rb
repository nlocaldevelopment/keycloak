FactoryBot.define do
  factory :resource_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "name": "default",
          "type": "urn:app:resources:default",
          "owner": {
              "id": SecureRandom.uuid,
              "name": "api"
          },
          "ownerManagedAccess": false,
          "displayName": "default",
          "attributes": {},
          "_id": SecureRandom.uuid,
          "uris": [
              "/*",
              "/"
          ],
          "scopes": [
              {
                  "id": SecureRandom.uuid,
                  "name": "get"
              },
              {
                  "id": SecureRandom.uuid,
                  "name": "post"
              },
              {
                  "id": SecureRandom.uuid,
                  "name": "delete"
              },
              {
                  "id": SecureRandom.uuid,
                  "name": "patch"
              },
              {
                  "id": SecureRandom.uuid,
                  "name": "put"
              }
          ]
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end