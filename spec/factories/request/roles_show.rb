FactoryBot.define do
  factory :role_show_request, class: OpenStruct do

    trait :success do
      realm = FactoryBot.build(:realm).id
      body {
        {
          "id": SecureRandom.uuid,
          "name": "offline_access",
          "description": "${role_offline-access}",
          "composite": false,
          "clientRole": false,
          "containerId": realm
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end