FactoryBot.define do
  factory :resource_server_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "id": SecureRandom.uuid,
          "clientId": SecureRandom.uuid,
          "name": "api",
          "allowRemoteResourceManagement": true,
          "policyEnforcementMode": "ENFORCING",
          "resources": [],
          "policies": [],
          "scopes": [],
          "decisionStrategy": "AFFIRMATIVE"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end