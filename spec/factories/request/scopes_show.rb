FactoryBot.define do
  factory :scope_show_request, class: OpenStruct do

    trait :success do
      body {
        {
          "id": SecureRandom.uuid,
          "name": "post"
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end