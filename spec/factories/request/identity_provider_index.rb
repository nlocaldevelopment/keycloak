FactoryBot.define do
  factory 'identity-provider/instance_search_request'.to_sym, class: OpenStruct do

    trait :success do
      body {
        [ ::JSON.parse(FactoryBot.build('identity-provider_show_request'.to_sym, :success).body) ].to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end