FactoryBot.define do
  factory :permission_show_request, class: OpenStruct do

    trait :success do
      realm = FactoryBot.build(:realm).id
      body {
        {
          "id": SecureRandom.uuid,
          "name": "default-admin",
          "description": "permisos por defecto para admins",
          "type": "scope",
          "logic": "POSITIVE",
          "decisionStrategy": "UNANIMOUS",
          "policies": [ SecureRandom.uuid ],
          "resources": [ SecureRandom.uuid ],
          "scopes": [ SecureRandom.uuid ],
        }.to_json
      }
      status { 200 }
    end

    trait :fail do
      body { { error: 'failed' }.to_json }
      status { 500 }
    end

  end
end