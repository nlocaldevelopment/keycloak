FactoryBot.define do
  factory :permission, class: Keycloak::Entity::Permission do
    initialize_with do
      client = build(:client)
      new( ::JSON.parse(
        build(:permission_show_request, :success).body
      ).merge(
        client: client,
        realm: client.realm
      ))
    end
  end
end