FactoryBot.define do
  factory :client, class: Keycloak::Entity::Client do
    initialize_with do
      new( ::JSON.parse(
        build(:client_show_request, :success).body
      ).merge(
        realm: build(:realm).send(:config_with, {
          data: ::JSON.parse(
                  build(:openid_configuration_request, :success).body
                ).merge(
                    ::JSON.parse(
                      build(:uma2_configuration_request, :success).body
                    )
                )
        }
      )))
    end
  end
end