FactoryBot.define do
  factory :realm, class: Keycloak::Entity::Realm do
    initialize_with { new(id: FFaker::Product.brand.downcase) }
  end
end