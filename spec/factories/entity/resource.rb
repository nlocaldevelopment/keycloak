FactoryBot.define do
  factory :resource, class: Keycloak::Entity::Resource do
    initialize_with do
      client = build(:client)
      new( ::JSON.parse(
        build(:resource_show_request, :success).body
      ).merge(
        client: client,
        realm: client.realm
      ))
    end
  end
end