FactoryBot.define do
  factory :federated_identity, class: Keycloak::Entity::FederatedIdentity do
    initialize_with do
      user = build(:user)
      new( ::JSON.parse(
        build('federated-identity_show_request'.to_sym, :success).body
      ).merge(
        user: user,
        realm: user.realm
      ) )
    end
  end
end