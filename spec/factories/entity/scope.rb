FactoryBot.define do
  factory :scope, class: Keycloak::Entity::Scope do
    initialize_with do
      client = build(:client)
      new( ::JSON.parse(
        build(:scope_show_request, :success).body
      ).merge(
        client: client,
        realm: client.realm
      ))
    end
  end
end