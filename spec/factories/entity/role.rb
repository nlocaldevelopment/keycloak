FactoryBot.define do
  factory :role, class: Keycloak::Entity::ClientRole do
    initialize_with do
      client = build(:client)
      new( ::JSON.parse(
        build(:role_show_request, :success).body
      ).merge(
        client: client,
        realm: client.realm
      ))
    end
  end
end