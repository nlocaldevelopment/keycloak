FactoryBot.define do
  factory :resource_server, class: Keycloak::Entity::ResourceServer do
    initialize_with do
      client = build(:client)
      new( ::JSON.parse(
        build(:resource_server_show_request, :success).body
      ).merge(
        client: client,
        realm: client.realm
      ))
    end
  end
end