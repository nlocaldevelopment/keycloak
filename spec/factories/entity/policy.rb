FactoryBot.define do
  factory :policy, class: Keycloak::Entity::Policy do
    initialize_with do
      client = build(:client)
      new( ::JSON.parse(
        build(:policy_show_request, :success).body
      ).merge(
        client: client,
        realm: client.realm,
      ))
    end
  end
end