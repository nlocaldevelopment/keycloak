FactoryBot.define do
  factory :identity_provider, class: Keycloak::Entity::IdentityProvider do
    initialize_with do
      new( ::JSON.parse(
        build('identity-provider_show_request'.to_sym, :success).body
      ).merge(
        realm: build(:realm).send(:config_with, {
          data: ::JSON.parse(
                  build(:openid_configuration_request, :success).body
                ).merge(
                    ::JSON.parse(
                      build(:uma2_configuration_request, :success).body
                    )
                )
        }
      )))
    end
  end
end