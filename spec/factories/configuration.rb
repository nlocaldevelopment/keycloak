FactoryBot.define do
  factory :configuration, class: Keycloak::Configuration do
    server_protocol { 'http' }
    server_domain { FFaker::Internet.domain_name }
    default_client_id { 'gatekeeper' }
    default_client_secret { SecureRandom.uuid }

    trait :admin do
      admin_client_id { 'realm-management' }
      admin_client_secret { SecureRandom.uuid }
      admin_username { 'superadmin' }
      admin_password { '1234' }
    end

    trait :without_client do
      default_client_id { nil }
      default_client_secret { nil }
    end

    # configurations inherit from gatekeeper service develop
    trait :demo do
      realm_id { 'demo' }
      admin_client_id { 'realm-management' }
      admin_client_secret { 'd16dbb7d-3568-432c-a068-6950ab55b1c8' }
      admin_username { 'superadmin' }
      admin_password { 'pass' }
      default_client_id { 'gatekeeper' }
      default_client_secret { '5a1cc460-5ba2-4ba2-ad5e-a09979b4fc8f' }
      server_protocol { 'http' }
      server_domain { 'keycloak' }
      server_port { 8080 }
    end

    # configurations inherit from gatekeeper service develop
    trait :external do
      realm_id { 'external' }
      admin_client_id { 'realm-management' }
      admin_client_secret { 'c1e501c6-a814-49b8-b76a-deb5dfa93060' }
      admin_username { 'superadmin' }
      admin_password { 'pass' }
      default_client_id { 'gatekeeper' }
      default_client_secret { '85263e1d-bc7a-4bb8-a654-e3c0eaf0fbd8' }
      server_protocol { 'http' }
      server_domain { 'keycloak' }
      server_port { 8080 }
    end
  end
end