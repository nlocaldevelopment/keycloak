lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'keycloak/version'

Gem::Specification.new do |spec|
  spec.name          = 'keycloak'
  spec.version       = Keycloak::VERSION
  spec.authors       = ['Raul Cabrera']
  spec.email         = ['raul.cabrera@publicar.com']

  spec.summary       = %q{Keycloak adapter for ruby}
  spec.description   = %q{Keycloak adapter for ruby}
  spec.homepage      = 'https://bitbucket.org/nlocaldevelopment/keycloak/src/master'
  spec.license       = 'MIT'

  spec.metadata['allowed_push_host'] = 'TODO: Set to \'http://mygemserver.com\''

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/nlocaldevelopment/keycloak/src/master'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/nlocaldevelopment/keycloak/src/master/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # spec.required_ruby_version = '~> 2.6'

  ## development dependencies
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'guard' # file change observer

  # testing
  spec.add_development_dependency 'rspec' # test framework
  spec.add_development_dependency 'rubocop-rspec' # rspec code analyzer
  spec.add_development_dependency 'guard-rspec' # extension guard
  spec.add_development_dependency 'shoulda-matchers' # collection of testing matchers
  spec.add_development_dependency 'ffaker' # data fakes
  spec.add_development_dependency 'factory_bot' # fixtures plus

  # http request handler
  spec.add_development_dependency 'vcr' # http calls records
  spec.add_development_dependency 'webmock' # mock http request

  # debugger
  spec.add_development_dependency 'pry' # debugger
  spec.add_development_dependency 'pry-doc' # ruby-core docs
  spec.add_development_dependency 'awesome_print' # pretty printer ruby objects

  # code analisys
  spec.add_development_dependency 'rubocop' # ruby code analyzer
  # spec.add_development_dependency 'simplecov' # coverage
  # spec.add_development_dependency 'rubycritic' # quality

  # security
  # spec.add_development_dependency 'bundler-audit' # check gem vulnerabilities

  # documentation
  spec.add_development_dependency 'rake-notes' # get TODO FIXME OPTIMIZE profile
  # spec.add_development_dependency 'yard' # code
  # spec.add_development_dependency 'visualize_ruby' # grap representation of code. require install graphviz

  # show code where refactor
  # spec.add_development_dependency 'mutant-rspec' # mutation tester
  # spec.add_development_dependency 'timecop' # test time-dependent code
  # spec.add_development_dependency 'method_profiler' # profiler ruby objects
  # spec.add_development_dependency 'reek' # code smells
  # spec.add_development_dependency 'flog' # code metric score
  # spec.add_development_dependency 'flay' # structural similarities finder

  ## run time dependencies
  spec.add_runtime_dependency 'activemodel', '>= 5'
  spec.add_runtime_dependency 'jwt', '~> 2'

  ## requirements
  spec.requirements << 'activemodel, >= 5'
  spec.requirements << 'jwt, 2'
end
