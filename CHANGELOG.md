# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.0] - YYYY-MM-DD
### Added
- Something added.

### Changed
- Something changed.

### Removed
- Somethind removed.

[Unreleased]: https://git-server/team/repo/compare/0.0.0...HEAD
[0.0.0]: https://git-server/team/repo/compare/0.0.0/releases/tag/0.0.0