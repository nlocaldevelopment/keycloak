# gems
require 'active_model'
require 'jwt'

# autoload keycloak folder # *****fails with docker hub builds
# path = File.dirname(File.realpath(__FILE__))
# Dir["#{path}/keycloak/**/*.rb"].each do |file|
#   require file
# end

# root
require 'keycloak/api_error'
require 'keycloak/configuration'

# modules
require 'keycloak/modules/api'
require 'keycloak/modules/authorization'
require 'keycloak/modules/crud'
require 'keycloak/modules/relations'
require 'keycloak/modules/token_handler'

# entities
require 'keycloak/entity/base'
require 'keycloak/entity/client'
require 'keycloak/entity/client_role'
require 'keycloak/entity/federated_identity'
require 'keycloak/entity/group'
require 'keycloak/entity/identity_provider'
require 'keycloak/entity/permission'
require 'keycloak/entity/policy'
require 'keycloak/entity/realm'
require 'keycloak/entity/resource'
require 'keycloak/entity/resource_server'
require 'keycloak/entity/scope'
require 'keycloak/entity/user'


module Keycloak
  class Error < ::StandardError; end

  @@configuration  = nil
  @@default_realm  = nil
  @@default_client = nil

  def self.configure
    @@configuration = Configuration.new
    yield @@configuration if block_given?
    @@default_realm = @@configuration.realm_id ? Keycloak::Entity::Realm.new.reload : nil
    @@default_client = ( @@default_realm and @@configuration.default_client_id ) ? Keycloak::Entity::Client.new(clientId: @@configuration.default_client_id, secret: @@configuration.default_client_secret, realm: @@default_realm).reload : nil
  end

  def self.config
    @@configuration
  end

  def self.default_realm
    @@default_realm
  end

  def self.default_client
    @@default_client
  end

end
