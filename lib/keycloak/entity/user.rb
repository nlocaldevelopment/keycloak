# frozen_string_literal: true

module Keycloak
  module Entity
    class User < Base
      # constants
      EMAIL_TYPES = %i[verify_email update_password update_profile]

      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations
      include ::Keycloak::TokenHandler
      include ::Keycloak::Authorization

      # errors
      class Error < ::Keycloak::APIError; end

      # relations
      belongs_to :realm
      belongs_to :client
      has_many :groups
      has_many :federated_identities, as: 'federated-identity'.to_sym

      # C.R.U.D.
      set_search_key :email
      enable_crud :search, :creation, :update, :delete, :reload

      # token handler
      enable_token :token, :exchange, :impersonation

      # authorization
      enable_authorization :user

      # attributes
      attr_accessor :password, :client

      # public interface
      def client=(object)
        @realm  = object.realm
        @client = object
      end

      # custom relation
      def policies(params={})
        raise Error, 'client not defined' unless client
        client.policies(params.merge(name: "#{username}-ownership", type: 'user', permission: false))
      end

      # ownership policy
      def policy
        _policies = policies
        _policies.any? ? _policies.first : ::Keycloak::Entity::Policy.create(name: "#{username}-ownership", description: 'acceso a propietario', type: 'user', logic: 'POSITIVE', decisionStrategy: 'UNANIMOUS', users: [ id ], realm: realm, client: client, admin_token: admin_token, configuration: realm.configuration)
      end

      def join_to_group(group: nil)
        raise Error, 'group not defined' unless group
        form = {
          realm: realm.id,
          userId: id,
          groupId: group
        }
        url = "#{entity_url}/groups/#{group}"
        response = put(url: url, form: form, token: admin_token&.access_token)
        unless response.code.to_i == 204
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        groups
      end

      def leave_a_group(group: nil)
        raise Error, 'group not defined' unless group
        url = "#{entity_url}/groups/#{group}"
        response = del(url: url, token: admin_token&.access_token)
        unless response.code.to_i == 204
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        groups
      end

      def send_emails(types: [], lifespan: (12*60*60), redirect_uri: nil, client_id: nil) # lifespan comes in seconds
        raise Error, 'email types are empty' unless types.any?
        raise Error, 'email types are invalids' unless types.all?{ |t| EMAIL_TYPES.include? t }
        
        redirect_uri ||= ENV['DEFAULT_REDIRECT_URI']

        query= {lifespan: lifespan}
        query[:client_id] = client_id if client_id
        if redirect_uri 
          query[:redirect_uri] = redirect_uri 
          query[:client_id] ||= self.client&.clientId 
        end
 

        uri = ::URI.parse "#{entity_url}/execute-actions-email"
        uri.query = URI.encode_www_form(query)

        response = put(url: uri.to_s, form: types.map(&:upcase), token: admin_token&.access_token)

        unless response.code.match(%r{20\d})
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        nil
      end

      def reset_password(password: nil, temporary: false)
        raise Error, 'password are empty' unless password

        form = {
          type: 'password',
          value: password,
          temporary: temporary,
        }
        uri = ::URI.parse "#{entity_url}/reset-password"

        response = put(url: uri.to_s, form: form, token: admin_token&.access_token)
        unless response.code.to_i == 204
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        nil
      end

      def self.build_from_token(token: nil, audience: Keycloak.default_client.clientId, realm: Keycloak.default_realm, client: Keycloak.default_client, configuration: nil)
        raise Error, 'token not defined' unless token
        raise Error, 'token hasnt access_token defined' unless token.respond_to? :access_token
        raise Error, 'token is expired' unless realm.token_introspection(access_token: token&.access_token, type: :state) == :active

        token_data = realm.token_introspection(access_token: token&.access_token)
        user = Keycloak::Entity::User.new(email: token_data[:email], realm: realm, client: client, configuration: configuration)
        user.token = token
        user.send(:mutate_entity)
        exchange_conditions = ( ( audience.present? and audience != user.client.clientId ) or user.token_introspection(type: :origin) == :external )
        exchange_conditions ? user.exchange_token(audience: audience || user.client.clientId) : user
      end

      def self.find(params)
         new(id: params[:key], realm: params[:realm], configuration: params[:configuration], admin_token: params[:admin_token], email: "")&.reload
      end 

      private

      def mandatory_attributes
        %i{ email realm }
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]

        id = params[:realm].id rescue params[:realm]
        realm = ::Keycloak::Entity::Realm.new(id: id, configuration: params[:configuration])
        params.delete(:realm)
        params.delete(:client)
        params.delete(:configuration)
        realm.send(:resource_url,{resource: 'users', params: params })
      end

      def entity_url
        "#{base_url}/users/#{self.send(self.class.send(:primary_key))}"
      end

      def entity_credentials
        raise Error, 'undefined user password' unless password
        client_credentials.merge(grant_type: 'password', username: username, password: password)
      end

      def client_credentials
        raise Error, 'undefined client parent' unless client
        client.send(:entity_credentials)
      end

      def resource_url(resource: nil, type: nil, params: {})
        raise Error, 'resource not defined' unless resource
        "#{entity_url}/#{resource}#{ type ? "/#{type}" : nil }?#{::URI.encode_www_form(params)}"
      end

      # used to mutate entity after token change
      def mutate_entity
        # check modules
        raise Error, 'token module is not included' unless self.class.ancestors.include? ::Keycloak::TokenHandler
        raise Error, 'crud module is not included'  unless self.class.ancestors.include? ::Keycloak::CRUD
        # check features
        raise Error, 'token feature is disabled'    unless respond_to? :token_introspection
        raise Error, 'reload feature is disabled'   unless respond_to? :reload

        info = token_introspection
        protocol,_,domain_with_port,_,_,realm_id = info[:iss].split('/')

        # dont update realm by default

        # create client
        unless self.client
          client_id = info[:azp]
          self.client = Client.new(clientId: client_id, realm: self.realm)
          self.client.secret = Keycloak.config.default_client_id == client_id ? Keycloak.config.default_client_secret : nil
          self.client.reload
        end

        # update himself
        origin = token_introspection(type: :origin)
        # to update you need token exchange before to get internal token
        update_conditions = ( origin == :internal and self.id != info[:sub] )
        if update_conditions
          self.id = info[:sub]
          self.email = info[:email]
          reload
        else
          self
        end
      end

    end
  end
end