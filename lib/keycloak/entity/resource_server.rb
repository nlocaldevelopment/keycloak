# frozen_string_literal: true

module Keycloak
  module Entity
    class ResourceServer
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::Keycloak::APIError; end

      # relations
      belongs_to :realm
      belongs_to :client

      # C.R.U.D.
      enable_crud :update, :reload

      # instantiation
      def initialize(args={})
        args = ::HashWithIndifferentAccess.new(args)
        send(:mandatory_attributes).each do |attr|
          raise Error, "#{attr} of #{self.class.name.demodulize.underscore} not found" unless args[attr]
        end
        config_with(data: args)
      end

      def to_h
        result = self.instance_variables.map do |attr|
          next if attr.match(%r{\A@errors\z})
          next if attr.match(%r{\A@validation_context\z})
          value = self.instance_variable_get(attr)
          [attr[1..-1].to_sym, value] if not value.is_a?(::Keycloak::Entity::Base) and not value.is_a?(::OpenStruct) and not value.is_a?(::Keycloak::Configuration)
        end.compact
        ::HashWithIndifferentAccess[result]
      end

      # public interface

      # I use this way for relation one to one
      # def resource_server
      #   return @resource_server if @resource_server.present?

      #   # we define this vars to binding in action entity
      #   url = resource_server_url
      #   access_token = admin_token&.access_token

      #   response = get(url: url, token: access_token)
      #   unless response.code.to_i == 200
      #     error = Keycloak::APIError.new response.body
      #     error.code = response.code
      #     raise error
      #   end

      #   entity = OpenStruct.new(::JSON.parse(response.body)).extend(Keycloak::Api)

      #   action = Proc.new do
      #     state = self.to_h
      #     response = put(url: url, form: state, token: access_token)
      #     unless response.code.to_i == 204
      #       error = Keycloak::APIError.new response.body
      #       error.code = response.code
      #       raise error
      #     end
      #     state.each{ |attr, value|  self.send("#{attr}=".to_sym,value) }
      #   end

      #   entity.define_singleton_method(:save,   action)
      #   entity.define_singleton_method(:update, action)

      #   @resource_server = entity
      # end

      private

      def mandatory_attributes
        %i{ realm client }
      end

      def base_url
        realm&.send(:admin_entity_url)
      end

      def entity_url
        "#{base_url}/clients/#{client.send(client.class.send(:primary_key))}/authz/resource-server"
      end

      def admin_token
        realm&.send(:admin_token)
      end

      def refresh_admin_token
        realm&.send(:refresh_token)&.token
      end

    end
  end
end