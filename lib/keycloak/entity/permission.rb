module Keycloak
  module Entity
    class Permission < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::StandardError; end

      # relations
      belongs_to :client
      belongs_to :realm
      has_many :associated_policies, entity: :policy, as: :associatedPolicies
      has_many :associated_resources, entity: :resource, as: :resource
      has_many :associated_scopes, as: :scopes


      # C.R.U.D.
      set_primary_key :id
      set_search_key :name
      enable_crud :search, :creation, :update, :delete, :reload

      # attributes
      attr_accessor :policies, :scopes, :resources

      # public interface

      private

      def mandatory_attributes
        %i{ name type logic decisionStrategy client realm }
      end

      def base_url
        client.send(:entity_url)
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]
        raise Error, 'client not found in attributes' unless params[:client]

        realm_id = params[:realm].id rescue params[:realm]
        params.delete(:realm)
        client_id = params[:client].id rescue params[:client]
        params.delete(:client)
        type = params[:type]
        params.delete(:type)

        realm = ::Keycloak::Entity::Realm.new(id: realm_id, configuration: params[:configuration])
        client = ::Keycloak::Entity::Client.new(id: client_id, clientId: '', realm: realm, configuration: params[:configuration])

        params.delete(:configuration)

        client.send(:resource_url,{resource: 'permission', type: type, params: params })
      end

      def entity_url
        "#{base_url}/authz/resource-server/permission/#{self.send(self.class.send(:primary_key))}"
      end

      def resource_url(resource: nil, params: {})
        raise Error, 'resource not defined' unless resource
        "#{entity_url}/#{resource}?#{::URI.encode_www_form(params)}"
      end

    end
  end
end