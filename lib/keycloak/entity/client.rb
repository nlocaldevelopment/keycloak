# frozen_string_literal: true

module Keycloak
  module Entity
    class Client < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations
      include ::Keycloak::TokenHandler
      include ::Keycloak::Authorization

      # errors
      class Error < ::StandardError; end

      # relations
      belongs_to :realm
      has_many :resources, as: :resource
      has_many :policies, as: :policy
      has_many :permissions, as: :permission
      has_many :scopes, as: :scope
      has_many :roles, entity: :client_role

      # C.R.U.D.
      set_search_key :clientId
      enable_crud :search, :creation, :update, :delete, :reload

      # token handler
      enable_token :token

      # authorization
      enable_authorization :client

      # attributes
      attr_accessor :secret, :resource_server

      # public interface
      def request_secret
        response = get(url: "#{entity_url}/client-secret", token: admin_token&.access_token)
        unless response.code.to_i == 200
          error = Keycloak::APIError.new response.body
          error.code = response.code
          raise error
        end
        @secret = ::JSON.parse(response.body)['value']
      end

      # one to one relation
      def resource_server
        return @resource_server if @resource_server.present?

        response = get(url: resource_server_url, token: admin_token&.access_token)
        unless response.code.to_i == 200
          error = Keycloak::APIError.new response.body
          error.code = response.code
          raise error
        end
        @resource_server = build_resource(type: :resource_server, info: ::JSON.parse(response.body))
      end

      def available_realm_management_permissions
        response = put(url: "#{entity_url}/management/permissions", form: { enabled: true }, token: admin_token&.access_token)
        unless response.code.to_i == 200
          error = Keycloak::APIError.new response.body
          error.code = response.code
          raise error
        end
        JSON.parse response.body
      end

      private

      def mandatory_attributes
        %i{ clientId realm }
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]

        id = params[:realm].id rescue params[:realm]
        realm = ::Keycloak::Entity::Realm.new(id: id, configuration: params[:configuration])
        params.delete(:realm)
        params.delete(:configuration)
        realm.send(:resource_url,{resource: 'clients', params: params })
      end

      def entity_credentials
        client_credentials
      end

      def client_credentials
        {grant_type: 'client_credentials', client_id: clientId, client_secret: secret}
      end

      def entity_url
        "#{base_url}/clients/#{self.send(self.class.send(:primary_key))}"
      end

      def resource_server_url
        "#{entity_url}/authz/resource-server"
      end

      def resource_url(resource: nil, type: nil, params: {})
        raise Error, 'resource not defined' unless resource

        if resource.to_s == 'roles'
          "#{entity_url}/#{resource}"
        else
          params.merge!(permission: false) if (resource.to_s == 'policy') and not params[:permission]
          query= ::URI.encode_www_form(params)
          query= "?#{query}" unless query.empty? 
          "#{resource_server_url}/#{resource}#{ type ? "/#{type}" : nil }#{query}"
        end
      end

    end
  end
end