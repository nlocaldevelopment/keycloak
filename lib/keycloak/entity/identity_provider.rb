module Keycloak
  module Entity
    class IdentityProvider < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::StandardError; end

      # relations
      belongs_to :realm

      # C.R.U.D.
      set_primary_key :alias
      set_search_key :alias
      enable_crud :search, :update, :reload

      # attributes

      # public interface

      private

      def mandatory_attributes
        %i{ alias realm }
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]

        id = params[:realm].id rescue params[:realm]
        realm = ::Keycloak::Entity::Realm.new(id: id, configuration: params[:configuration])
        params.delete(:realm)
        params.delete(:configuration)
        realm.send(:resource_url,{resource: 'identity-provider/instances', params: params })
      end

      def entity_url
        "#{base_url}/identity-provider/instances/#{self.send(self.class.send(:primary_key))}"
      end

    end
  end
end