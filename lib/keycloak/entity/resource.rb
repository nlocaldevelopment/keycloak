module Keycloak
  module Entity
    class Resource < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::StandardError; end

      # relations
      belongs_to :client
      belongs_to :user
      belongs_to :realm

      # C.R.U.D.
      set_primary_key :id
      set_search_key :uris, alias: :uri
      enable_crud :search, :creation, :update, :delete, :reload

      # attributes
      attr_accessor :scopes

      # public interface

      private

      def mandatory_attributes
        %i{ name type uris client realm}
      end

      def base_url
        client.send(:entity_url)
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]
        raise Error, 'client not found in attributes' unless params[:client]

        realm_id = params[:realm].id rescue params[:realm]
        params.delete(:realm)
        client_id = params[:client].id rescue params[:client]
        params.delete(:client)

        realm = ::Keycloak::Entity::Realm.new(id: realm_id, configuration: params[:configuration])
        client = ::Keycloak::Entity::Client.new(id: client_id, clientId: '', realm: realm, configuration: params[:configuration])

        params.delete(:configuration)

        client.send(:resource_url,{resource: 'resource', params: params })
      end

      def entity_url
        "#{base_url}/authz/resource-server/resource/#{self.send(self.class.send(:primary_key))}"
      end

    end
  end
end