module Keycloak
  module Entity
    class Realm < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations
      include ::Keycloak::TokenHandler

      # errors
      class Error < ::Keycloak::APIError; end

      # relations
      has_many :clients
      has_many :users
      has_many :roles
      has_many :groups
      has_many :identity_providers, as: 'identity-provider/instances'.to_sym

      # C.R.U.D.
      enable_crud :reload # is not necessary admin_token

      # token handler
      enable_token :token

      # attributes
      attr_reader :algorithm, :public_key

      def initialize(args={})
        args = ::HashWithIndifferentAccess.new(args)

        original_configuration = Keycloak.config.to_h
        args[:id] ||= args[:configuration]&.realm_id || original_configuration[:realm_id]
        args[:configuration] ||= Keycloak::Configuration.new original_configuration.merge(realm_id: args[:id])

        super(args)
      end


      # public interface

      def id=(value)
        configuration.realm_id = value if configuration
        @id = value
      end

      def update_cert
        response = get(url: jwks_uri)
        datas = ::JSON.parse response.body
        unless keys = datas['keys']
          error = Error.new 'keys not found'
          error.code = response.code
          raise error
        end

        key = keys.first
        pem = "-----BEGIN CERTIFICATE-----\n#{key['x5c'][0]}\n-----END CERTIFICATE-----"
        cert = ::OpenSSL::X509::Certificate.new(pem)

        @algorithm = key['alg']
        @public_key = cert.public_key
        self
      end

      # monkey patch reload crud method
      def reload
        update_openid_configuration
        update_uma2_configuration
        update_cert
        request_token
      end

      private

      attr_accessor :configuration

      def mandatory_attributes
        %i{ id }
      end

      def base_url
        "#{configuration.server_protocol}://#{configuration.server_domain}:#{configuration.server_port}/auth"
      end

      def entity_url
        "#{base_url}/realms/#{id}"
      end

      def broker_url
        "#{entity_url}/broker"
      end

      def admin_entity_url
        "#{base_url}/admin/realms/#{id}"
      end

      def openid_configuration_url
        "#{entity_url}/.well-known/openid-configuration"
      end

      def uma2_configuration_url
        "#{entity_url}/.well-known/uma2-configuration"
      end

      def resource_url(resource: nil, params: {})
        raise Error, 'resource not defined' unless resource
        "#{admin_entity_url}/#{resource}?#{URI.encode_www_form(params)}"
      end

      def update_openid_configuration
        response = get(url: openid_configuration_url)
        unless response.code.to_i == 200
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        config_with data: ::JSON.parse(response.body)
      end

      def update_uma2_configuration
        response = get(url: uma2_configuration_url)
        unless response.code.to_i == 200
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        config_with data: ::JSON.parse(response.body)
      end

      # mokey patch for base definition method,
      # this method is defined too in runtime with an attr_accessor call when realm is reloaded
      def token_endpoint; end

      def admin_token
        token
      end

      def entity_credentials
        {
          grant_type: 'password',
          client_id: configuration.admin_client_id,
          client_secret: configuration.admin_client_secret,
          username: configuration.admin_username,
          password: configuration.admin_password,
        }
      end

      alias_method :client_credentials, :entity_credentials

    end
  end
end