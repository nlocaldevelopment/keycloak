module Keycloak
  module Entity
    class FederatedIdentity < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::StandardError; end

      # relations
      belongs_to :user
      belongs_to :realm

      # C.R.U.D.
      set_primary_key :identityProvider
      set_search_key :userName
      enable_crud :search, :creation, :delete # :reload its not posible because this is a fake entity without id

      # attributes

      # public interface

      private

      def mandatory_attributes
        %i{ user userName userId identityProvider realm}
      end

      def base_url
        user.send(:entity_url)
      end

      def entity_url
        "#{base_url}/federated-identity/#{self.send(self.class.send(:primary_key))}"
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]
        raise Error, 'user not found in attributes' unless params[:user]

        realm_id = params[:realm].id rescue params[:realm]
        params.delete(:realm)
        user_id = params[:user].id rescue params[:user]
        params.delete(:user)
        type = params[:identityProvider]
        params.delete(:identityProvider)

        realm = ::Keycloak::Entity::Realm.new(id: realm_id, configuration: params[:configuration])
        user = ::Keycloak::Entity::User.new(id: user_id, email: '', realm: realm, configuration: params[:configuration])

        params.delete(:configuration)

        user.send(:resource_url,{resource: 'federated-identity', type: type, params: params })
      end

    end
  end
end