# frozen_string_literal: true

module Keycloak
  module Entity
    class Group < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::Keycloak::APIError; end

      # relations
      belongs_to :realm
      has_many :users, as: :members

      # C.R.U.D.
      set_search_key :name, alias: :search # dont use path because doesnt work. set names similar to path api-admin
      enable_crud :search, :creation, :update, :delete, :reload

      # attributes

      # public interface

      def subgroups
        subGroups.map{ |g| self.class.new(g.merge(realm: realm)) } if self.respond_to? :subGroups
      end

      def add_subgroup(subgroup: nil)
        raise Error, 'subgroup not defined' unless subgroup

        datas = subgroup.to_h
        url = "#{entity_url}/children"
        response = post(url: url, form: datas, token: admin_token&.access_token)
        unless response.code.match(%r{20\d})
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        subGroups.push datas
      end

      def realm_roles(available: false)
        url   = "#{entity_url}/role-mappings/realm#{available ? '/available' : nil}"
        roles = get_roles(url: url)
        roles.map{ |data| build_role(type: available ? :available : :assigned, data: data, reference: url) }
      end

      def client_roles(client: nil, available: false)
        raise Error, 'undefined client' unless client&.id

        url   = "#{entity_url}/role-mappings/clients/#{client.id}#{available ? '/available' : nil}"
        roles = get_roles(url: url)
        roles.map{ |data| build_role(type: available ? :available : :assigned, data: data, reference: url) }
      end

      # monkey patch crud module method
      # because when you search a subgroup, keycloak returns the root group
      def self.find_by(params={})
        raise Error, 'not valid format attributes' unless params.is_a? Hash
        identity_key = params.keys.map(&:to_sym) - %i{admin_token type configuration}
        identity_key -= up_associations if respond_to? :up_associations # remove parent relations if you use relations module
        raise Error, 'invalid amount of attributes' unless identity_key.size == 1

        if group = where(params)&.first
          key   = search_key_aliases.select{ |k,v| v == identity_key.first }.keys.first || identity_key.first
          value = params[identity_key.first]
          group.send(key) == value ? group : search_into_subgroups(group: group , key: key, value: value)
        else
          group
        end
      end

      private

      def mandatory_attributes
        %i{ name path realm }
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)

        raise Error, 'realm not found in attributes' unless params[:realm]

        id = params[:realm].id rescue params[:realm]
        realm = ::Keycloak::Entity::Realm.new(id: id, configuration: params[:configuration])
        params.delete(:realm)
        params.delete(:configuration)
        realm.send(:resource_url,{resource: 'groups', params: params })
      end

      def self.search_into_subgroups(group: nil, key: nil, value: nil)
        groups = group.subgroups
        groups.each do |subgroup|
          if subgroup.send(key) == value
            return subgroup
          else
            subgroup = search_into_subgroups(group: subgroup, key: key, value: value)
            return subgroup if subgroup
          end
        end
        nil
      end

      def entity_url
        "#{base_url}/groups/#{self.send(self.class.send(:primary_key))}"
      end

      def resource_url(resource: nil, params: {})
        raise Error, 'resource not defined' unless resource
        "#{entity_url}/#{resource}?#{::URI.encode_www_form(params)}"
      end

      def get_roles(url: nil)
        response = get(url: url, token: admin_token&.access_token)
        unless response.code.match(%r{20\d})
          error = Error.new response.body
          error.code = response.code
          raise error
        end
        ::JSON.parse response.body
      end

      def build_role(type: :assigned, data: {}, reference: "", token: admin_token&.access_token)
        reference = reference.gsub('/available', '')

        if type == :assigned
          action_name   = :leave
          action_method = :del
        else
          action_name   = :join
          action_method = :post
        end

        action = Proc.new do
          response = self.send(action_method, { url: reference, form: [data], token: token })
          unless response.code.match(%r{20\d})
            error = ::Keycloak::APIError.new response.body
            error.code = response.code
            raise error
          end
        end

        role = OpenStruct.new data
        role.extend(::Keycloak::Api)
        role.define_singleton_method("#{action_name}_#{self.class.name.demodulize.underscore}".to_sym,action)
        role
      end

    end
  end
end