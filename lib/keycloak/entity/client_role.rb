module Keycloak
  module Entity
    class ClientRole < Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations

      # errors
      class Error < ::StandardError; end

      # relations
      belongs_to :realm
      belongs_to :client

      # C.R.U.D.
      set_primary_key :name
      set_search_key :name # search is unavailable
      enable_crud :creation, :update, :delete, :reload

      # attributes

      # public interface

      private

      def mandatory_attributes
        %i{ name client realm}
      end

      def base_url
        client.send(:entity_url)
      end

      def self.search_url(params={})
        params = ::HashWithIndifferentAccess.new(params)
        raise Error, 'realm not found in attributes' unless params[:realm]
        raise Error, 'client not found in attributes' unless params[:client]

        realm_id = params[:realm].id rescue params[:realm]
        realm = ::Keycloak::Entity::Realm.new(id: realm_id, configuration: params[:configuration])

        client_id = params[:client].id rescue params[:client]
        client = ::Keycloak::Entity::Client.new(id: client_id, clientId: '', realm: realm, configuration: params[:configuration])

        # we dont pass query params because doesnt work
        "#{client.send(:entity_url)}/roles"
      end

      def entity_url
        "#{base_url}/roles/#{self.send(self.class.send(:primary_key))}"
      end

    end
  end
end