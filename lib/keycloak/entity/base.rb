# frozen_string_literal: true

module Keycloak
  module Entity
    class Base
      # libs
      include ::Keycloak::CRUD # include and extend with api module
      include ::Keycloak::Relations
      include ::Keycloak::TokenHandler
      include ::Keycloak::Authorization

      # attributes
      attr_accessor primary_key, search_key

      # instantiation
      def initialize(args={})
        args = ::HashWithIndifferentAccess.new(args)
        send(:mandatory_attributes).each do |attr|
          raise Error, "Attr #{attr} of #{self.class.name.demodulize.underscore} not found" unless args[attr]
        end
        config_with(data: args)
      end

      def to_h
        result = self.instance_variables.map do |attr|
          next if attr.match(%r{\A@errors\z})
          next if attr.match(%r{\A@validation_context\z})
          value = self.instance_variable_get(attr)
          [attr[1..-1].to_sym, value] if not value.is_a?(::Keycloak::Entity::Base) and not value.is_a?(::OpenStruct) and not value.is_a?(::Keycloak::Configuration) and not value.is_a?(::Keycloak::Entity::ResourceServer)
        end.compact
        ::HashWithIndifferentAccess[result]
      end

      def as_json(options = {})
        options[:with_relations] ? super : self.to_h
      end 

      def update_configuration; end

      private

      def mandatory_attributes; []; end

      def base_url
        realm&.send(:admin_entity_url)
      end

      def broker_url
        "#{realm&.send(:entity_url)}/broker"
      end

      def token_endpoint
        realm&.token_endpoint
      end

      def end_session_endpoint
        realm&.end_session_endpoint
      end

      def public_key
        realm&.public_key
      end

      def algorithm
        realm&.algorithm
      end

      def issuer
        realm&.issuer
      end

      def admin_token
        realm&.send(:admin_token)
      end

      def refresh_admin_token
        realm&.send(:refresh_token)&.token
      end

    end
  end
end