module Keycloak
  module Relations
    # errors
    class Error < ::Keycloak::APIError; end

    module ClassMethods
      # public interface
      def has_many(relation, **options)
        add_entity(reference: relation, entity: options[:entity]) if options[:entity]
        add_aliase(reference: relation, aliase: options[:as]) if options[:as]
        down_associations << relation
        build_children_relation(relation)
      end

      def belongs_to(relation)
        up_associations << relation
        build_parent_relation(relation)
      end

      private

      def build_children_relation(relation=nil)
        return unless relation
        define_method(relation) do |args = {}|
          entity = self.class.entities[relation] || relation.to_s.singularize.to_sym
          aliase = self.class.aliases[relation] || relation

          response = get(url: resource_url(resource: aliase, params: args), token: admin_token&.access_token)
          unless response.code.to_i == 200
            error = Error.new response.body
            error.code = response.code
            raise error
          end
          resources = ::JSON.parse(response.body)
          resources.map{ |r| build_resource(type: entity, info: r) }
        end
      end

      def build_parent_relation(relation=nil)
        return unless relation
        # reader accessor
        define_method(relation) do
          instance_variable_get("@#{relation}".to_sym)
        end
      end

      def add_entity(reference: nil, entity: nil)
        raise Error, 'reference not found' unless reference
        raise Error, 'entity not found' unless entity

        self.entities[reference.to_sym] = entity.to_sym
      end

      def add_aliase(reference: nil, aliase: nil)
        raise Error, 'reference not found' unless reference
        raise Error, 'aliase not found' unless aliase

        self.aliases[reference.to_sym] = aliase.to_sym
      end
    end

    module InstanceMethods
      private

      # define in class!
      def resource_url; end

      def build_resource(type: nil, info: {})
        klass = ::Kernel.const_get("Keycloak::Entity::#{type.to_s.camelize}") rescue ::OpenStruct
        reference = self.class.name.demodulize.underscore.to_sym
        klass.up_associations.each do |parent|
          info.merge!(parent => self.send(parent)) if self.respond_to? parent
          info.merge!(parent => self) if parent == reference
        end if klass.respond_to? :up_associations
        klass.new(info)
      end
    end

    def self.included(base)
      # module dependencies
      raise Error, 'c.r.u.d. module is neccesary to use relations module' unless base.ancestors.include? ::Keycloak::CRUD

      base.extend         ClassMethods
      base.send :include, InstanceMethods

      base.class_attribute :down_associations
      base.class_attribute :up_associations
      base.class_attribute :entities
      base.class_attribute :aliases
      base.down_associations = []
      base.up_associations   = []
      base.entities = {}
      base.aliases = {}
    end

  end
end