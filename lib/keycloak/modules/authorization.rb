module Keycloak
  module Authorization

    # errors
    class Error < ::Keycloak::APIError; end

    module ClassMethods
      private

      def enable_authorization(type)

        case type
        when :client
          class_eval do
            define_method(:authorized?) do |options={}|
              response = authorization_request(options.merge(client_credentials))
              response.code.to_i == 200 ? true : false
            end
          end
        when :user
          class_eval do
            define_method(:authorized?) do |options={}|
              response = authorization_request(options.merge(access_token: token&.access_token))
              response.code.to_i == 200 ? true : false
            end
          end
        when :resource
          class_eval do
            define_method(:authorized?) do |options={}|
              response = authorization_request(options.merge(resource: name))
              response.code.to_i == 200 ? true : false
            end
          end
        end

        self
      end

    end

    module InstanceMethods
      # public interface

      def authorization_request(options={})
        raise Error, 'resource is not defined' unless options[:resource]

        form =  if options[:client_id]
                  {
                    client_id: options[:client_id],
                    client_secret: options[:client_secret],
                  }
                else
                  {}
                end

        token = options[:access_token]
        raise Error, 'token is not defined' if form.empty? and not token
        token_audience = (token and respond_to?(:token_introspection)) ? token_introspection(access_token: token)[:aud] : nil

        form.merge!({
          grant_type: 'urn:ietf:params:oauth:grant-type:uma-ticket',
          audience: options[:audience] || token_audience || client_credentials[:client_id],
          response_mode: options[:response_mode] || 'decision',
          permission: options[:scope] ? "#{options[:resource]}##{options[:scope]}" : options[:resource],
        })

        response = post(url: token_endpoint, form: form, token: token)
        code = response.code.to_i
        unless code == 200 or code == 403
          error = Error.new response.body
          error.code = code
          raise error
        end
        response
      end

      private

      # define in class!
      def token_endpoint; end
      def client_credentials; {}; end

    end

    def self.included(base)
      # module dependencies
      raise Error, 'api module is neccesary to use authorization module' unless base.ancestors.include? ::Keycloak::Api
      raise Error, 'token_handler module is neccesary to use authorization module' unless base.ancestors.include? ::Keycloak::TokenHandler

      base.send :extend,  ClassMethods
      base.send :include, InstanceMethods
      base.attr_accessor :token
    end

  end

end
