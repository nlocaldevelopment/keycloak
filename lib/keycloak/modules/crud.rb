# frozen_string_literal: true

module Keycloak
  module CRUD

    # errors
    class Error < ::Keycloak::APIError; end

    module ClassMethods

      def set_primary_key(value)
        self.primary_key = value
      end

      def set_search_key(value, options={})
        self.search_key = value
        self.search_key_aliases[value] = options[:alias]
      end

      private

      # define in class!
      def search_url(params={}); end

      def enable_crud(*actions)
        actions = [actions].flatten

        if actions.include?(:search)
          class_eval do
            # where
            define_singleton_method(:where) do |params=nil|
              raise Error, 'attributes not found' unless params
              raise Error, 'admin_token not found' unless admin_token = params[:admin_token]
              params.delete(:admin_token) # removed because this params is not neccesary to build search_url

              # add parent relations to params if use relations module. used to build search_url
              relations = {}
              self.up_associations.each do |parent_relation|
                parent = params[parent_relation.to_sym]
                relations[parent_relation.to_sym] = parent if parent.present?
              end if self.respond_to? :up_associations

              url = search_url(params)

              response = get(url: url, token: admin_token&.access_token)
              unless response.code.match(%r{20\d})
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              ::JSON.parse(response.body).map{ |data| self.new(data.merge(relations)) }
            end

            # all
            define_singleton_method(:all) do |params={}|
              relations = {}
              self.up_associations.each do |parent_relation|
                relations[parent_relation] = params[parent_relation]
              end if self.respond_to? :up_associations

              where(relations.merge(admin_token: params[:admin_token]))
            end

            # find_by
            define_singleton_method(:find_by) do |params={}|
              raise Error, 'not valid format attributes' unless params.is_a? Hash
              identity_key = params.keys.map(&:to_sym) - %i{admin_token type configuration}
              identity_key -= up_associations if respond_to? :up_associations # remove parent relations if you use relations module
              raise Error, 'invalid amount of attributes' unless identity_key.size == 1
              where(params)&.first
            end

            # find
            define_singleton_method(:find) do |params={}|
              params = params.merge!(primary_key => params[:key])
              params.delete(:key)
              find_by(params)
            end
          end
        end

        if actions.include?(:creation)
          class_eval do
            # create
            define_singleton_method(:create) do |params={}|
              raise Error, 'attributes not found' unless params.present?
              raise Error, 'admin_token not found' unless admin_token = params[:admin_token]
              params.delete(:admin_token)

              record = self.new(params)
              raise Error, record.errors.messages unless record.valid?

              url = search_url(params).split('?').first

              response = post(url: url, form: record.to_h, token: admin_token&.access_token)
              unless response.code.match(%r{20\d})
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              record.respond_to?(:reload) ? record.reload : record
            end

            define_method(:save) do
              raise Error, self.errors.messages unless self.valid?
              raise Error, 'admin_token not found' unless admin_token

              attributes = self.to_h
              self.class.up_associations.each do |parent_relation|
                parent = self.send(parent_relation.to_sym)
                attributes[parent_relation.to_sym] = parent if parent.present?
              end if self.class.respond_to? :up_associations

              config_with data: self.class.create(attributes.merge(admin_token: admin_token, configuration: self&.realm&.configuration)).to_h
            end
          end
        end

        if actions.include?(:update)
          class_eval do
            # update
            define_method(:update) do |params={}|
              raise Error, 'admin_token not found' unless admin_token
              raise Error, 'id reference not found' unless self.send(self.class.primary_key)

              self.class.up_associations.each do |parent_relation|
                parent = self.send(parent_relation.to_sym)
                params[parent_relation.to_sym] = parent if parent.present?
              end if self.class.respond_to? :up_associations
              record = self.class.new(self.to_h.merge(params))

              raise Error, record.errors.messages unless record.valid?

              datas = record.to_h
              datas.delete(self.class.primary_key)

              response = put(url: entity_url, form: datas, token: admin_token&.access_token)
              if response.code.match(%r{20\d})
                config_with(data: params)
              else
                error = Error.new response.body
                error.code = response.code
                raise error
              end
            end
          end
        end

        if actions.include?(:delete)
          class_eval do
            # delete
            define_method(:delete) do
              raise Error, 'admin_token not found' unless admin_token
              raise Error, 'id reference not found' unless self.send(self.class.primary_key)
              response = del(url: entity_url, token: admin_token&.access_token)
              if response.code.match(%r{20\d})
                self.send("#{self.class.primary_key}=".to_sym,nil)
              else
                error = Error.new response.body
                error.code = response.code
                raise error
              end
            end
          end
        end

        if actions.include?(:reload)
          class_eval do
            # reload
            define_method(:reload) do
              raise Error, 'admin_token not found' unless admin_token
              same_keys = self.class.primary_key == self.class.search_key
              id = self.send(self.class.primary_key)
              if same_keys and not id
                raise Error, 'id reference not found' unless reference = self.send(self.class.primary_key)
              elsif not same_keys and not id
                # alternative find_by search_key
                raise Error, 'search reference not found' unless reference = self.send(self.class.search_key)

                # basic params
                search_key = self.class.search_key_aliases[self.class.search_key] || self.class.search_key
                params = {
                  search_key => reference, # attribute to search
                  :admin_token => admin_token, # necesary to make the request
                }

                # add parent relations to params if use relations module. used to build search_url
                if self.class.respond_to? :up_associations
                  self.class.up_associations.each do |parent_relation|
                    parent = self.send(parent_relation.to_sym)
                    params[parent_relation.to_sym] = parent if parent.present?
                  end
                end

                params.merge!(type: type) if self.respond_to? :type

                record = self.class.find_by(params.merge(configuration: self&.realm&.configuration))
                if id = record&.id
                  self.send("#{self.class.primary_key}=".to_sym,id)
                else
                  error = Error.new({ error: "any record by_#{self.class.search_key} found" }.to_json)
                  error.code = '404'
                  raise error
                end
              end

              response = get(url: entity_url, token: admin_token&.access_token)
              if response.code.match(%r{20\d})
                config_with data: ::JSON.parse(response.body)
              else
                error = Error.new response.body
                error.code = response.code
                raise error
              end
            end
          end
        end

        self
      end

    end

    module InstanceMethods
      private

      # define in class!
      def entity_url(id); end
      def admin_token; end

      def config_with(data: {})
        raise Error, 'data error format' unless data.is_a? Hash
        return self if data.empty?

        # to avoid monkey patching the original method
        data.delete(:admin_token)

        openstruct = self.is_a? OpenStruct
        data.each do |key, value|
          key = :id if key.match(%r{\A_id\z})
          unless openstruct
            self.respond_to?("#{key}=".to_sym) ? self.class.send(:attr_reader,key) : self.class.send(:attr_accessor,key)
          end
          self.send("#{key}=".to_sym,value)
        end
        self
      end

    end

    def self.included(base)
      base.extend  ClassMethods
      base.extend  ::Keycloak::Api

      base.include InstanceMethods
      base.include ::Keycloak::Api
      base.include ::ActiveModel::Validations

      base.class_attribute :primary_key
      base.class_attribute :search_key
      base.class_attribute :search_key_aliases
      base.primary_key = :id
      base.search_key  = :id
      base.search_key_aliases = {}
    end

  end

end