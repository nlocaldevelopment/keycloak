module Keycloak
  module TokenHandler

    # errors
    class Error < ::Keycloak::APIError; end

    module ClassMethods
      private

      def enable_token(*actions)
        actions = [actions].flatten

        if actions.include? :token
          class_eval do
            # request token
            define_method(:request_token) do |options={}|
              form = entity_credentials
              form.merge!(options) unless options.empty?
              response = post(url: token_endpoint, form: form)
              unless response.code.to_i == 200
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              @token = ::OpenStruct.new(::JSON.parse(response.body))
              self
            end

            # refresh token
            define_method(:refresh_token) do |options={}|
              raise Error, 'token is not defined' unless token&.access_token

              form = client_credentials.merge!(grant_type: 'refresh_token', refresh_token: token&.refresh_token)
              form.merge!(options) unless options.empty?
              response = post(url: token_endpoint, form: form)
              unless response.code.to_i == 200
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              @token = ::OpenStruct.new(::JSON.parse(response.body))
              self
            end

            # token introspection
            define_method(:token_introspection) do |type: :deserialize, access_token: token&.access_token|
              raise Error, 'token is not defined' unless access_token
              raise Error, 'algorithm is not defined' unless algorithm
              raise Error, 'public_key is not defined' unless public_key

              unless type == :validate
                deserialized = ::HashWithIndifferentAccess.new(::JWT.decode(access_token, nil, false, { algorithm: algorithm }).first)
              end

              case type
              when :deserialize
                deserialized
              when :origin
                deserialized[:iss] == issuer ? :internal : :external
              when :state
                Time.now < Time.at(deserialized[:exp]) ? :active : :expired
              when :entity
                deserialized.send(:[],:clientId) ? :client : :user
              when :validate
                begin
                  ::HashWithIndifferentAccess.new(::JWT.decode(access_token, public_key, true, { algorithm: algorithm }).first)
                rescue ::Exception => e
                  raise Error, e.message
                end
              end
            end

            # logout
            define_method(:logout) do
              raise Error, 'token is not defined' unless token&.access_token

              client_credentials.delete(:grant_type)
              response = post(url: end_session_endpoint, form: client_credentials.merge(refresh_token: token&.refresh_token))
              unless response.code.to_i == 204
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              self
            end

            # build from token
            define_singleton_method(:build_from_token) do |options={}|
              # redefine in entity classes
            end
          end
        end

        if actions.include? :exchange
          class_eval do
            # exchange token
            define_method(:exchange_token) do |options={}|
              raise Error, 'token is not defined' unless token&.access_token

              # default exchange by audience
              form = client_credentials.merge(
                grant_type: 'urn:ietf:params:oauth:grant-type:token-exchange',
                audience: options[:audience] || client_credentials[:client_id],
                requested_token_type: 'urn:ietf:params:oauth:token-type:refresh_token',
                subject_token_type: 'urn:ietf:params:oauth:token-type:access_token',
                subject_token: token&.access_token,
              )

              form.merge!(scope: options[:scope]) if options[:scope]

              # external for internal default token
              if token_introspection(type: :origin) == :external
                form.merge!(
                  audience: client_credentials[:client_id],
                  subject_token_type: 'urn:ietf:params:oauth:token-type:jwt',
                )
              end

              response = post(url: token_endpoint, form: form)
              unless response.code.to_i == 200
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              @token = ::OpenStruct.new(::JSON.parse(response.body))
              mutate_entity
            end

            # token for external provider
            define_method(:external_provider_token) do |options={}|
              raise Error, 'token is not defined' unless token&.access_token
              raise Error, 'target is not defined' unless target = options[:target]

              if options[:live]
                form = client_credentials.merge(
                  grant_type: 'urn:ietf:params:oauth:grant-type:token-exchange',
                  requested_issuer: target,
                  subject_token_type: 'urn:ietf:params:oauth:token-type:access_token',
                  subject_token: token&.access_token,
                )
                # new token
                response = post(url: token_endpoint, form: form)
              else
                # stored token
                response = get(url: token_provider_endpoint(target), token: token&.access_token)
              end

              unless response.code.to_i == 200
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              ::OpenStruct.new(::JSON.parse(response.body))
            end
          end
        end

        if actions.include? :impersonation
          class_eval do
            define_method(:impersonate) do |options={}|
              raise Error, 'requested_subject is not defined' unless options[:requested_subject]

              form = client_credentials.merge(
                grant_type: 'urn:ietf:params:oauth:grant-type:token-exchange',
                requested_token_type: 'urn:ietf:params:oauth:token-type:refresh_token',
                audience: options[:audience] || client_credentials[:client_id],
                requested_subject: options[:requested_subject],
              )

              unless options[:direct_naked] # direct naked impersonate with a client
                raise Error, 'token is not defined' unless token&.access_token
                form.merge!(
                  subject_token: token&.access_token,
                )
              end

              response = post(url: token_endpoint, form: form)
              unless response.code.to_i == 200
                error = Error.new response.body
                error.code = response.code
                raise error
              end
              @token = ::OpenStruct.new(::JSON.parse(response.body))
              mutate_entity
            end
          end
        end

        self
      end

    end

    module InstanceMethods
      private

      # define in class!
      def entity_url; end
      def broker_url; end
      def token_endpoint; end
      def token_provider_endpoint(provider)
        "#{broker_url}/#{provider}/token"
      end
      def end_session_endpoint; end
      def entity_credentials; {}; end
      def client_credentials; {}; end
      def public_key; end
      def algorithm; end
      def issuer; end

      # define in class!
      def mutate_entity
        self
      end

    end

    def self.included(base)
      # module dependencies
      raise Error, 'api module is neccesary to use token_handler module' unless base.ancestors.include? ::Keycloak::Api

      base.send :extend,  ClassMethods
      base.send :include, InstanceMethods
      base.attr_accessor :token
    end

  end
end
