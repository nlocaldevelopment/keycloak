require 'net/http'

module Keycloak
  module Api

    # errors
    class Error < ::StandardError; end

    # public interface

    def get(url: nil, token: nil)
      raise Error, 'url attribute not found' unless url

      uri = ::URI.parse(url)

      http = ::Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true if ENV['USE_SSL']

      http.start do |h|
        request = ::Net::HTTP::Get.new(uri.request_uri)
        request['Authorization'] = "Bearer #{token}" if token
        h.request(request)
      end
    end

    def post(url: nil, form: {}, token: nil)
      raise Error, 'url attribute not found' unless url
      raise Error, 'form attribute not found' unless form.any?

      uri = ::URI.parse(url)

      http = ::Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true if ENV['USE_SSL']

      http.start do |h|
        request = ::Net::HTTP::Post.new(uri)
        if form.is_a?(Hash) and form.keys.include? :grant_type
          request.set_form_data(form)
        else
          # entity creation
          request.body = form.to_json
          request['Content-Type'] = 'application/json'
        end
        request['Authorization'] = "Bearer #{token}" if token
        h.request(request)
      end
    end

    def put(url: nil, form: {}, token: nil)
      raise Error, 'url attribute not found' unless url
      raise Error, 'form attribute not found' unless form.any?

      uri = ::URI.parse(url)

      http = ::Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true if ENV['USE_SSL']

      http.start do |h|
        request = ::Net::HTTP::Put.new(uri)
        request.body = form.to_json
        request['Authorization'] = "Bearer #{token}" if token
        request['Content-Type'] = 'application/json'
        h.request(request)
      end
    end

    def del(url: nil, form: {}, token: nil)
      raise Error, 'url attribute not found' unless url

      uri = ::URI.parse(url)

      http = ::Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true if ENV['USE_SSL']

      http.start do |h|
        request = ::Net::HTTP::Delete.new(uri)
        request.body = form.to_json if form.any?
        request['Authorization'] = "Bearer #{token}" if token
        request['Content-Type'] = 'application/json'
        h.request(request)
      end
    end

  end
end
