module Keycloak
  class APIError < ::StandardError
    attr_accessor :code
  end
end