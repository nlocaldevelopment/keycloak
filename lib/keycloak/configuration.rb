require 'singleton'

module Keycloak
  class Configuration
    # libs
    include ::ActiveModel::Validations

    # constants
    @@attributes = %i{
      realm_id
      admin_client_id
      admin_client_secret
      admin_username
      admin_password
      default_client_id
      default_client_secret
      server_protocol
      server_domain
      server_port
    }

    # attributes
    attr_accessor *@@attributes

    def initialize(args={})
      args = ::HashWithIndifferentAccess.new(args)
      @@attributes.each do |attr|
        send("#{attr}=".to_sym, args[attr])
      end
    end

    # set with default attrs
    def server_protocol=(value)
      value = 'http' if value.nil?
      @server_protocol = value
    end

    def server_domain=(value)
      value = 'localhost' if value.nil?
      @server_domain = value
    end

    def server_port=(value)
      value = '80' if value.nil?
      @server_port = value
    end

    # validations
    validates :server_protocol, inclusion: { in: %w{ http https } }
    validates :server_domain, format: { with: %r{\A(([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5})|localhost)\z} }
    validates :server_port, length: { in: 1..5}, numericality: { only_integer: true }

    # public interface

    def reset
      instance_variables.each do |var|
        next if var.match(%r{error})
        send("#{var[1..-1]}=".to_sym,nil)
      end
    end

    def to_h
      result = instance_variables.map do |var|
        var = var[1..-1]
        [var, self.send(var.to_sym)]
      end
      ::HashWithIndifferentAccess[result]
    end

  end
end